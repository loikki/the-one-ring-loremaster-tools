<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AdversarySummary</name>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="14"/>
        <source>SummaryAdversary</source>
        <translation>Riassunto Avversari</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="20"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="26"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="43"/>
        <source>Hate / Resolve</source>
        <translation>Odio / Risolutezza</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="62"/>
        <location filename="../src/gui/adversary_summary.ui" line="90"/>
        <location filename="../src/gui/adversary_summary.ui" line="97"/>
        <location filename="../src/gui/adversary_summary.ui" line="111"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="69"/>
        <source>Attribute Level</source>
        <translation>Livello di Attributo</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="76"/>
        <source>Armor</source>
        <translation>Armatura</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="83"/>
        <source>Parry</source>
        <translation>Difesa</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="104"/>
        <source>Combat Proficiencies</source>
        <translation>Competenze di Combattimento</translation>
    </message>
</context>
<context>
    <name>AdversarySummaryUi</name>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="49"/>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="74"/>
        <source>Hate</source>
        <translation>Odio</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="52"/>
        <source>Resolve</source>
        <translation>Risolutezza</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="75"/>
        <source>Hate lost</source>
        <translation>Odio perso</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="77"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="78"/>
        <source>Endurance lost</source>
        <translation>Resistenza persa</translation>
    </message>
</context>
<context>
    <name>AdversaryUi</name>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="40"/>
        <source>Adversary ready</source>
        <translation>Avversario pronto</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="113"/>
        <source>Adversary removed</source>
        <translation>Avversario rimosso</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="131"/>
        <source>Open Adversary</source>
        <translation>Apri Avversario</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="133"/>
        <source>Adversary file (*.adv)</source>
        <translation>File Avversario (*.adv)</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="148"/>
        <source>Adversary loaded</source>
        <translation>Avversario caricato</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="157"/>
        <source>New adversary created</source>
        <translation>Crea nuovo avversario</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="192"/>
        <source>Adversary saved</source>
        <translation>Salva Avversario</translation>
    </message>
</context>
<context>
    <name>EventDialog</name>
    <message>
        <location filename="../src/gui/event.ui" line="14"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="22"/>
        <source>Role</source>
        <translation>Ruolo</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="36"/>
        <source>Event Type</source>
        <translation>Tipo di Evento</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="63"/>
        <source>Event Title</source>
        <translation>Titolo dell&apos;evento</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="70"/>
        <source>Recommended Skills</source>
        <translation>Abilità raccomandate</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="97"/>
        <source>Roll</source>
        <translation>Genera</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="104"/>
        <source>Save Event Done</source>
        <translation>Evento salvato fatto</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="111"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/main_window.cpp" line="344"/>
        <source>Update CORS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="345"/>
        <source>New CORS:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="355"/>
        <source>Restart needed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="356"/>
        <source>The update of the CORS rule will be taken into account next time you start this software.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NameGeneratorUi</name>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="28"/>
        <source>Balrog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="29"/>
        <source>Bree</source>
        <translation>Brea</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="31"/>
        <source>Beorning / Woodmen</source>
        <translation>Beorniani / Uomini dei Boschi</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="32"/>
        <source>Dale</source>
        <translation>Valle</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="34"/>
        <source>Druedain</source>
        <translation>Dunedain</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="36"/>
        <source>Dunlending</source>
        <translation>Dunlandiani</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="37"/>
        <source>Dwarf</source>
        <translation>Nani</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="39"/>
        <source>Easterling</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="41"/>
        <source>Haradrim</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="43"/>
        <source>Hobbit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="44"/>
        <source>Maiar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="45"/>
        <source>Orc</source>
        <translation>Orchi</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="47"/>
        <source>Quenya</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="49"/>
        <source>Rohirrim</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="51"/>
        <source>Sindarin</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Patron</name>
    <message>
        <location filename="../src/gui/patron.ui" line="14"/>
        <source>Patron</source>
        <translation>Patrono</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="22"/>
        <source>Fellowship points</source>
        <translation>Punti Compagnia</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="51"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="64"/>
        <source>Advantages</source>
        <translation>Vantaggi</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="91"/>
        <source>New Patron</source>
        <translation>Nuovo Patrono</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="98"/>
        <source>Save Patron</source>
        <translation>Salva Patrono</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="105"/>
        <source>Load Patron</source>
        <translation>Carica Patrono</translation>
    </message>
</context>
<context>
    <name>PatronUi</name>
    <message>
        <location filename="../src/patron_ui.cpp" line="98"/>
        <source>Open Patron</source>
        <translation>Apri Patrono</translation>
    </message>
    <message>
        <location filename="../src/patron_ui.cpp" line="100"/>
        <source>Patron file (*.pat)</source>
        <translation>File Patrono (*.pat)</translation>
    </message>
    <message>
        <location filename="../src/patron_ui.cpp" line="115"/>
        <source>Patron loaded</source>
        <translation>Carica Patrono</translation>
    </message>
</context>
<context>
    <name>PlayerSummary</name>
    <message>
        <location filename="../src/gui/player_summary.ui" line="14"/>
        <source>SummaryPlayer</source>
        <translation>Riassunto Giocatore</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="20"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="32"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="55"/>
        <source>Hope</source>
        <translation>Speranza</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="74"/>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="81"/>
        <source>Shadow</source>
        <translation>Ombra</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="88"/>
        <source>Temporary Load</source>
        <translation>Temporaneamente Affaticato</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="95"/>
        <location filename="../src/gui/player_summary.ui" line="112"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="105"/>
        <source>Fatigue</source>
        <translation>Fatica</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="123"/>
        <source>Guide</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="128"/>
        <source>Hunter</source>
        <translation>Cacciatore</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="133"/>
        <source>Look-out</source>
        <translation>Vedetta</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="138"/>
        <source>Scout</source>
        <translation>Esploratore</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="146"/>
        <source>Features</source>
        <translation>Tratti</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="156"/>
        <source>Travel Role</source>
        <translation>Ruolo di Viaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="163"/>
        <source>Shadow Scars</source>
        <translation>Cicatrice dell&apos;Ombra</translation>
    </message>
</context>
<context>
    <name>PlayerSummaryCombat</name>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="14"/>
        <source>SummaryCombatPlayer</source>
        <translation>Sommario Combattimento giocatore</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="20"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="26"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="45"/>
        <source>Helm</source>
        <translation>Elmo</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="52"/>
        <source>Shield</source>
        <translation>Scudo</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="59"/>
        <source>Wounded</source>
        <translation>Ferito</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="68"/>
        <source>Hope</source>
        <translation>Speranza</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="87"/>
        <source>Parry</source>
        <translation>Difesa</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="94"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
</context>
<context>
    <name>PlayerSummaryCombatUi</name>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="63"/>
        <source>Hope</source>
        <translation>Speranza</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="64"/>
        <source>Hope lost</source>
        <translation>Speranza persa</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="66"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="67"/>
        <source>Endurance lost</source>
        <translation>Resistenza persa</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="122"/>
        <source> (Shadow </source>
        <translation> (Ombra </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="150"/>
        <source> (Load </source>
        <translation> (Ingombro </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="191"/>
        <source>Injury</source>
        <translation>Lesioni</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="192"/>
        <source>Injury description</source>
        <translation>Descrizione della Ferita</translation>
    </message>
</context>
<context>
    <name>PlayerSummaryUi</name>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="58"/>
        <source>wounded, </source>
        <translation>ferito, </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="61"/>
        <source>weary, </source>
        <translation>affaticato, </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="64"/>
        <source>miserable, </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="95"/>
        <source>Hope</source>
        <translation>Speranza</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="96"/>
        <source>Hope lost</source>
        <translation>Speranza persa</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="98"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="99"/>
        <source>Endurance lost</source>
        <translation>Resistenza persa</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="138"/>
        <source>Injury</source>
        <translation>Ferite</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="139"/>
        <source>Injury description</source>
        <translation>Descrizione delle ferite</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="188"/>
        <source> (Shadow </source>
        <translation> (Ombra </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="219"/>
        <source> (Load </source>
        <translation> (Ingombro </translation>
    </message>
</context>
<context>
    <name>PlayerUi</name>
    <message>
        <location filename="../src/player/player_ui.cpp" line="48"/>
        <source>Player ready</source>
        <translation>Giocatore pronto</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="262"/>
        <source>Open Player</source>
        <translation>Giocatore aperto</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="264"/>
        <source>Player file (*.pl)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="279"/>
        <source>Player loaded</source>
        <translation>Giocatore caricato</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="359"/>
        <source>Click me to set an image</source>
        <translation>Premi per impostare un immagine</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="425"/>
        <source>New player created</source>
        <translation>Crea nuovo Giocatore</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="437"/>
        <source>Player removed</source>
        <translation>Giocatore rimosso</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="472"/>
        <source>Player saved</source>
        <translation>Salva Giocatore</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="632"/>
        <source>Open Image</source>
        <translation>Apri Immagine</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="632"/>
        <source>Image (*.jpeg *.jpg *.png)</source>
        <translation>Immagine (*.jpeg *.jpg *.png)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Poor</source>
        <translation>Povero</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Frugal</source>
        <translation>Frugale</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Common</source>
        <translation>Comune</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Prosperous</source>
        <translation>Prospero</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Rich</source>
        <translation>Ricco</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Very Rich</source>
        <translation>Ricchissimo</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="89"/>
        <source>Guide</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="90"/>
        <source>Hunter</source>
        <translation>Cacciatore</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="91"/>
        <source>Lookout</source>
        <translation>Vedetta</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="92"/>
        <source>Scout</source>
        <translation>Esploratore</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="108"/>
        <source>Terrible Misfortune</source>
        <translation>Terribile Sventura</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="109"/>
        <source>Despair</source>
        <translation>Disperazione</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="110"/>
        <source>Ill Choice</source>
        <translation>Scelte Infauste</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="111"/>
        <source>Mishap</source>
        <translation>Contrattempo</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="112"/>
        <source>Shortcut</source>
        <translation>Scorciatoia</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="113"/>
        <source>Chance Meeting</source>
        <translation>Incontro Fortuito</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="114"/>
        <source>Joyful Sight</source>
        <translation>Visione Gioiosa</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="87"/>
        <source>Loremaster-tools ready</source>
        <translation>Strumenti del MdS pronti</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="109"/>
        <source>Open Encounter</source>
        <translation>Apri Incontro</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="111"/>
        <location filename="../src/main_window.cpp" line="142"/>
        <source>Encounter file (*.enc)</source>
        <translation>File incontro (*.inc)</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="123"/>
        <source>Encounter loaded</source>
        <translation>Carica Incontro</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="140"/>
        <source>Save Encounter</source>
        <translation>Salva Incontro</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="153"/>
        <source>Encounter saved</source>
        <translation>Incontro salvato</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="161"/>
        <source>All players are saved</source>
        <translation>Tutti i giocatori sono salvati</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="171"/>
        <source>All players are loaded</source>
        <translation>Tutti i giocatori sono caricati</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="200"/>
        <source>Cannot find translation in </source>
        <translation>Non trovo la traduzione di </translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="208"/>
        <source>Failed to install translation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="222"/>
        <source>Language updated to </source>
        <translation>Lingua caricata di </translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="240"/>
        <source>Change Language</source>
        <translation>Cambia Lingua</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="241"/>
        <source>Language:</source>
        <translation>Lingua:</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="259"/>
        <source>Open Directory</source>
        <translation>Apri Directory</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="271"/>
        <source>Directory set to </source>
        <translation>Directory impostata</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="47"/>
        <source>Favoured</source>
        <translation>Favorito</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="48"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="49"/>
        <source>Ill Favoured</source>
        <translation>Sfavorito</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="28"/>
        <source>Positive</source>
        <translation>Positivo</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="29"/>
        <source>Negative</source>
        <translation>Negativo</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="30"/>
        <source>Both</source>
        <translation>Entrambi</translation>
    </message>
</context>
<context>
    <name>RandomEventUi</name>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="45"/>
        <location filename="../src/tools/random_event_ui.cpp" line="146"/>
        <source>No Role</source>
        <translation>Nessun Ruolo</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="142"/>
        <source>Craft or Explore</source>
        <translation>Manualità o Esplorazione</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="143"/>
        <source>Awareness or Scan</source>
        <translation>Allerta o Scrutare</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="144"/>
        <source>Athletics or Hunting</source>
        <translation>Atletica o Caccia</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="145"/>
        <source>Lore or Travel</source>
        <translation>Storia o Viaggio</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="146"/>
        <source>Travel</source>
        <translation>Viaggio</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="153"/>
        <source>No event found</source>
        <translation>Nessun evento trovato</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="165"/>
        <source>&lt;span style=&quot;font-weight:600;&quot;&gt;No new event.&lt;/span&gt;&lt;br/&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RandomFeatures</name>
    <message>
        <location filename="../src/gui/random_features.ui" line="14"/>
        <source>Random Features</source>
        <translation>Tratti Casuali</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="22"/>
        <source>Type of features</source>
        <translation>Tipi di Tratti</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="40"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="65"/>
        <source>New Features</source>
        <translation>Rigenera</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="72"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>RewardExperience</name>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="14"/>
        <source>Reward Experience</source>
        <translation>Assegna Esperienza</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="30"/>
        <source>Adventure Points</source>
        <translation>Punti Avventura</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="53"/>
        <source>Add Wits</source>
        <translation>Aggiungi Ingegno</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="68"/>
        <source>Skill Points</source>
        <translation>Punti abilità</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="92"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="99"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RollsDialog</name>
    <message>
        <location filename="../src/gui/rolls.ui" line="14"/>
        <source>Roll Analysis</source>
        <translation>Analisi dei tiri</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="36"/>
        <source>Miserable</source>
        <translation>Oppresso</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="46"/>
        <source>Skill Dices</source>
        <translation>Dado Successo</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="56"/>
        <source>Roll Type</source>
        <translation>Tipo di tiro</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="66"/>
        <source>Weary</source>
        <translation>Affaticato</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="83"/>
        <source>Number of Rolls</source>
        <translation>Numero di tiri</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="99"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="124"/>
        <source>Reset Axes</source>
        <translation>Azzera gli assi</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="134"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>RollsProbabilitiesUi</name>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="99"/>
        <source>Target Number</source>
        <translation>Tiro Necessario</translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="100"/>
        <source>Probability</source>
        <translation>Probabilità</translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="104"/>
        <source>Average value: </source>
        <translation>Valore medio: </translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="105"/>
        <source>Average number of 6: </source>
        <translation>Valore medio di 6: </translation>
    </message>
</context>
<context>
    <name>Ruins</name>
    <message>
        <location filename="../src/gui/ruins.ui" line="20"/>
        <source>Ruins Generator</source>
        <translation>Generatore di Rovine</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="34"/>
        <source>Original Purpose</source>
        <translation>Scopo Originario</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="47"/>
        <source>Built By</source>
        <translation>Costruito da</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="54"/>
        <location filename="../src/gui/ruins.ui" line="61"/>
        <location filename="../src/gui/ruins.ui" line="81"/>
        <location filename="../src/gui/ruins.ui" line="88"/>
        <source>Text</source>
        <translation>Testo</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="74"/>
        <source>Current Look</source>
        <translation>Aspetto Attuale</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="101"/>
        <source>Current Usage</source>
        <translation>Utilizzo Attuale</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="108"/>
        <source>See &quot;Journeys and Maps&quot; p. 25 for more details</source>
        <translation>Vedi &quot;viaggi e mappe&quot; p. 25 per maggiori dettagli</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="132"/>
        <source>New</source>
        <translation>Nuovo</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="139"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>ServerLog</name>
    <message>
        <location filename="../src/gui/server_logs.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/server_logs.ui" line="35"/>
        <source>Log Level</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ShuffleListDialog</name>
    <message>
        <location filename="../src/gui/shuffle.ui" line="14"/>
        <location filename="../src/gui/shuffle.ui" line="52"/>
        <source>Shuffle List</source>
        <translation>Lista Casuale</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="38"/>
        <source>Save List</source>
        <translation>Salva lista</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="45"/>
        <source>Load List</source>
        <translation>Carica Lista</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="59"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ShuffleListUi</name>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="41"/>
        <source>Save list</source>
        <translation>Salva lista</translation>
    </message>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="43"/>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="64"/>
        <source>List file (*.list)</source>
        <translation>Lista di file (*.lista)</translation>
    </message>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="62"/>
        <source>Open List</source>
        <translation>Apri Lista</translation>
    </message>
</context>
<context>
    <name>TheOneRing</name>
    <message>
        <location filename="../src/gui/main_window.ui" line="14"/>
        <source>TheOneRing</source>
        <translation>Unico Anello</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="37"/>
        <location filename="../src/gui/main_window.ui" line="89"/>
        <source>Players</source>
        <translation>Giocatori</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="42"/>
        <location filename="../src/gui/main_window.ui" line="97"/>
        <source>Adversaries</source>
        <translation>Avversari</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="47"/>
        <source>Combat Overview</source>
        <translation>Panoramica di Combattimento</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="52"/>
        <source>Overview</source>
        <translation>Panoramica</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="57"/>
        <source>Patron</source>
        <translation>Patrono</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="62"/>
        <source>Server Logs</source>
        <translation>Registro Server</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="80"/>
        <source>Menu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="104"/>
        <source>Tools</source>
        <translation>Strumenti</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="115"/>
        <source>View</source>
        <translation>Visualizza</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="129"/>
        <source>Save All Players</source>
        <translation>Salva tutti i Giocatori</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="134"/>
        <source>Set Directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="139"/>
        <source>Load All Players</source>
        <translation>Carica tutti i Personaggi</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="144"/>
        <source>Save Encounter</source>
        <translation>Salva Incontro</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="149"/>
        <source>Load Encounter</source>
        <translation>Carica Incontro</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="154"/>
        <source>Name Generator</source>
        <translation>Generatore di nomi</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="159"/>
        <source>Random Event</source>
        <translation>Eventi casuali</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="164"/>
        <source>Random Features</source>
        <translation>Tratti Casuali</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="169"/>
        <source>Rolls Probabilities</source>
        <translation>Tiro probabilità</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="174"/>
        <source>Toggle Player Skills </source>
        <translation>Nascondi Abilità Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="179"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="184"/>
        <source>Toggle Player Image</source>
        <translation>Nascondi Immagine Giocatore</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="189"/>
        <source>Shuffle List</source>
        <translation>Lista casuale</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="194"/>
        <source>Start / Stop Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="199"/>
        <source>Reward experience</source>
        <translation>Assegna Esperienza</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="204"/>
        <source>Update CORS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="209"/>
        <source>Ruins Generator</source>
        <translation>Generatore di Rovine</translation>
    </message>
</context>
<context>
    <name>adversary</name>
    <message>
        <location filename="../src/gui/adversary.ui" line="14"/>
        <source>Adversary</source>
        <translation>Avversario</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="71"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="78"/>
        <source>Attribute Level</source>
        <translation>Livello di Attributo</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="85"/>
        <source>Characteristics</source>
        <translation>Tratti</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="127"/>
        <source>Use Hate</source>
        <translation>Uso Odio</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="151"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="158"/>
        <source>Parry</source>
        <translation>Parata</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="168"/>
        <source>Might</source>
        <translation>Possanza</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="175"/>
        <source>Hate / Resolve</source>
        <translation>Odio / Risolutezza</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="182"/>
        <source>Armour</source>
        <translation>Armatura</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="205"/>
        <source>Combat Proficiencies</source>
        <translation>Competenze di Combattimento</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="212"/>
        <source>Fell Abilities</source>
        <translation>Capacità Funeste</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="243"/>
        <source>New Character</source>
        <translation>Nuovo Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="250"/>
        <source>Save Current Character</source>
        <translation>Salva Personaggio corrente</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="257"/>
        <source>Load Character</source>
        <translation>Carica Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="264"/>
        <source>Remove Character</source>
        <translation>Rimuovi Personaggio</translation>
    </message>
</context>
<context>
    <name>combat_overview</name>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="14"/>
        <source>Combat Overview</source>
        <translation>Panoramica di Combattimento</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="47"/>
        <source>Fellowship points</source>
        <translation>Punti Compagnia</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="87"/>
        <source>Players</source>
        <translation>Giocatori</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="94"/>
        <source>Adversaries</source>
        <translation>Avversari</translation>
    </message>
</context>
<context>
    <name>name_generator</name>
    <message>
        <location filename="../src/gui/name_generator.ui" line="14"/>
        <source>Name Generator</source>
        <translation>Generatore di Nomi</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="22"/>
        <source>Race</source>
        <translation>Razza</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="34"/>
        <source>Gender</source>
        <translation>Genere</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="40"/>
        <source>Male</source>
        <translation>Maschile</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="53"/>
        <source>Female</source>
        <translation>Femminile</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="81"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="96"/>
        <source>Generation thanks to fantasynamegenerators.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="124"/>
        <source>New Names</source>
        <translation>Nuovo nome</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="131"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
</context>
<context>
    <name>overview</name>
    <message>
        <location filename="../src/gui/overview.ui" line="14"/>
        <source>Overview</source>
        <translation>Panoramica</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="22"/>
        <source>Eye awareness</source>
        <translation>Percezione dell&apos;Occhio</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="32"/>
        <source>Fellowship Points</source>
        <translation>Punti Compagnia</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="42"/>
        <source>Season load</source>
        <translation>Carica Stagione</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="75"/>
        <source>Players</source>
        <translation>Giocatori</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="101"/>
        <source>Reset Fellowship Points</source>
        <translation>Reset Punti Compagnia</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="108"/>
        <source>Short Rest</source>
        <translation>Riposo Breve</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="115"/>
        <source>Long Rest</source>
        <translation>Riposo Prolungato</translation>
    </message>
</context>
<context>
    <name>player</name>
    <message>
        <location filename="../src/gui/player.ui" line="14"/>
        <location filename="../src/gui/player.ui" line="60"/>
        <source>Player</source>
        <translation>Giocatore</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="88"/>
        <source>Shadow Path</source>
        <translation>Discesa nell&apos;Ombra</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="95"/>
        <source>Standard of Living</source>
        <translation>Tenore di Vita</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="115"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="122"/>
        <source>Culture</source>
        <translation>Cultura</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="129"/>
        <source>Calling</source>
        <translation>Vocazione</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="142"/>
        <source>Click me to set an image</source>
        <translation>Premi per impostare un immagine</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="152"/>
        <source>Distinctive Features</source>
        <translation>Tratti Distintivi</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="279"/>
        <location filename="../src/gui/player.ui" line="576"/>
        <location filename="../src/gui/player.ui" line="815"/>
        <source>Target Number</source>
        <translation>Tiro Necessario</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="225"/>
        <source>Strength</source>
        <translation>Forza</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="292"/>
        <source>Endurance</source>
        <translation>Resistenza</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1250"/>
        <location filename="../src/gui/player.ui" line="1284"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="353"/>
        <source>Awareness</source>
        <translation>Allerta</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="339"/>
        <source>Awe</source>
        <translation>Intimorire</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="360"/>
        <source>Athletics</source>
        <translation>Atletica</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="381"/>
        <source>Song</source>
        <translation>Canzoni</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="367"/>
        <source>Craft</source>
        <translation>Manualità</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="266"/>
        <location filename="../src/gui/player.ui" line="563"/>
        <location filename="../src/gui/player.ui" line="866"/>
        <source>78</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="332"/>
        <source>Hunting</source>
        <translation>Caccia</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="406"/>
        <source>Axes</source>
        <translation>Asce</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="441"/>
        <source>Bows</source>
        <translation>Archi</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="448"/>
        <source>Spears</source>
        <translation>Lance</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="455"/>
        <source>Swords</source>
        <translation>Spade</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="522"/>
        <source>Heart</source>
        <translation>Cuore</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="589"/>
        <source>Hope</source>
        <translation>Speranza</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="650"/>
        <source>Enhearten</source>
        <translation>Incoraggiare</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="657"/>
        <source>Travel</source>
        <translation>Viaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="664"/>
        <source>Insight</source>
        <translation>Perspicacia</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="671"/>
        <source>Healing</source>
        <translation>Guarigione</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="678"/>
        <source>Battle</source>
        <translation>Battaglia</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="685"/>
        <source>Courtesy</source>
        <translation>Cortesia</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="703"/>
        <source>Valour</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="799"/>
        <source>Wits</source>
        <translation>Ingegno</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="828"/>
        <location filename="../src/gui/player.ui" line="1103"/>
        <source>Parry</source>
        <translation>Difesa</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="916"/>
        <source>Persuade</source>
        <translation>Persuasione</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="923"/>
        <source>Stealth</source>
        <translation>Furtività</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="930"/>
        <source>Scan</source>
        <translation>Scrutare</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="937"/>
        <source>Lore</source>
        <translation>Sapienza</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="951"/>
        <source>Explore</source>
        <translation>Esplorazione</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="965"/>
        <source>Riddle</source>
        <translation>Indovinelli</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="983"/>
        <source>Wisdom</source>
        <translation>Saggezza</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1033"/>
        <source>Temporary</source>
        <translation>Temporaneo</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1052"/>
        <source>Load</source>
        <translation>Ingombro</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1062"/>
        <source>Weapons</source>
        <translation>Armi</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1072"/>
        <source>Armour</source>
        <translation>Armatura</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1079"/>
        <source>Protection</source>
        <translation>Protezione</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1086"/>
        <source>Helm</source>
        <translation>Elmo</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1096"/>
        <source>Shield</source>
        <translation>Scudo</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1141"/>
        <source>Wounded</source>
        <translation>Ferito</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1166"/>
        <source>Injury</source>
        <translation>Lesioni</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1191"/>
        <source>Adventure Points</source>
        <translation>Punti Avventura</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1198"/>
        <source>Skill Points</source>
        <translation>Punti Abilità</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1205"/>
        <source>Current</source>
        <translation>Corrente</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1212"/>
        <source>Total</source>
        <translation>Totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1243"/>
        <source>Current Treasures</source>
        <translation>Tesoro</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1257"/>
        <source>Total Load</source>
        <translation>Ingombro Totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1264"/>
        <source>Shadow</source>
        <translation>Ombra</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1271"/>
        <source>Total Protection</source>
        <translation>Protezione totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1291"/>
        <source>Total Treasures</source>
        <translation>Tesoro Totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1311"/>
        <source>Shadow Scars</source>
        <translation>Cicatrice dell&apos;Ombra</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1328"/>
        <source>Lock Player</source>
        <translation>Blocca Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1348"/>
        <source>New Character</source>
        <translation>Nuovo Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1355"/>
        <source>Save Current Character</source>
        <translation>Salva Personaggio corrente</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1362"/>
        <source>Load Character</source>
        <translation>Carica Personaggio</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1369"/>
        <source>Remove Character</source>
        <translation>Rimuovi Personaggio</translation>
    </message>
</context>
</TS>
