<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AdversarySummary</name>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="14"/>
        <source>SummaryAdversary</source>
        <translation>Résumé Adversaire</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="20"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="26"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="43"/>
        <source>Hate / Resolve</source>
        <translation>Haine / Résolution</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="62"/>
        <location filename="../src/gui/adversary_summary.ui" line="90"/>
        <location filename="../src/gui/adversary_summary.ui" line="97"/>
        <location filename="../src/gui/adversary_summary.ui" line="111"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="69"/>
        <source>Attribute Level</source>
        <translation>Niveau d&apos;attribut</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="76"/>
        <source>Armor</source>
        <translation>Armure</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="83"/>
        <source>Parry</source>
        <translation>Parade</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary_summary.ui" line="104"/>
        <source>Combat Proficiencies</source>
        <translation>Compétences de combat</translation>
    </message>
</context>
<context>
    <name>AdversarySummaryUi</name>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="49"/>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="74"/>
        <source>Hate</source>
        <translation>Haine</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="52"/>
        <source>Resolve</source>
        <translation>Résolution</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="75"/>
        <source>Hate lost</source>
        <translation>Haine perdue</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="77"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_summary_ui.cpp" line="78"/>
        <source>Endurance lost</source>
        <translation>Endurance perdue</translation>
    </message>
</context>
<context>
    <name>AdversaryUi</name>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="40"/>
        <source>Adversary ready</source>
        <translation>Adversaire prêt</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="113"/>
        <source>Adversary removed</source>
        <translation>Adversaire enlevé</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="131"/>
        <source>Open Adversary</source>
        <translation>Ouvrir un adversaire</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="133"/>
        <source>Adversary file (*.adv)</source>
        <translation>Fichier d&apos;adversaire (*.adv)</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="148"/>
        <source>Adversary loaded</source>
        <translation>Adversaire chargé</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="157"/>
        <source>New adversary created</source>
        <translation>Nouvel adversaire crée</translation>
    </message>
    <message>
        <location filename="../src/adversary/adversary_ui.cpp" line="192"/>
        <source>Adversary saved</source>
        <translation>Adversaire sauvegardé</translation>
    </message>
</context>
<context>
    <name>EventDialog</name>
    <message>
        <location filename="../src/gui/event.ui" line="14"/>
        <source>Event</source>
        <translation>Evénement</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="22"/>
        <source>Role</source>
        <translation>Rôle</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="36"/>
        <source>Event Type</source>
        <translation>Type d&apos;événement</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="63"/>
        <source>Event Title</source>
        <translation>Titre de l&apos;événement</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="70"/>
        <source>Recommended Skills</source>
        <translation>Compétence recommandée</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="97"/>
        <source>Roll</source>
        <translation>Evénement aléatoire</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="104"/>
        <source>Save Event Done</source>
        <translation>Sauvegarder l&apos;événement</translation>
    </message>
    <message>
        <location filename="../src/gui/event.ui" line="111"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/main_window.cpp" line="344"/>
        <source>Update CORS</source>
        <translation>Mettre à jour les CORS</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="345"/>
        <source>New CORS:</source>
        <translation>Nouveau CORS:</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="355"/>
        <source>Restart needed</source>
        <translation>Redémarrage requis</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="356"/>
        <source>The update of the CORS rule will be taken into account next time you start this software.</source>
        <translation>La mise à jour des règles CORS va être prise en compte au prochain démarrage.</translation>
    </message>
</context>
<context>
    <name>NameGeneratorUi</name>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="28"/>
        <source>Balrog</source>
        <translation>Balrog</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="29"/>
        <source>Bree</source>
        <translation>Bree</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="31"/>
        <source>Beorning / Woodmen</source>
        <translation>Beorning / Homme des bois</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="32"/>
        <source>Dale</source>
        <translation>Dale</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="34"/>
        <source>Druedain</source>
        <translation>Druedain</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="36"/>
        <source>Dunlending</source>
        <translation>Dunlending</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="37"/>
        <source>Dwarf</source>
        <translation>Nain</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="39"/>
        <source>Easterling</source>
        <translation>Easterling</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="41"/>
        <source>Haradrim</source>
        <translation>Haradrim</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="43"/>
        <source>Hobbit</source>
        <translation>Hobbit</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="44"/>
        <source>Maiar</source>
        <translation>Maiar</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="45"/>
        <source>Orc</source>
        <translation>Orc</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="47"/>
        <source>Quenya</source>
        <translation>Quenya</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="49"/>
        <source>Rohirrim</source>
        <translation>Rohirrim</translation>
    </message>
    <message>
        <location filename="../src/tools/name_generator_ui.cpp" line="51"/>
        <source>Sindarin</source>
        <translation>Sindarin</translation>
    </message>
</context>
<context>
    <name>Patron</name>
    <message>
        <location filename="../src/gui/patron.ui" line="14"/>
        <source>Patron</source>
        <translation>Mécène</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="22"/>
        <source>Fellowship points</source>
        <translation>Point de communauté</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="51"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="64"/>
        <source>Advantages</source>
        <translation>Avantages</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="91"/>
        <source>New Patron</source>
        <translation>Nouveau mécène</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="98"/>
        <source>Save Patron</source>
        <translation>Sauvegarder mécène</translation>
    </message>
    <message>
        <location filename="../src/gui/patron.ui" line="105"/>
        <source>Load Patron</source>
        <translation>Charger mécène</translation>
    </message>
</context>
<context>
    <name>PatronUi</name>
    <message>
        <location filename="../src/patron_ui.cpp" line="98"/>
        <source>Open Patron</source>
        <translation>Ouvrir un mécène</translation>
    </message>
    <message>
        <location filename="../src/patron_ui.cpp" line="100"/>
        <source>Patron file (*.pat)</source>
        <translation>Fichier de mécène</translation>
    </message>
    <message>
        <location filename="../src/patron_ui.cpp" line="115"/>
        <source>Patron loaded</source>
        <translation>Mécène chargé</translation>
    </message>
</context>
<context>
    <name>PlayerSummary</name>
    <message>
        <location filename="../src/gui/player_summary.ui" line="14"/>
        <source>SummaryPlayer</source>
        <translation>Résumé de joueur</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="20"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="32"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="55"/>
        <source>Hope</source>
        <translation>Espoir</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="74"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="81"/>
        <source>Shadow</source>
        <translation>Ombre</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="88"/>
        <source>Temporary Load</source>
        <translation>Charge temporaire</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="95"/>
        <location filename="../src/gui/player_summary.ui" line="112"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="105"/>
        <source>Fatigue</source>
        <translation>Fatigue</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="123"/>
        <source>Guide</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="128"/>
        <source>Hunter</source>
        <translation>Chasseur</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="133"/>
        <source>Look-out</source>
        <translation>Guet</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="138"/>
        <source>Scout</source>
        <translation>Scoute</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="146"/>
        <source>Features</source>
        <translation>Caractéristiques</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="156"/>
        <source>Travel Role</source>
        <translation>Rôle pour le voyage</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary.ui" line="163"/>
        <source>Shadow Scars</source>
        <translation>Cicatrice d&apos;ombre</translation>
    </message>
</context>
<context>
    <name>PlayerSummaryCombat</name>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="14"/>
        <source>SummaryCombatPlayer</source>
        <translation>Résumé de combat</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="20"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="26"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="45"/>
        <source>Helm</source>
        <translation>Casque</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="52"/>
        <source>Shield</source>
        <translation>Bouclier</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="59"/>
        <source>Wounded</source>
        <translation>Blessure</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="68"/>
        <source>Hope</source>
        <translation>Espoir</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="87"/>
        <source>Parry</source>
        <translation>Parade</translation>
    </message>
    <message>
        <location filename="../src/gui/player_summary_combat.ui" line="94"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlayerSummaryCombatUi</name>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="63"/>
        <source>Hope</source>
        <translation>Espoir</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="64"/>
        <source>Hope lost</source>
        <translation>Espoir perdu</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="66"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="67"/>
        <source>Endurance lost</source>
        <translation>Endurance perdue</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="122"/>
        <source> (Shadow </source>
        <translation> (Ombre </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="150"/>
        <source> (Load </source>
        <translation> (Charge </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="191"/>
        <source>Injury</source>
        <translation>Blessure</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_combat_ui.cpp" line="192"/>
        <source>Injury description</source>
        <translation>Description de blessure</translation>
    </message>
</context>
<context>
    <name>PlayerSummaryUi</name>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="58"/>
        <source>wounded, </source>
        <translation>blessé, </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="61"/>
        <source>weary, </source>
        <translation>fatigué, </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="64"/>
        <source>miserable, </source>
        <translation>misérable, </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="95"/>
        <source>Hope</source>
        <translation>Espoir</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="96"/>
        <source>Hope lost</source>
        <translation>Espoir perdu</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="98"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="99"/>
        <source>Endurance lost</source>
        <translation>Endurance perdue</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="138"/>
        <source>Injury</source>
        <translation>Blessure</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="139"/>
        <source>Injury description</source>
        <translation>Description de blessure</translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="188"/>
        <source> (Shadow </source>
        <translation> (Ombre </translation>
    </message>
    <message>
        <location filename="../src/player/player_summary_ui.cpp" line="219"/>
        <source> (Load </source>
        <translation> (Charge </translation>
    </message>
</context>
<context>
    <name>PlayerUi</name>
    <message>
        <location filename="../src/player/player_ui.cpp" line="48"/>
        <source>Player ready</source>
        <translation>Joueur prêt</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="262"/>
        <source>Open Player</source>
        <translation>Ouvrir un joueur</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="264"/>
        <source>Player file (*.pl)</source>
        <translation>Fichier de joueur (*.pl)</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="279"/>
        <source>Player loaded</source>
        <translation>Joueur chargé</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="359"/>
        <source>Click me to set an image</source>
        <translation>Clique sur moi 
pour mettre une image</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="425"/>
        <source>New player created</source>
        <translation>Nouveau joueur crée</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="437"/>
        <source>Player removed</source>
        <translation>Joueur enlevé</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="472"/>
        <source>Player saved</source>
        <translation>Joueur sauvegardé</translation>
    </message>
    <message>
        <source>All players are saved</source>
        <translation type="vanished">Tout les joueurs sont sauvegardés</translation>
    </message>
    <message>
        <source>All players are loaded</source>
        <translation type="vanished">Tout les joueurs sont chargés</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="632"/>
        <source>Open Image</source>
        <translation>Ouvrir image</translation>
    </message>
    <message>
        <location filename="../src/player/player_ui.cpp" line="632"/>
        <source>Image (*.jpeg *.jpg *.png)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Adversary ready</source>
        <translation type="vanished">Adversaire prêt</translation>
    </message>
    <message>
        <source>Adversary removed</source>
        <translation type="vanished">Adversaire enlevé</translation>
    </message>
    <message>
        <source>Open Adversary</source>
        <translation type="vanished">Ouvrir un adversaire</translation>
    </message>
    <message>
        <source>Adversary file (*.adv)</source>
        <translation type="vanished">Fichier d&apos;adversaire (*.adv)</translation>
    </message>
    <message>
        <source>Adversary loaded</source>
        <translation type="vanished">Adversaire chargé</translation>
    </message>
    <message>
        <source>New adversary created</source>
        <translation type="vanished">Nouvel adversaire crée</translation>
    </message>
    <message>
        <source>Adversary saved</source>
        <translation type="vanished">Adversaire sauvegardé</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="109"/>
        <source>Open Encounter</source>
        <translation>Ouvrir une rencontre</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="111"/>
        <location filename="../src/main_window.cpp" line="142"/>
        <source>Encounter file (*.enc)</source>
        <translation>Fichier de rencontre (*.enc)</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="123"/>
        <source>Encounter loaded</source>
        <translation>Rencontre chargée</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="140"/>
        <source>Save Encounter</source>
        <translation>Sauvegarder rencontre</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="153"/>
        <source>Encounter saved</source>
        <translation>Rencontre sauvegardée</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="87"/>
        <source>Loremaster-tools ready</source>
        <translation>Loremaster-tools est prêt</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="161"/>
        <source>All players are saved</source>
        <translation>Tout les joueurs sont sauvegardés</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="171"/>
        <source>All players are loaded</source>
        <translation>Tout les joueurs sont chargés</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="200"/>
        <source>Cannot find translation in </source>
        <translation>Impossible de trouver la traduction dans </translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="208"/>
        <source>Failed to install translation</source>
        <translation>Erreur lors de l&apos;installation de la traduction</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="222"/>
        <source>Language updated to </source>
        <translation>Langues changée pour </translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="240"/>
        <source>Change Language</source>
        <translation>Changer la langue</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="241"/>
        <source>Language:</source>
        <translation>Langue:</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="259"/>
        <source>Open Directory</source>
        <translation>Ouvrir un dossier</translation>
    </message>
    <message>
        <location filename="../src/main_window.cpp" line="271"/>
        <source>Directory set to </source>
        <translation>Dossier mis à </translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Poor</source>
        <translation>Pauvre</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Frugal</source>
        <translation>Frugale</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="75"/>
        <source>Common</source>
        <translation>Commun</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Prosperous</source>
        <translation>Prospère</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Rich</source>
        <translation>Riche</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="76"/>
        <source>Very Rich</source>
        <translation>Très riche</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="89"/>
        <source>Guide</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="90"/>
        <source>Hunter</source>
        <translation>Chasseur</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="91"/>
        <source>Lookout</source>
        <translation>Guet</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="92"/>
        <source>Scout</source>
        <translation>Eclaireur</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="108"/>
        <source>Terrible Misfortune</source>
        <translation>Terrible Malheur</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="109"/>
        <source>Despair</source>
        <translation>Désespoir</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="110"/>
        <source>Ill Choice</source>
        <translation>Mauvais choix</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="111"/>
        <source>Mishap</source>
        <translation>Mésaventure</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="112"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="113"/>
        <source>Chance Meeting</source>
        <translation>Rencontre chanceuse</translation>
    </message>
    <message>
        <location filename="../src/enums.hpp" line="114"/>
        <source>Joyful Sight</source>
        <translation>Vue joyeuse</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="47"/>
        <source>Favoured</source>
        <translation>Favorisé</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="48"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/random.hpp" line="49"/>
        <source>Ill Favoured</source>
        <translation>Désavantagé</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="28"/>
        <source>Positive</source>
        <translation>Positive</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="29"/>
        <source>Negative</source>
        <translation>Negative</translation>
    </message>
    <message>
        <location filename="../src/tools/random_features_ui.hpp" line="30"/>
        <source>Both</source>
        <translation>Les deux</translation>
    </message>
</context>
<context>
    <name>RandomEventUi</name>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="45"/>
        <location filename="../src/tools/random_event_ui.cpp" line="146"/>
        <source>No Role</source>
        <translation>Aucun role</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="142"/>
        <source>Craft or Explore</source>
        <translation>Artisanat ou explorer</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="143"/>
        <source>Awareness or Scan</source>
        <translation>Conscience</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="144"/>
        <source>Athletics or Hunting</source>
        <translation>Athlétique ou chasse</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="145"/>
        <source>Lore or Travel</source>
        <translation>Traditions ou voyage</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="146"/>
        <source>Travel</source>
        <translation>Voyage</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="153"/>
        <source>No event found</source>
        <translation>Aucun événement trouvé</translation>
    </message>
    <message>
        <location filename="../src/tools/random_event_ui.cpp" line="165"/>
        <source>&lt;span style=&quot;font-weight:600;&quot;&gt;No new event.&lt;/span&gt;&lt;br/&gt;</source>
        <translation>&lt;span style=&quot;font-weight:600;&quot;&gt;Aucun nouvel événement.&lt;/span&gt;&lt;br/&gt;</translation>
    </message>
</context>
<context>
    <name>RandomFeatures</name>
    <message>
        <location filename="../src/gui/random_features.ui" line="14"/>
        <source>Random Features</source>
        <translation>Caractéristiques aléatoires</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="22"/>
        <source>Type of features</source>
        <translation>Sorte de caractéristiques</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="40"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="65"/>
        <source>New Features</source>
        <translation>Nouvelles caractéristiques</translation>
    </message>
    <message>
        <location filename="../src/gui/random_features.ui" line="72"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>RewardExperience</name>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="14"/>
        <source>Reward Experience</source>
        <translation>Récompenser de l&apos;expérience</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="30"/>
        <source>Adventure Points</source>
        <translation>Points d&apos;aventure</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="53"/>
        <source>Add Wits</source>
        <translation>Ajouter l&apos;esprit</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="68"/>
        <source>Skill Points</source>
        <translation>Points de compétences</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="92"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/gui/reward_experience.ui" line="99"/>
        <source>Ok</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>RollsDialog</name>
    <message>
        <location filename="../src/gui/rolls.ui" line="14"/>
        <source>Roll Analysis</source>
        <translation>Analyse de jet</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="36"/>
        <source>Miserable</source>
        <translation>Misérable</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="46"/>
        <source>Skill Dices</source>
        <translation>Dés de skill</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="56"/>
        <source>Roll Type</source>
        <translation>Type de lancé</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="66"/>
        <source>Weary</source>
        <translation>Fatigué</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="83"/>
        <source>Number of Rolls</source>
        <translation>Nombre de lancé</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="99"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="124"/>
        <source>Reset Axes</source>
        <translation>Réinitialiser les axes</translation>
    </message>
    <message>
        <source>Skill dices</source>
        <translation type="vanished">Dés de compétences</translation>
    </message>
    <message>
        <source>The vertical lines represent the average value obtained</source>
        <translation type="vanished">Les lines verticales représentes la valeur moyenne obtenue</translation>
    </message>
    <message>
        <location filename="../src/gui/rolls.ui" line="134"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>RollsProbabilitiesUi</name>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="99"/>
        <source>Target Number</source>
        <translation>Nombre cible</translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="100"/>
        <source>Probability</source>
        <translation>Probabilité</translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="104"/>
        <source>Average value: </source>
        <translation>Valeur moyenne: </translation>
    </message>
    <message>
        <location filename="../src/tools/rolls_probabilities_ui.cpp" line="105"/>
        <source>Average number of 6: </source>
        <translation>Nombre de 6 moyen: </translation>
    </message>
</context>
<context>
    <name>Ruins</name>
    <message>
        <location filename="../src/gui/ruins.ui" line="20"/>
        <source>Ruins Generator</source>
        <translation>Générateur de ruines</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="34"/>
        <source>Original Purpose</source>
        <translation>Utilité originale</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="47"/>
        <source>Built By</source>
        <translation>Construit par</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="54"/>
        <location filename="../src/gui/ruins.ui" line="61"/>
        <location filename="../src/gui/ruins.ui" line="81"/>
        <location filename="../src/gui/ruins.ui" line="88"/>
        <source>Text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="74"/>
        <source>Current Look</source>
        <translation>Appareance actuelle</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="101"/>
        <source>Current Usage</source>
        <translation>Utilisation actuelle</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="108"/>
        <source>See &quot;Journeys and Maps&quot; p. 25 for more details</source>
        <translation>Voir &quot;Journeys and Maps&quot; p.25 pour plus de détails</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="132"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../src/gui/ruins.ui" line="139"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>ServerLog</name>
    <message>
        <location filename="../src/gui/server_logs.ui" line="14"/>
        <source>Form</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../src/gui/server_logs.ui" line="35"/>
        <source>Log Level</source>
        <translation>Niveau de log</translation>
    </message>
</context>
<context>
    <name>ShuffleListDialog</name>
    <message>
        <location filename="../src/gui/shuffle.ui" line="14"/>
        <location filename="../src/gui/shuffle.ui" line="52"/>
        <source>Shuffle List</source>
        <translation>Mélangeur de liste</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="38"/>
        <source>Save List</source>
        <translation>Sauvegarder la liste</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="45"/>
        <source>Load List</source>
        <translation>Charger la liste</translation>
    </message>
    <message>
        <location filename="../src/gui/shuffle.ui" line="59"/>
        <source>Ok</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>ShuffleListUi</name>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="41"/>
        <source>Save list</source>
        <translation>Sauvegarder la liste</translation>
    </message>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="43"/>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="64"/>
        <source>List file (*.list)</source>
        <translation>Fichier de liste (*.list)</translation>
    </message>
    <message>
        <location filename="../src/tools/shuffle_list_ui.cpp" line="62"/>
        <source>Open List</source>
        <translation>Ouvrir une liste</translation>
    </message>
</context>
<context>
    <name>TheOneRing</name>
    <message>
        <location filename="../src/gui/main_window.ui" line="14"/>
        <source>TheOneRing</source>
        <translation>L&apos;anneau unique</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="37"/>
        <location filename="../src/gui/main_window.ui" line="89"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="42"/>
        <location filename="../src/gui/main_window.ui" line="97"/>
        <source>Adversaries</source>
        <translation>Adversaires</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="47"/>
        <source>Combat Overview</source>
        <translation>Vue de combat</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="52"/>
        <source>Overview</source>
        <translation>Vue générale</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="57"/>
        <source>Patron</source>
        <translation>Mécène</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="62"/>
        <source>Server Logs</source>
        <translation>Logs du serveur</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="80"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="104"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="115"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="129"/>
        <source>Save All Players</source>
        <translation>Sauvegarder les joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="134"/>
        <source>Set Directory</source>
        <translation>Choisir le dossier</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="139"/>
        <source>Load All Players</source>
        <translation>Charger tout les joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="144"/>
        <source>Save Encounter</source>
        <translation>Sauvegarder la rencontre</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="149"/>
        <source>Load Encounter</source>
        <translation>Charger la rencontre</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="154"/>
        <source>Name Generator</source>
        <translation>Générateur de nom</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="159"/>
        <source>Random Event</source>
        <translation>Evénement aléatoire</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="164"/>
        <source>Random Features</source>
        <translation>Caractéristiques aléatoires</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="169"/>
        <source>Rolls Probabilities</source>
        <translation>Probabilité de jet</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="174"/>
        <source>Toggle Player Skills </source>
        <translation>Montrer / Cacher compétences de joueurs </translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="179"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="184"/>
        <source>Toggle Player Image</source>
        <translation>Montrer / Cacher les images de joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="189"/>
        <source>Shuffle List</source>
        <translation>Mélangeur de listes</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="194"/>
        <source>Start / Stop Server</source>
        <translation>Lancer / arrêter le serveur</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="199"/>
        <source>Reward experience</source>
        <translation>Récompenser de l&apos;expérience</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="204"/>
        <source>Update CORS</source>
        <translation>Mettre à jour les CORS</translation>
    </message>
    <message>
        <location filename="../src/gui/main_window.ui" line="209"/>
        <source>Ruins Generator</source>
        <translation>Générateur de ruines</translation>
    </message>
</context>
<context>
    <name>adversary</name>
    <message>
        <location filename="../src/gui/adversary.ui" line="14"/>
        <source>Adversary</source>
        <translation>Adversaire</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="71"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="78"/>
        <source>Attribute Level</source>
        <translation>Niveau d&apos;attribut</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="85"/>
        <source>Characteristics</source>
        <translation>Caractéristiques</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="127"/>
        <source>Use Hate</source>
        <translation>Utilise haine</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="151"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="158"/>
        <source>Parry</source>
        <translation>Parade</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="168"/>
        <source>Might</source>
        <translation>Puissance</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="175"/>
        <source>Hate / Resolve</source>
        <translation>Haine / Résolution</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="182"/>
        <source>Armour</source>
        <translation>Armure</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="205"/>
        <source>Combat Proficiencies</source>
        <translation>Compétences de combat</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="212"/>
        <source>Fell Abilities</source>
        <translation>Abilité féroce</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="243"/>
        <source>New Character</source>
        <translation>Nouveau personnage</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="250"/>
        <source>Save Current Character</source>
        <translation>Sauvegarder personnage actuel</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="257"/>
        <source>Load Character</source>
        <translation>Charger personnage</translation>
    </message>
    <message>
        <location filename="../src/gui/adversary.ui" line="264"/>
        <source>Remove Character</source>
        <translation>Enlever personnage</translation>
    </message>
</context>
<context>
    <name>combat_overview</name>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="14"/>
        <source>Combat Overview</source>
        <translation>Vue de combat</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="47"/>
        <source>Fellowship points</source>
        <translation>Point de communauté</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="87"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/combat_overview.ui" line="94"/>
        <source>Adversaries</source>
        <translation>Adversaires</translation>
    </message>
</context>
<context>
    <name>name_generator</name>
    <message>
        <location filename="../src/gui/name_generator.ui" line="14"/>
        <source>Name Generator</source>
        <translation>Générateur de noms</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="22"/>
        <source>Race</source>
        <translation>Race</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="34"/>
        <source>Gender</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="40"/>
        <source>Male</source>
        <translation>Masculin</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="53"/>
        <source>Female</source>
        <translation>Feminin</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="81"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="96"/>
        <source>Generation thanks to fantasynamegenerators.com</source>
        <translation>Génération grâce a fantasynamegenerators.com</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="124"/>
        <source>New Names</source>
        <translation>Nouveaux noms</translation>
    </message>
    <message>
        <location filename="../src/gui/name_generator.ui" line="131"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
</context>
<context>
    <name>overview</name>
    <message>
        <location filename="../src/gui/overview.ui" line="14"/>
        <source>Overview</source>
        <translation>Vue générale</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="22"/>
        <source>Eye awareness</source>
        <translation>Conscience de l&apos;oeil</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="32"/>
        <source>Fellowship Points</source>
        <translation>Points de communauté</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="42"/>
        <source>Season load</source>
        <translation>Charge de saison</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="75"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="101"/>
        <source>Reset Fellowship Points</source>
        <translation>Réinitialiser les points de communauté</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="108"/>
        <source>Short Rest</source>
        <translation>Repos court</translation>
    </message>
    <message>
        <location filename="../src/gui/overview.ui" line="115"/>
        <source>Long Rest</source>
        <translation>Repos long</translation>
    </message>
</context>
<context>
    <name>player</name>
    <message>
        <location filename="../src/gui/player.ui" line="14"/>
        <location filename="../src/gui/player.ui" line="60"/>
        <source>Player</source>
        <translation>Joueur</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="95"/>
        <source>Standard of Living</source>
        <translation>Niveau de vie</translation>
    </message>
    <message>
        <source>Poor</source>
        <translation type="vanished">Pauvre</translation>
    </message>
    <message>
        <source>Frugal</source>
        <translation type="vanished">Frugale</translation>
    </message>
    <message>
        <source>Common</source>
        <translation type="vanished">Commun</translation>
    </message>
    <message>
        <source>Prosperous</source>
        <translation type="vanished">Prospère</translation>
    </message>
    <message>
        <source>Rich</source>
        <translation type="vanished">Riche</translation>
    </message>
    <message>
        <source>Very Rich</source>
        <translation type="vanished">Très riche</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="129"/>
        <source>Calling</source>
        <translation>Vocation</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="152"/>
        <source>Distinctive Features</source>
        <translation>Caractéristiques distinctives</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="115"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="122"/>
        <source>Culture</source>
        <translation>Culture</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="88"/>
        <source>Shadow Path</source>
        <translation>Chemin de l&apos;ombre</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="142"/>
        <source>Click me to set an image</source>
        <translation>Clique sur moi 
pour mettre une image</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="279"/>
        <location filename="../src/gui/player.ui" line="576"/>
        <location filename="../src/gui/player.ui" line="815"/>
        <source>Target Number</source>
        <translation>Nombre cible</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="225"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="292"/>
        <source>Endurance</source>
        <translation>Endurance</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1250"/>
        <location filename="../src/gui/player.ui" line="1284"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="353"/>
        <source>Awareness</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="339"/>
        <source>Awe</source>
        <translation>Admiration</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="360"/>
        <source>Athletics</source>
        <translation>Athlétisme</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="381"/>
        <source>Song</source>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="367"/>
        <source>Craft</source>
        <translation>Artisanat</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="266"/>
        <location filename="../src/gui/player.ui" line="563"/>
        <location filename="../src/gui/player.ui" line="866"/>
        <source>78</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="332"/>
        <source>Hunting</source>
        <translation>Chasse</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="406"/>
        <source>Axes</source>
        <translation>Haches</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="441"/>
        <source>Bows</source>
        <translation>Arcs</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="448"/>
        <source>Spears</source>
        <translation>Lances</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="455"/>
        <source>Swords</source>
        <translation>Epées</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="522"/>
        <source>Heart</source>
        <translation>Coeur</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="589"/>
        <source>Hope</source>
        <translation>Espoir</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="650"/>
        <source>Enhearten</source>
        <translation>Inspirer</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="657"/>
        <source>Travel</source>
        <translation>Voyage</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="664"/>
        <source>Insight</source>
        <translation>Perspicacité</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="671"/>
        <source>Healing</source>
        <translation>Soin</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="678"/>
        <source>Battle</source>
        <translation>Guerre</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="685"/>
        <source>Courtesy</source>
        <translation>Courtoisie</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="703"/>
        <source>Valour</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="799"/>
        <source>Wits</source>
        <translation>Esprit</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="828"/>
        <location filename="../src/gui/player.ui" line="1103"/>
        <source>Parry</source>
        <translation>Parade</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="916"/>
        <source>Persuade</source>
        <translation>Persuader</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="923"/>
        <source>Stealth</source>
        <translation>Discretion</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="930"/>
        <source>Scan</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="937"/>
        <source>Lore</source>
        <translation>Traditions</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="951"/>
        <source>Explore</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="965"/>
        <source>Riddle</source>
        <translation>Enigme</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="983"/>
        <source>Wisdom</source>
        <translation>Sagesse</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1033"/>
        <source>Temporary</source>
        <translation>Temporaire</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1052"/>
        <source>Load</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1062"/>
        <source>Weapons</source>
        <translation>Armes</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1072"/>
        <source>Armour</source>
        <translation>Armure</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1079"/>
        <source>Protection</source>
        <translation>Protection</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1086"/>
        <source>Helm</source>
        <translation>Casque</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1096"/>
        <source>Shield</source>
        <translation>Bouclier</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1141"/>
        <source>Wounded</source>
        <translation>Blessé</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1166"/>
        <source>Injury</source>
        <translation>Blessure</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1191"/>
        <source>Adventure Points</source>
        <translation>Points d&apos;aventure</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1198"/>
        <source>Skill Points</source>
        <translation>Points de compétences</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1205"/>
        <source>Current</source>
        <translation>Actuel</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1212"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1243"/>
        <source>Current Treasures</source>
        <translation>Trésors actuels</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1257"/>
        <source>Total Load</source>
        <translation>Charge totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1264"/>
        <source>Shadow</source>
        <translation>Ombre</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1271"/>
        <source>Total Protection</source>
        <translation>Protection totale</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1291"/>
        <source>Total Treasures</source>
        <translation>Trésores totals</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1311"/>
        <source>Shadow Scars</source>
        <translation>Cicatrice d&apos;ombre</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1328"/>
        <source>Lock Player</source>
        <translation>Verrouiller le joueur</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1348"/>
        <source>New Character</source>
        <translation>Nouveau personnage</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1355"/>
        <source>Save Current Character</source>
        <translation>Sauvegarder le personnage actuel</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1362"/>
        <source>Load Character</source>
        <translation>Charger personnage</translation>
    </message>
    <message>
        <location filename="../src/gui/player.ui" line="1369"/>
        <source>Remove Character</source>
        <translation>Enlever personnage</translation>
    </message>
</context>
</TS>
