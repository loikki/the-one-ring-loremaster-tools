.ONESHELL:
QTVERSION=6.5.1
QTDIR=${HOME}/Qt/$(QTVERSION)

all: build

init:
	mkdir -p build

conan-ubuntu:
	conan install cmake/conanfile_ubuntu.txt -of conan
	conan install cmake/conanfile_ubuntu.txt -of conan -s build_type=Debug --build="*"

cmake-debug: init
	rm -rf build
	cmake . -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=../conan/conan_toolchain.cmake -DCMAKE_FIND_ROOT_PATH:PATH=$(QTDIR)/gcc_64

cmake-release: init
	rm -rf build
	cmake . -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../conan/conan_toolchain.cmake -DCMAKE_FIND_ROOT_PATH:PATH=$(QTDIR)/gcc_64

build: init
	cd build
	make -j 10


run-as-windows: STYLE=Windows
run-as-windows: .run

run-linux: STYLE=Fusion
run-linux: .run

.run: build
	cd build
	export QT_STYLE_OVERRIDE=$(STYLE)
	./bin/loremastertools

gdb-linux: init build
	cd build
	gdb ./bin/loremastertools --eval-command="run"

tidy:
	cd build
	make tidy

format:
	cd build
	make format

check-format:
	cd build
	make check-format

appimage:
	cd build
	make appimage
