#!/bin/bash

cmd="clang-format-14 --Werror --style=Mozilla"
if [ "$1" == "check" ];
then
    shift;
    $cmd --dry-run $@
    if [[ $? -ne 0 ]]
    then
        echo "The code is not properly formatted"
        exit 1
    fi
elif [ "$1" == "format" ];
then
    shift;
    $cmd -i $@
else
    echo Wrong input
    exit 1
fi
