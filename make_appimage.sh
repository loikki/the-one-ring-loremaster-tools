#!/bin/bash

set -e

cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
make install DESTDIR=AppDir -j 10

# now, build AppImage using linuxdeploy and linuxdeploy-plugin-qt
# download linuxdeploy and its Qt plugin

wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
wget https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage

# make them executable and extract them (to make them compatible with docker
chmod +x linuxdeploy*.AppImage
./linuxdeploy-plugin-qt-x86_64.AppImage --appimage-extract
./linuxdeploy-x86_64.AppImage --appimage-extract
rm linuxdeploy*.AppImage # important ! else the AppImage of the Qt plugin will be used (and will fail on docker)

# Create the desktop file
cp ../the-one-ring.desktop loremastertools.desktop
# Remove /opt/the-.../ path
sed -i "s/\/opt\/the-one-ring-loremaster-tools/\/usr/g" loremastertools.desktop
# Remove /usr/bin directory
sed -i "s/\/usr\/bin\///g" loremastertools.desktop
# Remove /usr directory
sed -i "s/\/usr\///g" loremastertools.desktop
# Remove .png in favicon
sed -i "s/\.png//g" loremastertools.desktop
# Remove path
sed -i "/Path/d" loremastertools.desktop

# Copy the swagger files
mv AppDir/usr/swagger AppDir/usr/bin/

# initialize AppDir, bundle shared libraries for QtQuickApp, use Qt plugin to bundle additional resources, and build AppImage, all in one single command
export QMAKE=/usr/bin/qmake6
./squashfs-root/AppRun --appdir AppDir --plugin qt --output appimage -d loremastertools.desktop -i ../favicon.png
