/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#undef NDEBUG

/* Own includes */
#include "../src/tools/rolls_probabilities_ui.hpp"

/* Standard library */
#include <assert.h>

/* Other includes */
#include <QMainWindow>

/* This was mostly tested in random.hpp */
class TestRollsProbabilitiesUi : public RollsProbabilitiesUi
{
public:
  TestRollsProbabilitiesUi()
    : RollsProbabilitiesUi()
  {
  }

  void CheckProbabilitiesAreMonotonic(std::vector<RollPoint>& test,
                                      bool test_is_miserable)
  {
    /* Check that the function is monotonic decreasing */
    float old_y = 1.;
    float old_x = 0;
    for (RollPoint res : test) {
      assert(res.y <= old_y);
      assert(res.x > old_x);
      old_y = res.y;
      old_x = res.x;
    }

    if (test_is_miserable) {
      assert(test[0].y < 1.);
    } else {
      assert(test[0].y > 0);
    }
    assert(test.back().y > 0);
  }

  void CheckProbabilities()
  {
    const int test_number_rolls = 10000;
    int number_dice = 0;
    const RollType test_roll_type = kRollNormal;
    const bool test_is_weary = false;
    const bool test_is_miserable = false;
    RollStatistics test = this->ComputeProbabilities(test_number_rolls,
                                                     test_roll_type,
                                                     number_dice,
                                                     test_is_weary,
                                                     test_is_miserable);
    this->CheckProbabilitiesAreMonotonic(test.success, test_is_miserable);
    /* Check secondary attributes */
    assert(test.average_tengwar == 0);
    assert(test.average_value > 0 && test.average_value <= 10);

    /* Now do the same but with some skill dices */
    number_dice = 3;
    test = this->ComputeProbabilities(test_number_rolls,
                                      test_roll_type,
                                      number_dice,
                                      test_is_weary,
                                      test_is_miserable);
    this->CheckProbabilitiesAreMonotonic(test.success, test_is_miserable);

    /* Check secondary attributes */
    assert(test.average_tengwar > 0);
    assert(test.average_tengwar < number_dice);
    assert(test.average_value >= number_dice &&
           test.average_value <= 10 + 6 * number_dice);
  }

  void RunTests()
  {
    this->CheckProbabilities();
    /* Just ensure that nothing bad happen when resetting the axis */
    this->ResetAxes();
    /* Checking if removing the axis is not an issue */
    this->CreateGraph();
  }
};

int
main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  TestRollsProbabilitiesUi rolls;
  rolls.RunTests();
}
