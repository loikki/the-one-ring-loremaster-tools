/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"
#include "../src/player/player_ui.hpp"

/* Other includes */
#include <QMainWindow>

class TestMainWindow : public MainWindow
{
public:
  TestMainWindow(QMainWindow& new_main_window, QApplication& new_app)
    : MainWindow(new_main_window, new_app){};

  void RunTests()
  {
    this->TestLoadAllPlayers();
    this->TestExtractLanguages();
    this->TestSwitchTabs();
  }

  void TestLoadAllPlayers()
  {
    this->LoadAllPlayers();
    assert(this->fellowship->size() >= 1);
  }

  void TestExtractLanguages()
  {
    std::map<std::string, std::string> lang = this->ExtractLanguages();
    assert(lang.size() == 4);
    assert(lang["pt"] == "Português");
    assert(lang["fr"] == "Français");
    assert(lang["en"] == "English");
    assert(lang["it"] == "Italiano");

    /* Now in reverse order */
    lang = this->ExtractLanguages(true);
    assert(lang.size() == 4);
    assert(lang["Português"] == "pt");
    assert(lang["Français"] == "fr");
    assert(lang["English"] == "en");
    assert(lang["Italiano"] == "it");
  }

  void TestSwitchTabs()
  {
    for (int i = 0; i < kTabCount; i++) {
      this->main_ui->tabWidget->setCurrentIndex(i);
    }
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  TestMainWindow own_main(main_window, app);
  main_window.show();
  own_main.RunTests();
}
