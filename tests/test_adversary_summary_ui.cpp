/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_summary_ui.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"

/* Other includes */
#include <QMainWindow>

class TestAdversarySummaryUi : public AdversarySummaryUi
{
public:
  TestAdversarySummaryUi(QWidget& new_parent, Adversary& new_adversary)
    : AdversarySummaryUi(new_parent, new_adversary){};

  void RunTests() { this->CheckUpdateUi(); }

  void CheckUpdateUi()
  {
    this->adversary.use_hate = false;
    this->UpdateUi();
    assert(this->hate_label->text() == "Resolve");
    assert(this->endurance->value() == this->adversary.current_endurance);
    assert(this->hate->value() == this->adversary.current_hate);
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Create the test adversary_summary ui */
  Adversary adversary;
  TestAdversarySummaryUi tests =
    TestAdversarySummaryUi(*own_main.GetMainUi()->adversary_tab, adversary);
  main_window.show();
  tests.RunTests();
}
