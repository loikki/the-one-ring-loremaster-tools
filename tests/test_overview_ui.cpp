/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
class TestOverviewUi;

/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"
#include "../src/overview_ui.hpp"
#include "../src/player/player_ui.hpp"

/* Other includes */
#include <QMainWindow>

class TestOverviewUi : public OverviewUi
{
public:
  TestOverviewUi(QWidget& new_parent, std::shared_ptr<Fellowship> fellow)
    : OverviewUi(new_parent, fellow){};

  void RunTests() { this->TestUpdate(); }

  void TestUpdate()
  {
    /* Rest (really tested in player not overview) */
    this->ShortRest();
    this->LongRest();
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Create the test adversary ui */
  TestOverviewUi tests(*own_main.GetMainUi()->adversary_tab,
                       own_main.GetSharedFellowship());
  main_window.show();
  tests.RunTests();
}
