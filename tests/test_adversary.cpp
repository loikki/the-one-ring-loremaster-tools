/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/utils.hpp"

/* Other includes */
#include <iostream>

class TestAdversary : public Adversary
{
  void check_json()
  {
    /* Create Json */
    this->name = "test_adversary";
    QJsonObject json = this->GetJson();

    /* Write to file */
    const std::string directory = "tests/";
    const std::string filename = this->GetFilename(directory);
    QFile file(QString::fromStdString(filename));
    this->SaveToFile(directory);

    /* Check if the file exists */
    assert(file.exists());

    /* Read from file */
    QJsonObject json2 = ReadJsonObject(filename);

    /* Compare from same json */
    Adversary adversary2;
    adversary2.RestoreFromJson(json);
    assert(memcmp(this, &adversary2, sizeof(adversary2)));

    /* Now compare with json from file */
    adversary2.RestoreFromJson(json2);
    assert(memcmp(this, &adversary2, sizeof(adversary2)));
  }

  void check_endurance()
  {
    /* Check endurance */
    this->max_endurance = 10;
    this->current_endurance = 2;
    this->RemoveEndurance(5);
    assert(this->current_endurance == 0);
    this->RemoveEndurance(-20);
    assert(this->current_endurance == 10);
    this->RemoveEndurance(5);
    assert(this->current_endurance == 5);
  }

  void check_hate()
  {
    /* Check hate */
    this->max_hate = 10;
    this->current_hate = 2;
    this->RemoveHate(5);
    assert(this->current_hate == 0);
    this->RemoveHate(-20);
    assert(this->current_hate == 10);
    this->RemoveHate(5);
    assert(this->current_hate == 5);
  }

  void check_filename()
  {
    /* Check filename */
    this->name = "test";
    assert(this->GetFilename("test/app") == "test/app/test.adv");
  }

public:
  void run_tests()
  {
    this->check_endurance();
    this->check_hate();
    this->check_filename();
    this->check_json();
  }
};

int
main()
{
  /* Run tests */
  TestAdversary adversary;
  adversary.run_tests();

  return 0;
}
