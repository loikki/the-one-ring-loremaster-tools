/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"
#include "../src/player/player_ui.hpp"

/* Other includes */
#include <QMainWindow>

class TestPlayerUi : public PlayerUi
{
public:
  TestPlayerUi(QWidget& parent_ref,
               std::string new_directory,
               std::shared_ptr<Fellowship> fellowship_ptr,
               bool hide_image,
               bool hide_skills,
               bool lock_player_bool)
    : PlayerUi(parent_ref,
               new_directory,
               fellowship_ptr,
               hide_image,
               hide_skills,
               lock_player_bool){};

  void RunTests() { this->TestCreationDeletionPlayers(); }

  void TestCreationDeletionPlayers()
  {
    /* Test creation / deletion of new players */
    assert(this->fellowship->size() == 1);
    this->NewPlayer();
    assert(this->fellowship->size() == 2);
    assert(this->player == &(*this->fellowship)[1]);
    this->RemoveCurrentPlayer();
    assert(this->fellowship->size() == 1);
    assert(this->player == &(*this->fellowship)[0]);
    /* We always have at least one player */
    this->RemoveCurrentPlayer();
    assert(this->fellowship->size() == 1);
    assert(this->player == &(*this->fellowship)[0]);
  }

  void TestUpdateStandardOfLiving()
  {
    /* Deeply tested in player */
    this->total_treasure->setValue(-5);
    this->UpdateStandardOfLivingUi();
    assert(this->player->GetStandardOfLiving() == kLivingStandardPoor);
    this->total_treasure->setValue(5);
    this->UpdateStandardOfLivingUi();
    assert(this->player->GetStandardOfLiving() == kLivingStandardFrugal);
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Create the test player ui */
  TestPlayerUi* tests = new TestPlayerUi(*own_main.GetMainUi()->player_tab,
                                         own_main.GetDirectory(),
                                         own_main.GetSharedFellowship(),
                                         /* hide_image */ false,
                                         /* hide_skills */ false,
                                         /* lock_player */ false);
  own_main.ResetPlayerUi(tests);
  main_window.show();
  tests->RunTests();
}
