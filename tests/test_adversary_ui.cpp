/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"

/* Other includes */
#include <QMainWindow>

class TestAdversaryUi : public AdversaryUi
{
public:
  TestAdversaryUi(QWidget& new_parent,
                  std::string new_directory,
                  std::shared_ptr<Encounter> encounter_ptr)
    : AdversaryUi(new_parent, new_directory, encounter_ptr){};

  void RunTests()
  {
    this->TestCreationDeletionAdversarys();
    this->TestDirectory();
    this->TestSaving();
  }

  void TestCreationDeletionAdversarys()
  {
    /* Test creation / deletion of new adversarys */
    assert(this->encounter->size() == 1);
    this->NewAdversary();
    assert(this->encounter->size() == 2);
    assert(this->adversary == &(*this->encounter)[1]);
    this->RemoveCurrentAdversary();
    assert(this->encounter->size() == 1);
    assert(this->adversary == &(*this->encounter)[0]);
    /* We always have at least one adversary */
    this->RemoveCurrentAdversary();
    assert(this->encounter->size() == 1);
    assert(this->adversary == &(*this->encounter)[0]);
  }

  void TestDirectory()
  {
    const std::string base = this->directory;
    assert(base + "/Adversaries/" == this->GetDirectory());
  }

  void TestSaving()
  {
    this->name->setText("Test");
    this->SaveToAdversary();
    assert(this->adversary->name == "Test");
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Create the test adversary ui */
  TestAdversaryUi* tests = new TestAdversaryUi(
    *own_main.GetMainUi()->adversary_tab, "", own_main.GetSharedEncounter());
  own_main.ResetAdversaryUi(tests);
  main_window.show();
  tests->RunTests();
}
