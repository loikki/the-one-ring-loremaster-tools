/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/tools/random_event_ui.hpp"

/* Standard library */
#include <iostream>

/* Other includes */
#include <QMainWindow>

class TestRandomEventUi : public RandomEventUi
{
public:
  TestRandomEventUi(std::string test_events_done)
    : RandomEventUi(test_events_done)
  {
  }

  void AssertAllElementsAreUsed()
  {
    for (QJsonValue val : this->data) {
      QJsonObject obj = val.toObject();

      /* Check if the role is known */
      QString test_role = obj["role"].toString();
      if (not test_role.contains("all")) {
        bool role_ok = false;
        for (QString role_name : kJourneyRoleString) {
          if (test_role.contains(role_name)) {
            role_ok = true;
            break;
          }
        }
        if (not role_ok) {
          std::cerr << obj["title"].toString().toStdString() << std::endl;
          assert(role_ok);
        }
      }

      /* Check if the type of event is known */
      bool event_type_ok = false;
      QString ref = obj["event_type"].toString();
      for (QString test_event_type : kEventTypeString) {
        if (test_event_type == ref) {
          event_type_ok = true;
          break;
        }
      }
      if (not event_type_ok) {
        std::cerr << obj["title"].toString().toStdString() << std::endl;
        assert(event_type_ok);
      }
    }
  }

  void TestSaveEvent()
  {
    for (int i = 0; i < 10 * this->data.size(); i++) {
      this->Roll();
      this->SaveEvent();
    }

    assert(this->description->toPlainText().contains("No new event"));
  }

  void RunTests()
  {
    this->AssertAllElementsAreUsed();
    this->TestSaveEvent();
  }
};

int
main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  TestRandomEventUi events("tests/unexisting_file.txt");
  events.RunTests();
}
