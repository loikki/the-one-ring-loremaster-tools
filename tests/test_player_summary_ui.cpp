/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"
#include "../src/overview_ui.hpp"
#include "../src/player/player_summary_ui.hpp"
#include "../src/player/player_ui.hpp"

/* Other includes */
#include <QMainWindow>

class TestPlayerSummaryUi : public PlayerSummaryUi
{
public:
  TestPlayerSummaryUi(QWidget& new_parent, Player& new_player)
    : PlayerSummaryUi(new_parent, new_player){};

  void RunTests() { this->CheckUpdate(); }

  void CheckUpdate()
  {
    /* Shadow scars */
    int scars = 5;
    this->shadow_scars->setValue(scars);
    this->UpdateShadowScars();
    assert(this->player.statistics.heroic.shadow_scars == scars);

    /* Fatigue */
    int current_fatigue = 5;
    this->fatigue->setValue(current_fatigue);
    this->UpdateFatigue();
    assert(this->player.statistics.fatigue == current_fatigue);

    /* Temporary load */
    int temporary = 5;
    this->temporary_load->setValue(temporary);
    this->UpdateTemporaryLoad();
    assert(this->player.equipment.temporary_load == temporary);
  }
};

int
main(int argc, char* argv[])
{
  /* Create the required windows for Qt */
  QApplication app(argc, argv);
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Create the test player_summary ui */
  Player player;
  TestPlayerSummaryUi tests =
    TestPlayerSummaryUi(*own_main.GetOverviewUi(), player);
  main_window.show();
  tests.RunTests();
}
