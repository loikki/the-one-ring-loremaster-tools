/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#undef NDEBUG
/* Own includes */
#include "../src/tools/random_features_ui.hpp"

/* Other includes */
#include <QMainWindow>

class TestRandomFeaturesUi : public RandomFeaturesUi
{
public:
  TestRandomFeaturesUi()
    : RandomFeaturesUi()
  {
  }

  void RunTests()
  {
    const int n_features = 10;
    for (int i = 0; i < 10; i++) {
      /* positive */
      this->features_type_combo->setCurrentIndex(0);
      this->NewFeatures();
      QString text = this->features_label->text();
      assert(not text.isEmpty());
      assert(text.count("\n") == n_features);

      /* negative */
      this->features_type_combo->setCurrentIndex(1);
      this->NewFeatures();
      text = this->features_label->text();
      assert(not text.isEmpty());
      assert(text.count("\n") == n_features);

      /* both */
      this->features_type_combo->setCurrentIndex(2);
      this->NewFeatures();
      text = this->features_label->text();
      assert(not text.isEmpty());
      assert(text.count("\n") == n_features);
    }
  }
};

int
main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  TestRandomFeaturesUi features;
  features.RunTests();
}
