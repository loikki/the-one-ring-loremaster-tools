/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/tools/shuffle_list_ui.hpp"

/* Other includes */
#include <QMainWindow>
#include <iostream>

/* This was mostly tested in random.hpp */
class TestShuffleListUi : public ShuffleListUi
{
public:
  TestShuffleListUi(std::string dir)
    : ShuffleListUi(dir)
  {
  }

  void RunTests()
  {
    /* Quick test to see if we are really shuffling */
    this->Load("../tests/test.list");
    QStringList list = this->text_edit->toPlainText().split("\n");
    for (int i = 0; i < list.size(); i++) {
      assert(list[i] == QString::number(i));
    }

    this->Shuffle();
    list = this->text_edit->toPlainText().split("\n");
    int count = 0;
    for (int i = 0; i < list.size(); i++) {
      count += list[i] == QString::number(i);
    }
    std::cout << count << std::endl;
    assert(count < 4);
  }
};

int
main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  TestShuffleListUi list("tests");
  list.RunTests();
}
