/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/tools/name_generator_ui.hpp"

/* Standard includes */
#include <iostream>

class TestNameGeneratorUi : public NameGeneratorUi
{
public:
  void RunTests()
  {
    this->TestGenerators();
    this->TestComplexGenerators();
  }

  void TestGenerators()
  {
    for (tuple_gen gen : this->generators) {
      for (int i = 0; i < 10; i++) {
        assert(not std::get<1>(gen)->GetName(true, false).empty());
        assert(not std::get<1>(gen)->GetName(true, true).empty());
        assert(not std::get<1>(gen)->GetName(false, false).empty());
        assert(not std::get<1>(gen)->GetName(false, true).empty());
      }
    }
  }

  void TestComplexGenerators()
  {
    /* Test Quenya */
    QuenyaNameGenerator quenya;
    for (int i = 0; i < 500; i++) {
      assert(not quenya.GetName(true, false).empty());
      assert(not quenya.GetName(true, true).empty());
      assert(not quenya.GetName(false, false).empty());
      assert(not quenya.GetName(false, true).empty());
    }

    /* Test Sindarin */
    SindarinNameGenerator sindarin;
    for (int i = 0; i < 500; i++) {
      assert(not sindarin.GetName(true, false).empty());
      assert(not sindarin.GetName(true, true).empty());
      assert(not sindarin.GetName(false, false).empty());
      assert(not sindarin.GetName(false, true).empty());
    }
  }
};

int
main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  TestNameGeneratorUi generator;
  generator.RunTests();
  return 0;
}
