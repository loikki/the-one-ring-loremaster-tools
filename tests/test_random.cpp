/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#undef NDEBUG
/* Own includes */
#include "../src/adversary/adversary.hpp"
#include "../src/adversary/adversary_ui.hpp"
#include "../src/main_window.hpp"
#include "../src/player/player_ui.hpp"
#include "../src/random.hpp"

/* Other includes */
#include <QMainWindow>

void
TestRollSkillDice()
{
  std::array<int, 7> result = { 0, 0, 0, 0, 0, 0, 0 };
  const int n_rolls = 1000;
  const int error = n_rolls / 20;

  /* Normal roll */
  for (int i = 0; i < n_rolls; i++) {
    result[RollSkillDice(false)]++;
  }

  assert(result[0] == 0);
  for (int i = 1; i < 7; i++) {
    assert(std::abs(result[i] - n_rolls / 6) < error);
    result[i] = 0;
  }

  /* Weary */
  for (int i = 0; i < n_rolls; i++) {
    result[RollSkillDice(true)]++;
  }

  for (int i = 1; i < 4; i++) {
    assert(result[i] == 0);
  }
  for (int i = 4; i < 7; i++) {
    assert(std::abs(result[i] - n_rolls / 6) < error);
  }
  assert(std::abs(result[0] - 3 * n_rolls / 6) < error);
}

void
TestRollFeatDice()
{
  std::array<int, 13> result = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  const int n_rolls = 10000;
  const int error = n_rolls / 20;

  /* Normal roll */
  for (int i = 0; i < n_rolls; i++) {
    result[RollFeatDice(kRollNormal)]++;
  }

  assert(result[0] == 0);
  for (int i = 1; i < 13; i++) {
    assert(std::abs(result[i] - n_rolls / 12) < error);
    result[i] = 0;
  }

  /* Non standard */
  for (int i = 0; i < 50; i++) {
    RollFeatDice(kRollFavoured);
    RollFeatDice(kRollIllFavoured);
  }
}

void
TestShuffle()
{
  std::vector<int> array;
  for (int i = 0; i < 100; i++) {
    array.push_back(i);
  }

  shuffle(array.begin(), array.end());

  int sum = 0;
  for (int i = 0; i < 100; i++) {
    sum += array[i] == i;
  }
  assert(sum < 10);
}

int
main()
{

  TestRollSkillDice();
  TestRollFeatDice();
  TestShuffle();
}
