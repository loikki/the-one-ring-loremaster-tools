/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "../src/player/player.hpp"
#include "../src/utils.hpp"

/* Other includes */
#include <iostream>

class TestPlayer : Player
{
  void CheckJson()
  {
    /* Create Json */
    this->name = "test_player";
    QJsonObject json = this->GetJson();

    /* Write to file */
    const std::string directory = "tests/";
    const std::string filename = this->GetFilename(directory);
    QFile file(QString::fromStdString(filename));
    this->SaveToFile(directory);

    /* Check if the file exists */
    assert(file.exists());

    /* Read from file */
    QJsonObject json2 = ReadJsonObject(filename);

    /* Compare from same json */
    Player player2;
    player2.RestoreFromJson(json);
    // NOLINTNEXTLINE
    assert(memcmp(this, &player2, sizeof(player2)));

    /* Now compare with json from file */
    player2.RestoreFromJson(json2);
    // NOLINTNEXTLINE
    assert(memcmp(this, &player2, sizeof(player2)));

    /* Now try to delete the file */
    this->DeleteFile(directory);
    assert(!file.exists());
  }

  void CheckEndurance()
  {
    /* Check endurance */
    this->statistics.attributes.max_endurance = 10;
    this->statistics.attributes.current_endurance = 2;
    this->statistics.RemoveEndurance(5);
    assert(this->statistics.attributes.current_endurance == 0);
    this->statistics.RemoveEndurance(-20);
    assert(this->statistics.attributes.current_endurance == 10);
    this->statistics.RemoveEndurance(5);
    assert(this->statistics.attributes.current_endurance == 5);
  }

  void CheckHope()
  {
    /* Check hope */
    this->statistics.attributes.max_hope = 10;
    this->statistics.attributes.current_hope = 2;
    this->statistics.RemoveHope(5);
    assert(this->statistics.attributes.current_hope == 0);
    this->statistics.RemoveHope(-20);
    assert(this->statistics.attributes.current_hope == 10);
    this->statistics.RemoveHope(5);
    assert(this->statistics.attributes.current_hope == 5);
  }

  void CheckParry()
  {
    /* Check parry */
    this->statistics.attributes.parry = 3;
    auto shield = std::make_shared<Shield>();
    shield->WearShield(true);
    shield->SetParry(1);
    this->equipment.equipment.push_back(shield);
    assert(this->GetTotalParry() == 4);

    this->equipment.equipment.pop_back();
  }

  void CheckProtection()
  {
    /* Check protection */
    auto helm = std::make_shared<Helm>();
    helm->WearHelm(true);
    helm->SetProtection(1);
    auto armour = std::make_shared<Armour>();
    armour->wearing = true;
    armour->SetProtection(3);

    this->equipment.equipment.push_back(armour);
    this->equipment.equipment.push_back(helm);

    assert(this->GetTotalProtection() == 4);

    this->equipment.equipment.pop_back();
    this->equipment.equipment.pop_back();
  }

  void CheckLoad()
  {
    auto helm = std::make_shared<Helm>();
    helm->WearHelm(true);
    helm->load = 14;
    auto armour = std::make_shared<Armour>();
    armour->wearing = true;
    armour->load = 4;
    auto shield = std::make_shared<Shield>();
    shield->WearShield(true);
    shield->load = 11;
    auto weapon = std::make_shared<Weapon>();
    weapon->wearing = true;
    weapon->load = 1;

    this->equipment.equipment.push_back(shield);
    this->equipment.equipment.push_back(armour);
    this->equipment.equipment.push_back(helm);
    this->equipment.equipment.push_back(weapon);

    /* Check load */
    this->equipment.treasure.current = 5;
    this->equipment.temporary_load = 7;
    assert(this->equipment.GetCurrentLoad() == 42);

    this->equipment.equipment.pop_back();
    this->equipment.equipment.pop_back();
    this->equipment.equipment.pop_back();
    this->equipment.equipment.pop_back();
  }

  void CheckStandardOfLiving()
  {
    /* Poor */
    this->equipment.treasure.total = -5;
    assert(this->GetStandardOfLiving() == kLivingStandardPoor);

    /* Frugal */
    this->equipment.treasure.total = 5;
    assert(this->GetStandardOfLiving() == kLivingStandardFrugal);

    /* Common */
    this->equipment.treasure.total = 40;
    assert(this->GetStandardOfLiving() == kLivingStandardCommon);

    /* Prosperous */
    this->equipment.treasure.total = 90;
    assert(this->GetStandardOfLiving() == kLivingStandardProsperous);

    /* Rich */
    this->equipment.treasure.total = 190;
    assert(this->GetStandardOfLiving() == kLivingStandardRich);

    /* Very Rich */
    this->equipment.treasure.total = 340;
    assert(this->GetStandardOfLiving() == kLivingStandardVeryRich);
  }

  void CheckFilename()
  {
    /* Check filename */
    this->name = "test";
    assert(this->GetFilename("test/app") == "test/app/test.pl");
  }

public:
  void RunTests()
  {
    this->CheckEndurance();
    this->CheckHope();
    this->CheckParry();
    this->CheckProtection();
    this->CheckLoad();
    this->CheckFilename();
    this->CheckJson();
    this->CheckStandardOfLiving();
  }
};

int
main()
{
  /* Run tests */
  TestPlayer player;
  player.RunTests();

  return 0;
}
