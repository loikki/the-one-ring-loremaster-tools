#!/bin/bash

set -eE

NO_CLEAN=0
FORMAT="xml"
while [[ $# -gt 0 ]]; do
    case $1 in
        -n|--no-clean)
            NO_CLEAN=1
            shift # past argument
            ;;
        -f|--format)
            FORMAT="$2"
            shift # past argument
            shift # past value
            ;;
        -*|--*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            echo "Positional arguments are not supported"
            exit 1
            ;;
    esac
done

cd build

# Compile for test coverage
if [ $NO_CLEAN -eq 0 ];
then
    rm -rf CMakeCache.txt
    cmake .. -DEnableCoverage=ON -DCMAKE_BUILD_TYPE=DEBUG
fi

# Run test
make -j 10 build-tests
trap 'cat Testing/Temporary/LastTest.log' ERR
make test
trap - ERR

# Check coverage
if [ "$FORMAT" == "xml" ];
then
    gcovr -r . CMakeFiles -f ../src --exclude-unreachable-branches --exclude=../tests/* --exclude=translate/* --print-summary --fail-under-line 50 --xml-pretty -o coverage.xml
elif [ "$FORMAT" == "html" ];
then
    lcov --capture --directory .. --output-file coverage_report.txt --exclude "/usr/*" --exclude "*/tests/*" --exclude "*translate/*"
    genhtml coverage_report.txt --output-directory coverage
else
    echo "Format '$FORMAT' not supported"
    exit 1
fi
