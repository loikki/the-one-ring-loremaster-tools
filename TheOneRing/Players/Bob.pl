{
    "calling": "Captain",
    "combat_skills": [
        0,
        2,
        0,
        1
    ],
    "culture": "Bardings",
    "current_adventure_points": 0,
    "current_skill_points": 0,
    "endurance": 25,
    "endurance_current": 25,
    "equipment": [
        {
            "damage": 1,
            "load": 1,
            "name": "Weapon1",
            "notes": "",
            "one_hand_injury": 1,
            "proficiency": 0,
            "qualities": [
                ""
            ],
            "two_hand_injury": 1,
            "type": 1,
            "unlocked_qualities": 1,
            "wearing": true
        },
        {
            "load": 2,
            "name": "object1",
            "notes": "",
            "qualities": [
                ""
            ],
            "type": 0,
            "unlocked_qualities": 0,
            "wearing": true
        },
        {
            "damage": 5,
            "load": 2,
            "name": "Weapon2",
            "notes": "",
            "one_hand_injury": 4,
            "proficiency": 1,
            "qualities": [
                ""
            ],
            "two_hand_injury": 3,
            "type": 1,
            "unlocked_qualities": 1,
            "wearing": true
        },
        {
            "load": 4,
            "name": "Armor2",
            "notes": "",
            "protection": 2,
            "qualities": [
                "\t\t\t"
            ],
            "type": 2,
            "unlocked_qualities": 2,
            "wearing": true
        },
        {
            "load": 1,
            "name": "Armor1",
            "notes": "",
            "parry_modifier": 1,
            "qualities": [
                "\t\t"
            ],
            "type": 3,
            "unlocked_qualities": 1,
            "wearing": true
        },
        {
            "load": 0,
            "name": "object2",
            "notes": "",
            "qualities": [
                ""
            ],
            "type": 0,
            "unlocked_qualities": 0,
            "wearing": true
        }
    ],
    "fatigue": 0,
    "features": "Bold, Eager, Leadership",
    "heart": 7,
    "hope": 15,
    "hope_current": 15,
    "image": "",
    "injury": "",
    "is_miserable": false,
    "is_weary": false,
    "journey_role": 0,
    "name": "Bob",
    "parry": 14,
    "player_name": "Moi",
    "rewards": "",
    "shadow": 0,
    "shadow_path": "Lure of Power",
    "shadow_scars": 0,
    "skills": [
        1,
        1,
        0,
        2,
        1,
        1,
        2,
        2,
        2,
        0,
        2,
        4,
        3,
        0,
        1,
        1,
        0,
        1
    ],
    "skills_favorite": [
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false
    ],
    "standard_of_living": "Frugal",
    "strength": 5,
    "temporary_load": 0,
    "total_adventure_points": 0,
    "total_load_fatigue": 10,
    "total_parry": 15,
    "total_protection": 2,
    "total_skill_points": 0,
    "treasure": 0,
    "treasure_total": 0,
    "valour": 1,
    "virtues": "Stout-Hearted",
    "wisdom": 1,
    "wits": 2,
    "wounded": false
}
