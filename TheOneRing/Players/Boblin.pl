{
    "calling": "Elve of Lindon",
    "combat_skills": [
        0,
        2,
        1,
        0
    ],
    "culture": "Scholar",
    "current_adventure_points": 0,
    "current_skill_points": 0,
    "endurance": 25,
    "endurance_current": 25,
    "equipment": [
        {
            "load": 4,
            "name": "armor",
            "notes": "asbd",
            "protection": 2,
            "qualities": [
                "Test",
                "Test2",
                "Test3"
            ],
            "type": 2,
            "unlocked_qualities": 1,
            "wearing": true
        },
        {
            "load": 1,
            "name": "New Item",
            "notes": "",
            "qualities": [
                ""
            ],
            "type": 0,
            "unlocked_qualities": 0,
            "wearing": true
        },
        {
            "damage": 4,
            "load": 2,
            "name": "Bow",
            "notes": "",
            "one_hand_injury": 14,
            "proficiency": 1,
            "qualities": [
                "test1",
                "test2",
                "test3"
            ],
            "two_hand_injury": 0,
            "type": 1,
            "unlocked_qualities": 2,
            "wearing": true
        },
        {
            "damage": 3,
            "load": 3,
            "name": "Axe",
            "notes": "",
            "one_hand_injury": 16,
            "proficiency": 0,
            "qualities": [
                ""
            ],
            "two_hand_injury": 14,
            "type": 1,
            "unlocked_qualities": 0,
            "wearing": true
        },
        {
            "load": 1,
            "name": "shield",
            "notes": "",
            "protection": 0,
            "qualities": [
                ""
            ],
            "type": 2,
            "unlocked_qualities": 0,
            "wearing": true
        },
        {
            "load": 2,
            "name": "helm",
            "notes": "",
            "parry_modifier": 0,
            "qualities": [
                ""
            ],
            "type": 3,
            "unlocked_qualities": 0,
            "wearing": true
        }
    ],
    "fatigue": 0,
    "features": "Fair, Merry, Rhymes of lore",
    "heart": 3,
    "hope": 11,
    "hope_current": 6,
    "image": "",
    "injury": "",
    "is_miserable": true,
    "is_weary": false,
    "journey_role": 0,
    "name": "Boblin",
    "parry": 18,
    "player_name": "Moi2",
    "rewards": "",
    "shadow": 7,
    "shadow_path": "Lure of Secrets",
    "shadow_scars": 0,
    "skills": [
        2,
        2,
        2,
        0,
        2,
        3,
        1,
        0,
        0,
        2,
        0,
        0,
        0,
        3,
        0,
        0,
        0,
        4
    ],
    "skills_favorite": [
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false
    ],
    "standard_of_living": "Frugal",
    "strength": 5,
    "temporary_load": 0,
    "total_adventure_points": 0,
    "total_load_fatigue": 13,
    "total_parry": 18,
    "total_protection": 2,
    "total_skill_points": 0,
    "treasure": 0,
    "treasure_total": 0,
    "valour": 1,
    "virtues": "Elven-skill\nThe long defeat",
    "wisdom": 1,
    "wits": 6,
    "wounded": false
}
