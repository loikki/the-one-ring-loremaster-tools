/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "patron.hpp"
#include "utils.hpp"

/* Other libraries */
#include <QJsonObject>

void
Patron::ReadFromFile(const std::string& filename)
{
  QJsonObject json = ReadJsonObject(filename);
  this->RestoreFromJson(json);
};

QJsonObject
Patron::GetJson() const
{
  QJsonObject object;
  object["name"] = QString::fromStdString(this->name);
  object["fellowship_points"] = this->fellowship_points;
  object["advantages"] = QString::fromStdString(this->advantages);
  return object;
};

std::string
Patron::GetFilename(const std::string& directory) const
{
  std::string output(directory);
  output += "/";
  output += this->name;
  output += ".pat";
  return output;
}

void
Patron::SaveToFile(const std::string& directory) const
{
  /* Ensure that the output directory exists */
  if (not std::filesystem::exists(directory)) {
    std::filesystem::create_directory(directory);
  }

  /* Avoid saving empty players */
  if (this->name.empty()) {
    return;
  }
  std::string filename = this->GetFilename(directory);
  QJsonObject json = this->GetJson();
  WriteJsonToFile(json, filename);
};

void
Patron::RestoreFromJson(const QJsonObject& object)
{
  this->name = object["name"].toString().toStdString();
  this->fellowship_points = object["fellowship_points"].toInt();
  this->advantages = object["advantages"].toString().toStdString();
};
