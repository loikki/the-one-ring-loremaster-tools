/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "overview_ui.hpp"
#include "fellowship.hpp"
#include "player/player_summary_ui.hpp"

OverviewUi::~OverviewUi()
{
  this->RemoveAllSummaries();

  delete this->players_layout;
  this->players_layout = nullptr;
};

OverviewUi::OverviewUi(QWidget& parent_ref,
                       std::shared_ptr<Fellowship> fellowship_ptr)
  : Ui_overview()
  , parent(parent_ref)
  , fellowship(std::move(fellowship_ptr))
{
  this->setupUi(&parent_ref);
  this->ConnectSignalSlots();

  // NOLINTNEXTLINE
  this->players_layout = new QGridLayout(this->player_overview);
  this->player_overview->setLayout(this->players_layout);

  this->UpdateUi();
}

void
OverviewUi::ConnectSignalSlots()
{
  QObject::connect(this->short_rest_button, &QPushButton::clicked, [=]() {
    this->ShortRest();
  });
  QObject::connect(
    this->long_rest_button, &QPushButton::clicked, [=]() { this->LongRest(); });
  QObject::connect(this->reset_fellowship_points, &QPushButton::clicked, [=]() {
    this->fellowship->ResetFellowshipPoints();
    this->fellowship_points_spin->setValue(this->fellowship->fellowship_points);
  });
  QObject::connect(
    this->fellowship_points_spin, &QSpinBox::valueChanged, [=](int value) {
      this->fellowship->fellowship_points = value;
    });
}

void
OverviewUi::ShortRest()
{
  for (Player& player : *this->fellowship) {
    player.ShortRest();
  }
  this->UpdateUi();
}

void
OverviewUi::LongRest()
{
  for (Player& player : *this->fellowship) {
    player.LongRest();
  }
  this->UpdateUi();
}

void
OverviewUi::RemoveAllSummaries()
{
  /* player */
  for (int ind = this->players_layout->count() - 1; ind >= 0; --ind) {
    QLayoutItem* item = this->players_layout->itemAt(ind);
    this->players_layout->removeItem(item);
    delete item->widget();
    delete item;
  }
};

void
OverviewUi::UpdateUi()
{
  /* Save the player in order to be safe */
  emit this->SaveToPlayer();

  /* Cleanup current window */
  this->RemoveAllSummaries();

  /* Add all summaries */
  int index = 0;
  for (Player& player : *this->fellowship) {
    /* Skip empty players */
    if (player.IsEmpty()) {
      continue;
    };

    /* Compute location */
    const int col = index / 2;
    const int row = index - col * 2;
    ++index;

    auto* summary = new PlayerSummaryUi(*this, player);
    this->players_layout->addWidget(summary, row, col);
  }

  /* Update fellowship points */
  this->fellowship_points_spin->setValue(this->fellowship->fellowship_points);
};
