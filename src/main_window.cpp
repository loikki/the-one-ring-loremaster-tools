/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "main_window.hpp"
#include "adversary/adversary.hpp"
#include "adversary/adversary_ui.hpp"
#include "combat_overview_ui.hpp"
#include "overview_ui.hpp"
#include "player/player_ui.hpp"
#include "tools/name_generator_ui.hpp"
#include "tools/random_event_ui.hpp"
#include "tools/random_features_ui.hpp"
#include "tools/reward_experience_dialog.hpp"
#include "tools/rolls_probabilities_ui.hpp"
#include "tools/ruins_ui.hpp"
#include "tools/shuffle_list_ui.hpp"

/* Standard library */
#include <cstdlib>

/* Other libraries */
#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QTranslator>
#include <yaml-cpp/yaml.h>

MainWindow::~MainWindow()
{
  this->SaveAllPlayers();
  this->main_ui->tabWidget->blockSignals(true);
  this->settings.SaveSettings();
}

MainWindow::MainWindow(QMainWindow& main_window_ref, QApplication& app_ref)
  : app(app_ref)
  , main_window(main_window_ref)
  , fellowship(new Fellowship())
  , encounter(new Encounter())
  , main_ui(new Ui_TheOneRing)
  , translator(new QTranslator())
  , server(this->fellowship, this->settings.cors)
{
  /* Setup the UI */
  this->main_ui->setupUi(&this->main_window);
  this->player_ui.reset(new PlayerUi(*this->main_ui->player_tab,
                                     this->settings.directory,
                                     this->fellowship,
                                     this->settings.hide_player_image,
                                     this->settings.hide_player_skills,
                                     this->settings.lock_player));
  this->adversary_ui.reset(new AdversaryUi(
    *this->main_ui->adversary_tab, this->settings.directory, this->encounter));
  this->combat_overview_ui.reset(new CombatOverviewUi(
    *this->main_ui->combat_overview_tab, this->fellowship, this->encounter));
  this->overview_ui.reset(
    new OverviewUi(*this->main_ui->overview_tab, this->fellowship));
  this->patron_ui.reset(
    new PatronUi(*this->main_ui->patron_tab, this->settings.directory));
  this->server_logs_ui.reset(new ServerLogsUi(*this->main_ui->server_logs_tab));

  this->ConnectSignalSlots();

  this->ChangeLanguage(this->settings.language);

  this->main_ui->statusBar->showMessage(QObject::tr("Loremaster-tools ready"));

  /* Keep it for the end */
  if (this->settings.use_server) {
    this->StartOrStopServer();
  }
}

void
MainWindow::LoadEncounter() const
{
  /* Get the directory */
  const std::string directory = this->GetEncounterDirectory();
  const QString qdirectory = QString::fromStdString(directory);

  /* Ensure that the directory exists */
  if (!QDir().mkpath(qdirectory)) {
    throw std::ios_base::failure("Failed to created the directory '" +
                                 directory + "'");
  };
  std::string filename =
    QFileDialog::getOpenFileName(&this->main_window,
                                 QObject::tr("Open Encounter"),
                                 qdirectory,
                                 QObject::tr("Encounter file (*.enc)"))
      .toStdString();

  /* Check if we received something */
  if (filename.length() == 0) {
    return;
  }

  /* Load encounter and update UI */
  this->encounter->LoadEncounter(filename, this->adversary_ui->GetDirectory());
  this->adversary_ui->UpdateCurrentAdversary(-1);
  this->combat_overview_ui->UpdateUi();
  this->GetStatusBar()->showMessage(QObject::tr("Encounter loaded"));
}

void
MainWindow::SaveEncounter() const
{
  /* Get the directory */
  const std::string default_directory = this->GetEncounterDirectory();
  const QString qdefault_directory = QString::fromStdString(default_directory);

  /* Ensure that the directory exists */
  if (!QDir().mkpath(qdefault_directory)) {
    throw std::ios_base::failure("Failed to created the directory '" +
                                 default_directory + "'");
  };
  std::string filename =
    QFileDialog::getSaveFileName(&this->main_window,
                                 QObject::tr("Save Encounter"),
                                 qdefault_directory,
                                 QObject::tr("Encounter file (*.enc)"))
      .toStdString();

  /* Check if we received something */
  if (filename.length() == 0) {
    return;
  }

  /* Load encounter and update UI */
  this->adversary_ui->SaveToAdversary();
  this->encounter->SaveEncounter(filename, this->adversary_ui->GetDirectory());
  this->GetStatusBar()->showMessage(QObject::tr("Encounter saved"));
}

void
MainWindow::SaveAllPlayers() const
{
  this->player_ui->SaveToPlayer();
  this->fellowship->SaveAllPlayers(this->player_ui->GetDirectory());
  this->GetStatusBar()->showMessage(QObject::tr("All players are saved"));
}

void
MainWindow::LoadAllPlayers() const
{
  this->fellowship->LoadAllPlayers(this->player_ui->GetDirectory());
  this->player_ui->UpdateCurrentPlayer(-1);
  this->combat_overview_ui->UpdateUi();
  this->overview_ui->UpdateUi();
  this->GetStatusBar()->showMessage(QObject::tr("All players are loaded"));
}

std::map<std::string, std::string>
MainWindow::ExtractLanguages(bool reverse_order)
{
  std::map<std::string, std::string> languages;
  YAML::Node yaml = YAML::LoadFile("translate/languages.yaml");
  for (auto lang = yaml.begin(); lang != yaml.end(); ++lang) {
    auto key = lang->first.as<std::string>();
    auto value = lang->second.as<std::string>();
    if (reverse_order) {
      languages[value] = key;
    } else {
      languages[key] = value;
    }
  }
  return languages;
}

void
MainWindow::ChangeLanguage(const std::string& lang)
{
  this->settings.language = lang;

  /* Load the translation */
  std::string filename = "translate/" + lang + ".qm";
  if (not this->translator->load(QString::fromStdString(filename))) {
    this->main_ui->statusBar->showMessage(
      QObject::tr("Cannot find translation in ") +
      QString::fromStdString(filename));
    return;
  };

  /* Install the translation */
  if (not QApplication::installTranslator(this->translator.get())) {
    this->main_ui->statusBar->showMessage(
      QObject::tr("Failed to install translation"));
    return;
  }

  /* Update the UI */
  this->main_ui->retranslateUi(&this->main_window);
  this->overview_ui->retranslateUi(&this->main_window);
  this->player_ui->retranslateUi(&this->main_window);
  this->adversary_ui->retranslateUi(&this->main_window);
  this->combat_overview_ui->retranslateUi(&this->main_window);
  this->patron_ui->retranslateUi(&this->main_window);
  this->server_logs_ui->retranslateUi(&this->main_window);

  this->TabChanged();
  this->main_ui->statusBar->showMessage(QObject::tr("Language updated to ") +
                                        QString::fromStdString(lang));
}

void
MainWindow::GetNewLanguage()
{
  /* Get the list of languages */
  std::map<std::string, std::string> languages =
    this->ExtractLanguages(/* reverse_order */ true);
  QStringList choices;
  for (const auto& [key, value] : languages) {
    choices.push_back(QString::fromStdString(key));
  }

  /* Ask for the language */
  bool is_ok = false;
  QString item = QInputDialog::getItem(&this->main_window,
                                       QObject::tr("Change Language"),
                                       QObject::tr("Language:"),
                                       choices,
                                       0,
                                       false,
                                       &is_ok);

  if (not is_ok) {
    return;
  }

  this->ChangeLanguage(languages[item.toStdString()]);
}

void
MainWindow::SetDirectory()
{
  /* Get the new directory */
  QString dir = QFileDialog::getExistingDirectory(&this->main_window,
                                                  QObject::tr("Open Directory"),
                                                  "",
                                                  QFileDialog::ShowDirsOnly);
  if (dir.size() == 0) {
    return;
  }

  this->settings.directory = dir.toStdString();
  this->player_ui->SetDirectory(dir.toStdString());
  this->adversary_ui->SetDirectory(dir.toStdString());
  this->patron_ui->SetDirectory(dir.toStdString());

  this->main_ui->statusBar->showMessage(QObject::tr("Directory set to ") + dir);
}

void
MainWindow::StartOrStopServer()
{
  if (this->server.IsRunning()) {
    this->server.Stop();
    this->settings.use_server = false;
    this->main_ui->statusBar->showMessage("The server is stopped");
  } else {
    this->server.Start();
    this->settings.use_server = true;
    this->main_ui->statusBar->showMessage("The server is running");
  }
}

void
MainWindow::NameGenerator()
{
  NameGeneratorUi generator = NameGeneratorUi();
  generator.exec();
}

void
MainWindow::RandomEvent() const
{
  RandomEventUi event = RandomEventUi(this->GetDirectory() + "/events.ev");
  event.exec();
}

void
MainWindow::RandomFeatures()
{
  RandomFeaturesUi features = RandomFeaturesUi();
  features.exec();
}

void
MainWindow::RollsProbabilities()
{
  RollsProbabilitiesUi rolls = RollsProbabilitiesUi();
  rolls.exec();
}

void
MainWindow::ShuffleList() const
{
  ShuffleListUi shuffle = ShuffleListUi(this->GetDirectory() + "/List");
  shuffle.exec();
}

void
MainWindow::RuinsGenerator()
{
  RuinsUi ruins = RuinsUi();
  ruins.exec();
}

void
MainWindow::RewardExperience() const
{
  RewardExperiencecDialog dialog = RewardExperiencecDialog(this->fellowship);
  dialog.exec();
  this->player_ui->UpdateUi();
}

void
MainWindow::UpdateCors()
{
  bool is_ok = false;
  QString text =
    QInputDialog::getText(&this->main_window,
                          tr("Update CORS"),
                          tr("New CORS:"),
                          QLineEdit::Normal,
                          QString::fromStdString(this->settings.cors),
                          &is_ok);
  const std::string cors = text.toStdString();
  if (is_ok && !text.isEmpty() && cors != this->settings.cors) {
    this->settings.cors = cors;
    this->server.UpdateCors(cors);
    /* Warn the user about rebooting the app */
    QMessageBox::warning(&this->main_window,
                         tr("Restart needed"),
                         tr("The update of the CORS rule will be taken into "
                            "account next time you start this software."));
  }
}

void
MainWindow::ChangeTargetNumber() const
{
  const int def = 20;
  const int min = 0;
  const int max = 50;
  const int step = 1;

  bool is_ok = false;
  int value = QInputDialog::getInt(&this->main_window,
                                   tr("Changing Target Number"),
                                   tr("New Base Target Value"),
                                   def,
                                   min,
                                   max,
                                   step,
                                   &is_ok);

  if (is_ok) {
    QSettings().setValue(kSettingNames[kSettingPlayerMaxTarget], value);
  }
}

void
MainWindow::ConnectSignalSlots()
{
  /* Main menu */
  QObject::connect(this->main_ui->actionLanguage, &QAction::triggered, [=]() {
    this->GetNewLanguage();
  });
  QObject::connect(this->main_ui->actionSet_Directory,
                   &QAction::triggered,
                   [=]() { this->SetDirectory(); });
  QObject::connect(this->main_ui->actionStart_Stop_Server,
                   &QAction::triggered,
                   [=]() { this->StartOrStopServer(); });
  QObject::connect(this->main_ui->actionUpdate_Cors,
                   &QAction::triggered,
                   [=]() { this->UpdateCors(); });

  /* Players actions */
  QObject::connect(this->main_ui->actionSave_Players,
                   &QAction::triggered,
                   [=]() { this->SaveAllPlayers(); });
  QObject::connect(this->main_ui->actionLoad_All_Players,
                   &QAction::triggered,
                   [=]() { this->LoadAllPlayers(); });
  QObject::connect(
    this->main_ui->actionToggle_Player_Skills, &QAction::triggered, [=]() {
      this->settings.hide_player_skills = !this->settings.hide_player_skills;
      this->player_ui->HideSkills(this->settings.hide_player_skills);
    });
  QObject::connect(
    this->main_ui->actionToggle_Player_Image, &QAction::triggered, [=]() {
      this->settings.hide_player_image = !this->settings.hide_player_image;
      this->player_ui->HideImage(this->settings.hide_player_image);
    });
  QObject::connect(this->main_ui->actionReward_experience,
                   &QAction::triggered,
                   [=]() { this->RewardExperience(); });

  QObject::connect(this->main_ui->actionChange_Target_Number,
                   &QAction::triggered,
                   [=]() { this->ChangeTargetNumber(); });

  /* Adversaries actions */
  QObject::connect(this->main_ui->actionSave_Encounter,
                   &QAction::triggered,
                   [=]() { this->SaveEncounter(); });
  QObject::connect(this->main_ui->actionLoad_Encounter,
                   &QAction::triggered,
                   [=]() { this->LoadEncounter(); });

  /* Tools */
  QObject::connect(this->main_ui->actionName_Generator,
                   &QAction::triggered,
                   [=]() { this->NameGenerator(); });
  QObject::connect(this->main_ui->actionRandom_Event,
                   &QAction::triggered,
                   [=]() { this->RandomEvent(); });
  QObject::connect(this->main_ui->actionRandom_Features,
                   &QAction::triggered,
                   [=]() { this->RandomFeatures(); });
  QObject::connect(this->main_ui->actionRolls_Probabilities,
                   &QAction::triggered,
                   [=]() { this->RollsProbabilities(); });
  QObject::connect(this->main_ui->actionShuffle_List,
                   &QAction::triggered,
                   [=]() { this->ShuffleList(); });
  QObject::connect(this->main_ui->actionRuins_Generator,
                   &QAction::triggered,
                   [=]() { this->RuinsGenerator(); });

  /* Change tab */
  QObject::connect(this->main_ui->tabWidget,
                   &QTabWidget::currentChanged,
                   [=]() { this->TabChanged(); });

  /* Player UI */
  QObject::connect(this->player_ui.get(),
                   &PlayerUi::UpdateStatusBar,
                   [=](const QString& value) {
                     this->main_ui->statusBar->showMessage(value);
                   });
  QObject::connect(this->player_ui.get(),
                   &PlayerUi::lockChanged,
                   [=](bool value) { this->settings.lock_player = value; });

  /* Adversary UI */
  QObject::connect(this->adversary_ui.get(),
                   &AdversaryUi::UpdateStatusBar,
                   [=](const QString& value) {
                     this->main_ui->statusBar->showMessage(value);
                   });

  /* Overview UI */
  QObject::connect(this->overview_ui.get(), &OverviewUi::SaveToPlayer, [=]() {
    this->player_ui->SaveToPlayer();
  });

  /* Combat overview UI */
  QObject::connect(this->combat_overview_ui.get(),
                   &CombatOverviewUi::SaveToPlayer,
                   [=]() { this->player_ui->SaveToPlayer(); });

  /* Patron UI */
  QObject::connect(this->patron_ui.get(),
                   &PatronUi::UpdateStatusBar,
                   [=](const QString& value) {
                     this->main_ui->statusBar->showMessage(value);
                   });
  QObject::connect(
    this->patron_ui.get(), &PatronUi::PatronChanged, [=](Patron* new_patron) {
      this->fellowship->SetPatron(new_patron);
    });

  /* Server */
  QObject::connect(
    &this->server, &TheOneRingServer::UpdateUi, this, &MainWindow::TabChanged);
}

void
MainWindow::TabChanged() const
{
  const int expected_number_tabs = kTabCount;
  const int number_tabs = this->main_ui->tabWidget->count();
  /* Ensure that we are not currently destroying the main window */
  if (number_tabs != expected_number_tabs) {
    if (number_tabs > expected_number_tabs) {
      std::cout << "I guess you forgot about me!" << std::endl;
    } else {
      return;
    }
  }

  const int index = this->main_ui->tabWidget->currentIndex();

  switch (index) {
    case kTabCombatOverview:
      this->combat_overview_ui->UpdateUi();
      break;
    case kTabPlayers:
      this->player_ui->UpdateUi();
      break;
    case kTabAdversaries:
      this->adversary_ui->UpdateUi();
      break;
    case kTabOverview:
      this->overview_ui->UpdateUi();
      break;
    case kTabPatron:
      this->patron_ui->UpdateUi();
      break;
    case kTabServerLogs:
      this->server_logs_ui->UpdateLogs();
      break;
    default:
      std::cout << "Tab change not implemented";
  }
}
