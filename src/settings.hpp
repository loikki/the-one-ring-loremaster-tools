/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Standard includes */
#include <filesystem>
#include <fstream>

/* Standard library */
#include <iostream>

/* Other includes */
#include <QStandardPaths>
#include <yaml-cpp/yaml.h>

const std::string kHomeDirectory =
  QStandardPaths::writableLocation(QStandardPaths::HomeLocation).toStdString() +
  "/";

enum SettingEnum
{
  kSettingMainDirectory = 0,
  kSettingMainLanguage,
  kSettingPlayerHidePlayerSkills,
  kSettingPlayerHidePlayerImage,
  kSettingPlayerLockPlayer,
  kSettingPlayerMaxTarget,
  kSettingServerUseServer,
  kSettingServerCrossOriginResourceSharing,
  kSettingCount,
};

static const std::array<QString, kSettingCount> kSettingNames{
  "main/directory",          "main/language",
  "player/hidePlayerSkills", "player/hidePlayerImage",
  "player/lockPlayer",       "player/maxTarget",
  "server/useServer",        "server/crossOriginResourceSharing",
};

class Settings
{
public:
  explicit Settings();

  void SaveSettings() const;

  /* Attribute */
  bool hide_player_skills{ false };
  bool hide_player_image{ false };
  bool lock_player{ false };
  bool use_server{ false };
  std::string directory{ kHomeDirectory + ".cache/TheOneRing" };
  std::string language{ "en" };
  std::string cors{ "https://theonering-loremaster-tools.herokuapp.com" };
};
