/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_ENCOUNTER_HPP
#define LOREMASTER_TOOLS_SRC_ENCOUNTER_HPP

/* Own includes */
#include "adversary/adversary.hpp"

class Encounter
{

public:
  /* We should never have 0 adversary */
  Encounter() { this->NewAdversary(); }
  auto begin() { return this->adversaries.begin(); }
  auto end() { return this->adversaries.end(); }
  auto cbegin() { return this->adversaries.cbegin(); }
  auto cend() { return this->adversaries.cend(); }
  void LoadEncounter(const std::string& filename,
                     const std::string& adversary_directory);
  void SaveEncounter(const std::string& filename,
                     const std::string& adversary_directory) const;
  void RemoveAdversary(const Adversary* adversary);
  void NewAdversary() { this->adversaries.emplace_back(Adversary()); }

  Adversary& operator[](std::size_t idx) { return this->adversaries[idx]; }
  [[nodiscard]] std::size_t size() const noexcept
  {
    return this->adversaries.size();
  }

protected:
  /* Attributes */
  std::vector<Adversary> adversaries;
};

#endif // LOREMASTER_TOOLS_SRC_ENCOUNTER_HPP
