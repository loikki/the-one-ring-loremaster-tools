/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "patron_ui.hpp"

/* Other libraries */
#include <QDir>
#include <QFileDialog>
#include <QString>

PatronUi::PatronUi(QWidget& parent_ref, const std::string& dir)
  : Ui_Patron()
  , parent(parent_ref)
  , patrons(1)
  , current_patron(&this->patrons.back())
{
  this->SetDirectory(dir);
  this->setupUi(&parent_ref);
  this->ConnectSignalSlots();

  this->UpdateUi();
};

void
PatronUi::ConnectSignalSlots()
{
  /* Button */
  QObject::connect(this->new_patron, &QPushButton::clicked, [=]() {
    this->patrons.emplace_back(Patron());
    this->patron_choice->addItem("");
    this->patron_choice->setCurrentIndex(
      static_cast<int>(this->patrons.size()) - 1);
  });
  QObject::connect(this->save_patron, &QPushButton::clicked, [=]() {
    this->current_patron->SaveToFile(this->GetDirectory());
  });
  QObject::connect(this->load_patron, &QPushButton::clicked, [=]() {
    this->LoadPatron();
    // Ensure that the pointers is available everywhere
    int current = this->patron_choice->currentIndex();
    emit this->PatronChanged(&this->patrons[current]);
  });

  /* Input */
  QObject::connect(
    this->name, &QLineEdit::textChanged, [=](const QString& text) {
      this->current_patron->name = text.toStdString();
      int current = this->patron_choice->currentIndex();
      this->patron_choice->setItemText(current, text);
    });
  QObject::connect(this->advantages, &QPlainTextEdit::textChanged, [=]() {
    this->current_patron->advantages =
      this->advantages->toPlainText().toStdString();
  });
  QObject::connect(
    this->fellowship_points, &QSpinBox::valueChanged, [=](int value) {
      this->current_patron->fellowship_points = value;
    });
  QObject::connect(
    this->patron_choice, &QComboBox::currentIndexChanged, [=](int index) {
      this->current_patron = &this->patrons[index];
      this->UpdateUi();
      emit this->PatronChanged(&this->patrons[index]);
    });
}

void
PatronUi::LoadPatron(std::string filename)
{
  if (filename.length() == 0) {
    /* Get the directory */
    const std::string default_directory = this->GetDirectory();
    const QString qdefault_directory =
      QString::fromStdString(default_directory);

    /* Ensure that the directory exists */
    if (!QDir().mkpath(qdefault_directory)) {
      throw std::ios_base::failure("Failed to created the directory '" +
                                   default_directory + "'");
    };
    filename = QFileDialog::getOpenFileName(&this->parent,
                                            tr("Open Patron"),
                                            qdefault_directory,
                                            tr("Patron file (*.pat)"))
                 .toStdString();

    /* Check if we received something */
    if (filename.length() == 0) {
      return;
    }
  }

  /* Read it from the file */
  this->current_patron->ReadFromFile(filename);

  /* Update Ui */
  this->UpdateUi();

  emit this->UpdateStatusBar(tr("Patron loaded"));
}

void
PatronUi::UpdateUi()
{
  this->name->setText(QString::fromStdString(this->current_patron->name));
  this->fellowship_points->setValue(this->current_patron->fellowship_points);
  this->advantages->setPlainText(
    QString::fromStdString(this->current_patron->advantages));
};
