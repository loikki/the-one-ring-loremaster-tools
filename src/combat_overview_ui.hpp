/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_COMBAT_OVERVIEW_UI_HPP
#define LOREMASTER_TOOLS_SRC_COMBAT_OVERVIEW_UI_HPP

/* Own includes */
#include "gui/ui_combat_overview.h"

/* Other libraries */
#include <QObject>

/* Forward declaration */
class Fellowship;
class Encounter;

class CombatOverviewUi
  : public QObject
  , public Ui_combat_overview
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  CombatOverviewUi(const CombatOverviewUi&) = delete;
  CombatOverviewUi& operator=(const CombatOverviewUi&) = delete;
  CombatOverviewUi& operator=(CombatOverviewUi&&) = delete;
  CombatOverviewUi(CombatOverviewUi&&) = delete;
  CombatOverviewUi(QWidget& parent,
                   std::shared_ptr<Fellowship> fellowship,
                   std::shared_ptr<Encounter> encounter);

  ~CombatOverviewUi() override;

  void RemoveAllSummaries();
  void UpdateUi();

protected:
  void ConnectSignalSlots();

  /* Attributes */
  QWidget& parent;
  std::shared_ptr<Fellowship> fellowship{ nullptr };
  std::shared_ptr<Encounter> encounter{ nullptr };
  std::unique_ptr<QHBoxLayout> players_layout{ nullptr };
  std::unique_ptr<QHBoxLayout> adversaries_layout{ nullptr };

signals:
  void SaveToPlayer();
};

#endif // LOREMASTER_TOOLS_SRC_ADVERSARY_HPP
