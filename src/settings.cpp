/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "settings.hpp"

/* Other includes */
#include <QSettings>

constexpr int default_max_target = 20;

Settings::Settings()
{
  /* Ensure that the output directory exists */
  if (not std::filesystem::exists(this->directory)) {
    std::filesystem::create_directories(this->directory);
  }

  /* Prepare settings */
  QSettings settings;

  /* Read the settings */
  if (settings.contains(kSettingNames[kSettingMainDirectory])) {
    this->directory = settings.value(kSettingNames[kSettingMainDirectory])
                        .toString()
                        .toStdString();
  }
  if (settings.contains(kSettingNames[kSettingMainLanguage])) {
    this->language = settings.value(kSettingNames[kSettingMainLanguage])
                       .toString()
                       .toStdString();
  }
  if (settings.contains(kSettingNames[kSettingPlayerHidePlayerSkills])) {
    this->hide_player_skills =
      settings.value(kSettingNames[kSettingPlayerHidePlayerSkills]).toBool();
  }
  if (settings.contains(kSettingNames[kSettingPlayerHidePlayerImage])) {
    this->hide_player_image =
      settings.value(kSettingNames[kSettingPlayerHidePlayerImage]).toBool();
  }
  if (settings.contains(kSettingNames[kSettingPlayerLockPlayer])) {
    this->lock_player =
      settings.value(kSettingNames[kSettingPlayerLockPlayer]).toBool();
  }
  if (settings.contains(kSettingNames[kSettingServerUseServer])) {
    this->use_server =
      settings.value(kSettingNames[kSettingServerUseServer]).toBool();
  }
  if (settings.contains(
        kSettingNames[kSettingServerCrossOriginResourceSharing])) {
    this->cors =
      settings.value(kSettingNames[kSettingServerCrossOriginResourceSharing])
        .toString()
        .toStdString();
  }

  if (!settings.contains(kSettingNames[kSettingPlayerMaxTarget])) {
    settings.setValue(kSettingNames[kSettingPlayerMaxTarget],
                      default_max_target);
  }
};

void
Settings::SaveSettings() const
{
  QSettings settings;

  /* Main */
  settings.setValue(kSettingNames[kSettingMainDirectory],
                    QString::fromStdString(this->directory));
  settings.setValue(kSettingNames[kSettingMainLanguage],
                    QString::fromStdString(this->language));

  /* Player */
  settings.setValue(kSettingNames[kSettingPlayerHidePlayerSkills],
                    this->hide_player_skills);
  settings.setValue(kSettingNames[kSettingPlayerHidePlayerImage],
                    this->hide_player_image);
  settings.setValue(kSettingNames[kSettingPlayerLockPlayer], this->lock_player);

  /* Server */
  settings.setValue(kSettingNames[kSettingServerUseServer], this->use_server);
  settings.setValue(kSettingNames[kSettingServerCrossOriginResourceSharing],
                    QString::fromStdString(this->cors));

  /* Write to file */
  settings.sync();
};
