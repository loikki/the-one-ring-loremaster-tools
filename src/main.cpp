/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Own includes */
#include "main_window.hpp"
#include "player/player.hpp"
#include "player/player_ui.hpp"

/* C system headers */

/* C++ standard library */

/* Other includes */

int
main(int argc, char* argv[])
{

  /* Create the required windows */
  QApplication app(argc, argv);

  QCoreApplication::setOrganizationName("Loikki");
  QCoreApplication::setApplicationName("The One Ring - Loremaster Tools");

  QApplication::setWindowIcon(QIcon("favicon.ico"));
  QMainWindow main_window;
  MainWindow own_main(main_window, app);

  /* Run the app */
  main_window.show();
  return QApplication::exec();
}
