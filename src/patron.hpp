/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PATRON_HPP
#define LOREMASTER_TOOLS_SRC_PATRON_HPP

/* Other libraries */
#include <QJsonObject>

class Patron
{
public:
  void ReadFromFile(const std::string& filename);
  [[nodiscard]] QJsonObject GetJson() const;
  void SaveToFile(const std::string& directory) const;
  void RestoreFromJson(const QJsonObject& object);
  [[nodiscard]] std::string GetFilename(const std::string& directory) const;

  std::string name;
  int fellowship_points{ 0 };
  std::string advantages;
};

#endif // LOREMASTER_TOOLS_SRC_PATRON_HPP
