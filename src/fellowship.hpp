/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_FELLOWSHIP_HPP
#define LOREMASTER_TOOLS_SRC_FELLOWSHIP_HPP

/* Own includes */
#include "patron.hpp"
#include "player/player.hpp"

/* Other libraries */
#include <QJsonObject>

class Fellowship
{
public:
  /* We should never have 0 player */
  Fellowship() { this->NewPlayer(); }
  auto begin() { return this->players.begin(); }
  auto end() { return this->players.end(); }
  auto cbegin() { return this->players.cbegin(); }
  auto cend() { return this->players.cend(); }
  void LoadAllPlayers(const std::string& directory);
  void SaveAllPlayers(const std::string& directory);
  void RemovePlayer(const Player* player);
  void NewPlayer() { this->players.emplace_back(Player()); }
  void ResetFellowshipPoints()
  {
    int patron_points =
      this->patron != nullptr ? this->patron->fellowship_points : 0;
    // TODO(loikki): include virtues + others
    this->fellowship_points =
      static_cast<int>(this->players.size()) + patron_points;
  }
  void SetPatron(Patron* new_patron) { this->patron = new_patron; }
  [[nodiscard]] Patron* GetPatron() const { return this->patron; }

  Player* GetPlayer(const std::string& name);
  [[nodiscard]] QJsonObject GetJson() const;

  Player& operator[](std::size_t idx) { return this->players[idx]; }
  [[nodiscard]] std::size_t size() const noexcept
  {
    return this->players.size();
  }

  /* Public attributes */
  int fellowship_points{ 0 };

protected:
  /* Attributes */
  std::vector<Player> players;
  Patron* patron{ nullptr };
};

#endif // LOREMASTER_TOOLS_SRC_FELLOWSHIP_HPP
