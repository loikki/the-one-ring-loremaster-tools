/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_RANDOM_EVENT_UI_HPP
#define LOREMASTER_TOOLS_SRC_RANDOM_EVENT_UI_HPP

/* Own includes */
#include "../enums.hpp"
#include "../gui/ui_event.h"

/* Other includes */
#include <QDialog>
#include <QJsonArray>
#include <QJsonObject>

class RandomEventUi
  : public QDialog
  , public Ui_EventDialog
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  RandomEventUi& operator=(RandomEventUi&&) = delete;
  RandomEventUi(RandomEventUi&&) = delete;
  RandomEventUi(const RandomEventUi&) = delete;
  RandomEventUi& operator=(const RandomEventUi&) = delete;
  explicit RandomEventUi(std::string events_done_filename);
  ~RandomEventUi() override;

protected:
  void Roll(bool only_new_event = true);
  void SaveEvent();
  void UpdateUi();
  void ConnectSignalSlots();

  QJsonArray data;
  QJsonArray events_done;
  QJsonObject event;
  bool already_done{ false };
  std::string events_done_filename;
};

#endif // LOREMASTER_TOOLS_SRC_RANDOM_EVENT_UI_HPP
