/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_REWARD_EXPERIENCE_DIALOG_HPP
#define LOREMASTER_TOOLS_SRC_REWARD_EXPERIENCE_DIALOG_HPP

/* Own includes */
#include "../fellowship.hpp"
#include "../gui/ui_reward_experience.h"
#include "../player/player_ui.hpp"

/* Other includes */
#include <QDialog>

class RewardExperiencecDialog
  : public QDialog
  , public Ui_RewardExperience
{
public:
  explicit RewardExperiencecDialog(std::shared_ptr<Fellowship> new_fellowship)
    : Ui_RewardExperience()
    , fellowship(std::move(new_fellowship))
  {
    this->setupUi(this);
    this->ConnectSignalSlots();
  }

  void ConnectSignalSlots()
  {
    QObject::connect(
      this->cancel_button, &QPushButton::clicked, [=]() { this->done(1); });
    QObject::connect(this->ok_button, &QPushButton::clicked, [=]() {
      this->RewardExperience();
    });
  }

  void RewardExperience()
  {
    const int adventure_points = this->adventure->value();
    const int skill_points = this->skill->value();
    const bool use_wits = this->add_wits->isChecked();
    for (Player& current : *this->fellowship) {
      current.statistics.AddExperience(
        skill_points, adventure_points, use_wits);
    }
    this->accept();
  }

protected:
  std::shared_ptr<Fellowship> fellowship;
};

#endif // LOREMASTER_TOOLS_SRC_REWARD_EXPERIENCE_DIALOG_HPP
