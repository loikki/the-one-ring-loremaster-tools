/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_ROLLS_PROBABILITIES_UI_HPP
#define LOREMASTER_TOOLS_SRC_ROLLS_PROBABILITIES_UI_HPP

/* Own includes */
#include "../gui/ui_rolls.h"
#include "../random.hpp"

/* Other includes */
#include <QDialog>

const int kTargetNumberMax = 30;
const int kTargetNumberMin = 5;

struct RollPoint
{
  int x;
  float y;
};

struct RollStatistics
{
  std::vector<RollPoint> success{ 0 };
  float average_tengwar{ 0 };
  float average_value{ 0 };
};

class RollsProbabilitiesUi
  : public QDialog
  , public Ui_RollsDialog
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  RollsProbabilitiesUi();

protected:
  void ResetAxes();
  void CreateGraph();

  static RollStatistics ComputeProbabilities(const int number_rolls,
                                             const RollType roll_type,
                                             const int number_dice,
                                             const bool is_weary,
                                             const bool is_miserable);

  void ConnectSignalSlots();

  static const int kFeatDifferentValues = 10;
  QChart* chart{ new QChart() };
};

#endif // LOREMASTER_TOOLS_SRC_ROLLS_PROBABILITIES_UI_HPP
