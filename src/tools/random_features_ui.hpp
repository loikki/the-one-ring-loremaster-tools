/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_RANDOM_FEATURES_UI_HPP
#define LOREMASTER_TOOLS_SRC_RANDOM_FEATURES_UI_HPP

/* Own includes */
#include "../gui/ui_random_features.h"

/* Other includes */
#include <QDialog>
#include <QJsonArray>

const QString positive_features(QObject::tr("Positive"));
const QString negative_features(QObject::tr("Negative"));
const QString both_features(QObject::tr("Both"));

class RandomFeaturesUi
  : public QDialog
  , public Ui_RandomFeatures
{
public:
  RandomFeaturesUi();

protected:
  const int kNumberFeatures = 10;
  const float kProbabilityPositiveFeatures = 0.5;
  void NewFeatures();

  void ConnectSignalSlots();

  QJsonArray positives;
  QJsonArray negatives;
};

#endif // LOREMASTER_TOOLS_SRC_RANDOM_FEATURES_UI_HPP
