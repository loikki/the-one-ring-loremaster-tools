/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_RUINS_HPP
#define LOREMASTER_TOOLS_SRC_RUINS_HPP

/* Own includes */
#include "../gui/ui_ruins.h"

/* Other includes */
#include <QDialog>

class RuinsUi
  : public QDialog
  , public Ui_Ruins
{

public:
  RuinsUi& operator=(RuinsUi&&) = delete;
  RuinsUi(RuinsUi&&) = delete;
  RuinsUi& operator=(const RuinsUi&) = delete;
  RuinsUi(const RuinsUi&) = delete;
  ~RuinsUi() override = default;

  RuinsUi()
    : Ui_Ruins()
  {
    this->setupUi(this);
    this->ConnectSignalSlots();
    this->Roll();
  };

protected:
  void Roll();
  void ConnectSignalSlots();
  const std::array<std::string, 4> original_builders = {
    "Dúnedain",
    "Dwarves",
    "Elves",
    "Servants of the Shadow"
  };
  const std::array<std::string, 12> original_purpose = {
    "Archive", "Decorative", "Villa",       "Fortification",
    "Hall",    "Inn",        "Observatory", "Road",
    "Tomb",    "Workshop",   "Wondrous",    "Dark Purposes"
  };
  const std::array<std::string, 6> current_appearance = {
    "Crumbling",           "Fallen", "Overgrown", "Partially Buried",
    "Partially Collapsed", "Sunken"
  };
  const std::array<std::string, 5> current_usage = { "Abandonned",
                                                     "Hideout / Lair",
                                                     "Infested",
                                                     "Refuge",
                                                     "Haunted" };
  const std::array<int, 5> current_usage_weights = { 8, 1, 1, 1, 1 };
};

#endif // LOREMASTER_TOOLS_SRC_RUINS_HPP
