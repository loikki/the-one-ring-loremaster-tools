/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "random_event_ui.hpp"
#include "../random.hpp"
#include "../utils.hpp"

/* Other libraries */
#include <QFileInfo>
#include <QJsonValue>

RandomEventUi::RandomEventUi(std::string events_done_filename_ref)
  : Ui_EventDialog()
  , events_done_filename(std::move(events_done_filename_ref))
{
  this->setupUi(this);

  /* Read the events already done */
  QFileInfo info(QString::fromStdString(events_done_filename));
  if (info.exists()) {
    this->events_done = ReadJsonArray(this->events_done_filename);
  }

  /* Add roles */
  for (int i = 0; i < kJourneyRoleCount; i++) {
    // NOLINTNEXTLINE
    this->role->addItem(kJourneyRoleString[i]);
  }
  this->role->addItem(tr("No Role"));

  /* Add event types */
  for (int i = 0; i < kEventTypeCount; i++) {
    // NOLINTNEXTLINE
    this->event_type->addItem(kEventTypeString[i]);
  }

  /* Read the data */
  this->data = ReadJsonArray("data/random_event.json");

  /* Set the UI */
  this->Roll();

  /* Connect slots */
  this->ConnectSignalSlots();
};

RandomEventUi::~RandomEventUi()
{
  /* Save the list of events done */
  WriteJsonToFile(this->events_done, this->events_done_filename);
}

void
RandomEventUi::Roll(bool only_new_event)
{
  if (only_new_event) {
    this->already_done = false;
  }

  /* Get the type of event */
  const QString current_role = this->role->currentText();
  const QString current_event_type = this->event_type->currentText();

  /* Get all the events */
  std::vector<QJsonObject> objects;
  // NOLINTNEXTLINE
  for (const QJsonValue& val : this->data) {
    /* Get the object */
    const QJsonObject obj = val.toObject();

    /* Check if the event type is compatible */
    if (obj["event_type"].toString() != current_event_type) {
      continue;
    }

    /* Check if the role is compatible */
    const QString event_role = obj["role"].toString();
    if (not event_role.contains(current_role) &&
        not event_role.contains("all")) {
      continue;
    }

    /* Check if the event is already done */
    if (only_new_event && this->events_done.contains(obj["title"])) {
      continue;
    }

    /* Add the element to the list */
    objects.push_back(obj);
  }

  /* Check if we have some elements */
  if (objects.empty()) {
    /* Avoid infinite loops */
    if (this->already_done) {
      this->event = QJsonObject();
      this->UpdateUi();
      return;
    }

    this->already_done = true;
    this->Roll(false);
    return;
  }

  /* Pick a random one */
  this->event = *select_randomly(objects.cbegin(), objects.cend());

  /* Update the Ui */
  this->UpdateUi();
};

void
RandomEventUi::SaveEvent()
{
  if (not this->events_done.contains(this->event["title"])) {
    this->events_done.push_back(this->event["title"]);
  }
};

void
RandomEventUi::UpdateUi()
{
  /* Set recommended skill */
  std::map<QString, QString> skills = {
    { kJourneyRoleString[kJourneyRoleScout], tr("Craft or Explore") },
    { kJourneyRoleString[kJourneyRoleLookout], tr("Awareness or Scan") },
    { kJourneyRoleString[kJourneyRoleHunter], tr("Athletics or Hunting") },
    { kJourneyRoleString[kJourneyRoleGuide], tr("Lore or Travel") },
    { tr("No Role"), tr("Travel") }
  };
  const QString current_role = this->role->currentText();
  this->recommended_skills->setText(skills[current_role]);

  /* Check if we have an event */
  if (this->event.isEmpty()) {
    this->title->setText(tr("No event found"));
    this->description->setHtml("");
    return;
  }

  /* Set simple labels */
  this->title->setText(this->event["title"].toString());

  /* Description */
  QString current_description;
  if (this->already_done) {
    current_description =
      tr("<span style=\"font-weight:600;\">No new event.</span><br/>");
  }
  current_description += this->event["description"].toString();
  this->description->setHtml(current_description);
};

void
RandomEventUi::ConnectSignalSlots()
{
  /* Button */
  QObject::connect(
    this->roll_button, &QPushButton::clicked, [=]() { this->Roll(); });
  QObject::connect(
    this->done_button, &QPushButton::clicked, [=]() { this->accept(); });
  QObject::connect(this->save_event_button, &QPushButton::clicked, [=]() {
    this->SaveEvent();
  });

  /* Combobox */
  QObject::connect(
    this->role, &QComboBox::currentIndexChanged, [=]() { this->Roll(); });
  QObject::connect(
    this->event_type, &QComboBox::currentIndexChanged, [=]() { this->Roll(); });
}
