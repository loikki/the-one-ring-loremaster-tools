/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "shuffle_list_ui.hpp"
#include "../random.hpp"

/* Other libraries */
#include <QFileDialog>

ShuffleListUi::ShuffleListUi(std::string directory_arg)
  : Ui_ShuffleListDialog()
  , directory(std::move(directory_arg))
{
  this->setupUi(this);

  /* Connect the signals */
  this->ConnectSignalSlots();
};

void
ShuffleListUi::Save()
{
  QString filename =
    QFileDialog::getSaveFileName(this,
                                 tr("Save list"),
                                 QString::fromStdString(this->directory),
                                 tr("List file (*.list)"));

  /* Check if we received something */
  if (filename.length() == 0) {
    return;
  }

  QFile file(filename);
  file.open(QIODevice::WriteOnly);
  file.write(this->text_edit->toPlainText().toUtf8());
  file.close();
};

void
ShuffleListUi::Load(QString filename)
{
  if (filename.size() == 0) {
    filename =
      QFileDialog::getOpenFileName(this,
                                   tr("Open List"),
                                   QString::fromStdString(this->directory),
                                   tr("List file (*.list)"));

    /* Check if we received something */
    if (filename.length() == 0) {
      return;
    }
  }

  QFile file(filename);
  file.open(QIODevice::ReadOnly);
  this->text_edit->setText(file.readAll());
  file.close();
};

void
ShuffleListUi::Shuffle()
{
  QStringList list = this->text_edit->toPlainText().split("\n");
  /* Remove empty lines */
  if (list.back().isEmpty()) {
    list.pop_back();
  }
  shuffle(list.begin(), list.end());
  this->text_edit->setText(list.join("\n"));
};

void
ShuffleListUi::ConnectSignalSlots()
{
  /* Button */
  QObject::connect(
    this->ok_button, &QPushButton::clicked, [=]() { this->accept(); });
  QObject::connect(
    this->save_button, &QPushButton::clicked, [=]() { this->Save(); });
  QObject::connect(
    this->load_button, &QPushButton::clicked, [=]() { this->Load(); });
  QObject::connect(
    this->shuffle_button, &QPushButton::clicked, [=]() { this->Shuffle(); });
}
