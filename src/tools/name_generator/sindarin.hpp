/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_SINDARIN_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_SINDARIN_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

using s_array = std::array<QString, 3>;

class SindarinNameGenerator : public AbstractNameGenerator
{
public:
  [[nodiscard]] bool HasNeutral() const override { return true; };

  SindarinNameGenerator()
  {
    this->data = ReadJsonObject("data/sindarin.json");
  };
  [[nodiscard]] std::string GetName(const bool male, const bool female) override
  {
    const QJsonObject first = this->data["first"].toObject();
    const QJsonObject second = this->data["second"].toObject();

    /* Output variables */
    QString str1;
    QString str1_desc;
    std::list<s_array> output;

    const double probability = GetProbability();
    if (probability < kLowProbability) {
      /* Get a random entry */
      QStringList keys = first.keys();
      str1 = *select_randomly(keys.cbegin(), keys.cend());
      str1_desc = first[str1].toString();

      /* Get complement */
      output =
        SindarinNameGenerator::GetComplementFirstCase(str1, male, female);
    } else {
      /* Get a random entry */
      QStringList keys = second.keys();
      str1 = *select_randomly(keys.cbegin(), keys.cend());
      str1_desc = second[str1].toString();

      /* Get complement */
      output =
        SindarinNameGenerator::GetComplementSecondCase(str1, male, female);
      const s_array tmp = *select_randomly(output.cbegin(), output.cend());
    }

    const s_array tmp = *select_randomly(output.cbegin(), output.cend());

    /* Format output */
    std::string name = (str1 + tmp[2]).toStdString();
    QString description = str1 + " (" + str1_desc;
    if (tmp[2].isEmpty()) {
      description += +")";
    } else {
      description += +") + " + tmp[0] + " (" + tmp[1] + ")";
    }
    return ToTitleCase(name) + " = " + description.toStdString();
  }

private:
  const float kLowProbability = 0.5;

  // NOLINTNEXTLINE
  [[nodiscard]] static std::list<s_array> GetComplementFirstCaseFemale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a") || str1.endsWith("â")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ahel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "awen" }));
      output.emplace_back(s_array({ "neth", "Girl", "aneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "anis" }));
      output.emplace_back(s_array({ "dess", "Woman", "anes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "anith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "athel" }));
      output.emplace_back(s_array({ "bess", "Wife", "aves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("e") || str1.endsWith("ê")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ehel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "ewen" }));
      output.emplace_back(s_array({ "neth", "Girl", "eneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "enis" }));
      output.emplace_back(s_array({ "dess", "Woman", "enes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "enith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "ethel" }));
      output.emplace_back(s_array({ "bess", "Wife", "eves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("i") || str1.endsWith("í") || str1.endsWith("î")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ihel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "iwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "ineth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "inis" }));
      output.emplace_back(s_array({ "dess", "Woman", "ines" }));
      output.emplace_back(s_array({ "nîth", "Sister", "inith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "ithel" }));
      output.emplace_back(s_array({ "bess", "Wife", "ives" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("o") || str1.endsWith("ô")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ohel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "owen" }));
      output.emplace_back(s_array({ "neth", "Girl", "oneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "onis" }));
      output.emplace_back(s_array({ "dess", "Woman", "ones" }));
      output.emplace_back(s_array({ "nîth", "Sister", "onith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "othel" }));
      output.emplace_back(s_array({ "bess", "Wife", "oves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("u") || str1.endsWith("û")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "uhel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "uwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "uneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "unis" }));
      output.emplace_back(s_array({ "dess", "Woman", "unes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "unith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "uthel" }));
      output.emplace_back(s_array({ "bess", "Wife", "uves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("b")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "bess", "Wife", "es" }));
    }

    else if (str1.endsWith("c")) {
      output.emplace_back(s_array({ "eth", "Female", "geth" }));
      output.emplace_back(s_array({ "el", "Female", "gel" }));
      output.emplace_back(s_array({ "il", "Female", "gil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "gien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "giel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "gwen" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("nd")) {
      output.emplace_back(s_array({ "eth", "Female", "neth" }));
      output.emplace_back(s_array({ "el", "Female", "nel" }));
      output.emplace_back(s_array({ "il", "Female", "nil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "nien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "niel" }));
      output.emplace_back(s_array({ "sell", "Girl", "hel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "gwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "thel" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("d")) {
      output.emplace_back(s_array({ "eth", "Female", "deth" }));
      output.emplace_back(s_array({ "el", "Female", "del" }));
      output.emplace_back(s_array({ "il", "Female", "dil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "dien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "diel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ssel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "dwen" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("f")) {
      output.emplace_back(s_array({ "eth", "Female", "veth" }));
      output.emplace_back(s_array({ "el", "Female", "vel" }));
      output.emplace_back(s_array({ "il", "Female", "vil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "vien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "viel" }));
      output.emplace_back(s_array({ "bess", "Wife", "ves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("g")) {
      output.emplace_back(s_array({ "eth", "Female", "geth" }));
      output.emplace_back(s_array({ "el", "Female", "gel" }));
      output.emplace_back(s_array({ "il", "Female", "gil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "gien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "giel" }));
      output.emplace_back(s_array({ "sell", "Girl", "gel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "gwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "gneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "gnis" }));
      output.emplace_back(s_array({ "dess", "Woman", "gnes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "gnith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "cthel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("ch")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "el" }));
    }

    else if (str1.endsWith("h")) {
      output.emplace_back(s_array({ "eth", "Female", "es" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "el" }));
      output.emplace_back(s_array({ "thêl", "Sister", "el" }));
    }

    else if (str1.endsWith("ll") || str1.endsWith("l")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "hel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "wen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "thel" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));

      if (str1.endsWith("ll")) {
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("m")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));
    }

    else if (str1.endsWith("n")) {
      output.emplace_back(s_array({ "eth", "Female", "neth" }));
      output.emplace_back(s_array({ "el", "Female", "nel" }));
      output.emplace_back(s_array({ "il", "Female", "nil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "nien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "niel" }));
      output.emplace_back(s_array({ "sell", "Girl", "ssel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "ngwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "ndis" }));
      output.emplace_back(s_array({ "dess", "Woman", "ndes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "nthel" }));
      output.emplace_back(s_array({ "bess", "Wife", "mes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("mp")) {
      output.emplace_back(s_array({ "eth", "Female", "meth" }));
      output.emplace_back(s_array({ "el", "Female", "mel" }));
      output.emplace_back(s_array({ "il", "Female", "mil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "mien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "miel" }));
      output.emplace_back(s_array({ "sell", "Girl", "hel" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "bess", "Wife", "mes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("p")) {
      output.emplace_back(s_array({ "eth", "Female", "beth" }));
      output.emplace_back(s_array({ "el", "Female", "bel" }));
      output.emplace_back(s_array({ "il", "Female", "bil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "bien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "biel" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "el", "Female", "el" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "sell", "Girl", "hel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "wen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "thel" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));
    }

    else if (str1.endsWith("ss")) {
      output.emplace_back(s_array({ "eth", "Female", "seth" }));
      output.emplace_back(s_array({ "el", "Female", "sel" }));
      output.emplace_back(s_array({ "il", "Female", "sil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "sien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "siel" }));
      output.emplace_back(s_array({ "sell", "Girl", "sel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "sengwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "seneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "sendis" }));
      output.emplace_back(s_array({ "dess", "Woman", "sendes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "senith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "senthel" }));
      output.emplace_back(s_array({ "bess", "Wife", "semes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("s")) {
      output.emplace_back(s_array({ "eth", "Female", "seth" }));
      output.emplace_back(s_array({ "el", "Female", "sel" }));
      output.emplace_back(s_array({ "il", "Female", "sil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "sien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "siel" }));
      output.emplace_back(s_array({ "sell", "Girl", "sel" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "bess", "Wife", "bes" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("lt")) {
      output.emplace_back(s_array({ "eth", "Female", "eth" }));
      output.emplace_back(s_array({ "il", "Female", "il" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "wen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "dis" }));
      output.emplace_back(s_array({ "dess", "Woman", "des" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "bess", "Wife", "ves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("nt")) {
      output.emplace_back(s_array({ "eth", "Female", "nneth" }));
      output.emplace_back(s_array({ "el", "Female", "nnel" }));
      output.emplace_back(s_array({ "il", "Female", "nnil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "nnien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "nniel" }));
      output.emplace_back(s_array({ "sell", "Girl", "nthel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "ngwen" }));
      output.emplace_back(s_array({ "neth", "Girl", "nneth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "ndis" }));
      output.emplace_back(s_array({ "dess", "Woman", "ndes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nnith" }));
      output.emplace_back(s_array({ "bess", "Wife", "mbes" }));
      str1.remove(str1.size() - 2, 2);
    }

    else if (str1.endsWith("t")) {
      output.emplace_back(s_array({ "eth", "Female", "teth" }));
      output.emplace_back(s_array({ "el", "Female", "tel" }));
      output.emplace_back(s_array({ "il", "Female", "til" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "tien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "tiel" }));
      output.emplace_back(s_array({ "sell", "Girl", "sel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("w")) {
      output.emplace_back(s_array({ "eth", "Female", "weth" }));
      output.emplace_back(s_array({ "el", "Female", "wel" }));
      output.emplace_back(s_array({ "il", "Female", "wil" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "wien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "wiel" }));
      output.emplace_back(s_array({ "sell", "Girl", "hel" }));
      output.emplace_back(s_array({ "gwend", "Maiden", "wen" }));
      output.emplace_back(s_array({ "neth", "Girl", "neth" }));
      output.emplace_back(s_array({ "dîs", "Bride", "nis" }));
      output.emplace_back(s_array({ "dess", "Woman", "nes" }));
      output.emplace_back(s_array({ "nîth", "Sister", "nith" }));
      output.emplace_back(s_array({ "thêl", "Sister", "thel" }));
      output.emplace_back(s_array({ "bess", "Wife", "ves" }));
      str1.remove(str1.size() - 1, 1);
    }

    else {
      throw std::logic_error("This should not happen");
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array> GetComplementFirstCaseMale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a") || str1.endsWith("â") || str1.endsWith("e") ||
        str1.endsWith("ê") || str1.endsWith("i") || str1.endsWith("í") ||
        str1.endsWith("î") || str1.endsWith("o") || str1.endsWith("ô") ||
        str1.endsWith("u") || str1.endsWith("û")) {
      output.emplace_back(s_array({ "daer", "Groom", "naer" }));
      output.emplace_back(s_array({ "dir", "Man", "nir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ven" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      output.emplace_back(s_array({ "hawn", "Brother", "chon" }));
      output.emplace_back(s_array({ "hanar", "Brother", "chanar" }));
    }

    else if (str1.endsWith("b")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "benn", "Husband", "en" }));
    }

    else if (str1.endsWith("c")) {
      output.emplace_back(s_array({ "on", "Male", "gon" }));
      output.emplace_back(s_array({ "ion", "Son of", "gion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("nd") || str1.endsWith("nt")) {
      output.emplace_back(s_array({ "on", "Male", "nnor" }));
      output.emplace_back(s_array({ "ion", "Son of", "nnion" }));
      output.emplace_back(s_array({ "daer", "Groom", "ndaer" }));
      output.emplace_back(s_array({ "dir", "Man", "ndir" }));
      output.emplace_back(s_array({ "benn", "Husband", "mben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "ndor" }));
      str1.remove(str1.size() - 2, 2);
    }

    else if (str1.endsWith("d")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "daer", "Groom", "aer" }));
      output.emplace_back(s_array({ "dir", "Man", "ir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "or" }));
    }

    else if (str1.endsWith("f")) {
      output.emplace_back(s_array({ "on", "Male", "von" }));
      output.emplace_back(s_array({ "ion", "Son of", "vion" }));
      output.emplace_back(s_array({ "benn", "Husband", "ven" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("g") || str1.endsWith("h")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "hawn", "Brother", "on" }));
      output.emplace_back(s_array({ "hanar", "Brother", "anar" }));
    }

    else if (str1.endsWith("l")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "daer", "Groom", "aer" }));
      output.emplace_back(s_array({ "dir", "Man", "ir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "or" }));

      if (str1.endsWith("ll")) {
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("m")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "daer", "Groom", "daer" }));
      output.emplace_back(s_array({ "dir", "Man", "dir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("n")) {
      output.emplace_back(s_array({ "on", "Male", "non" }));
      output.emplace_back(s_array({ "ion", "Son of", "nion" }));
      output.emplace_back(s_array({ "daer", "Groom", "ndaer" }));
      output.emplace_back(s_array({ "dir", "Man", "ndir" }));
      output.emplace_back(s_array({ "benn", "Husband", "men" }));
      output.emplace_back(s_array({ "tôr", "Brother", "thor" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("mp")) {
      output.emplace_back(s_array({ "on", "Male", "mon" }));
      output.emplace_back(s_array({ "ion", "Son of", "mion" }));
      output.emplace_back(s_array({ "daer", "Groom", "daer" }));
      output.emplace_back(s_array({ "dir", "Man", "dir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("p")) {
      output.emplace_back(s_array({ "on", "Male", "bon" }));
      output.emplace_back(s_array({ "ion", "Son of", "bion" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "daer", "Groom", "daer" }));
      output.emplace_back(s_array({ "dir", "Man", "dir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      output.emplace_back(s_array({ "hawn", "Brother", "chon" }));
      output.emplace_back(s_array({ "hanar", "Brother", "chanar" }));
    }

    else if (str1.endsWith("ss")) {
      output.emplace_back(s_array({ "on", "Male", "son" }));
      output.emplace_back(s_array({ "ion", "Son of", "sion" }));
      output.emplace_back(s_array({ "daer", "Groom", "sendaer" }));
      output.emplace_back(s_array({ "dir", "Man", "sendir" }));
      output.emplace_back(s_array({ "benn", "Husband", "semen" }));
      output.emplace_back(s_array({ "tôr", "Brother", "tor" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("s")) {
      output.emplace_back(s_array({ "on", "Male", "son" }));
      output.emplace_back(s_array({ "ion", "Son of", "sion" }));
      output.emplace_back(s_array({ "daer", "Groom", "daer" }));
      output.emplace_back(s_array({ "dir", "Man", "dir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ben" }));
      output.emplace_back(s_array({ "tôr", "Brother", "tor" }));
    }

    else if (str1.endsWith("lt")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "daer", "Groom", "daer" }));
      output.emplace_back(s_array({ "dir", "Man", "dir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ven" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      output.emplace_back(s_array({ "hawn", "Brother", "chon" }));
      output.emplace_back(s_array({ "hanar", "Brother", "chanar" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("t")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "tôr", "Brother", "or" }));
    }

    else if (str1.endsWith("w")) {
      output.emplace_back(s_array({ "on", "Male", "won" }));
      output.emplace_back(s_array({ "ion", "Son of", "wion" }));
      output.emplace_back(s_array({ "daer", "Groom", "naer" }));
      output.emplace_back(s_array({ "dir", "Man", "nir" }));
      output.emplace_back(s_array({ "benn", "Husband", "ven" }));
      output.emplace_back(s_array({ "tôr", "Brother", "dor" }));
      output.emplace_back(s_array({ "hawn", "Brother", "chon" }));
      output.emplace_back(s_array({ "hanar", "Brother", "chanar" }));
      str1.remove(str1.size() - 1, 1);
    }

    else {
      throw std::logic_error("This should not happen");
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array> GetComplementFirstCaseNeutral(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("b") || (str1.endsWith("p") && not str1.endsWith("mp"))) {
      output.emplace_back(s_array({ "pen", "Person", "en" }));
      output.emplace_back(s_array({ "", "", "" }));
    }

    else if (str1.endsWith("nd") || str1.endsWith("n") || str1.endsWith("nt")) {
      output.emplace_back(s_array({ "pen", "Person", "mben" }));
      output.emplace_back(s_array({ "", "", "nd" }));
      str1.remove(str1.size() - 2, 2);
    }

    else if (str1.endsWith("s")) {
      output.emplace_back(s_array({ "pen", "Person", "pen" }));
      output.emplace_back(s_array({ "", "", "" }));

      if (str1.endsWith("ss")) {
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("c") || str1.endsWith("d") || str1.endsWith("g") ||
             str1.endsWith("ch") ||
             (str1.endsWith("t") && not str1.endsWith("lt")) ||
             str1.endsWith("w")) {
      output.emplace_back(s_array({ "", "", "" }));
    }

    else if (str1.endsWith("f") || str1.endsWith("r")) {
      output.emplace_back(s_array({ "pen", "Person", "phen" }));
      output.emplace_back(s_array({ "", "", "" }));

      if (str1.endsWith("f")) {
        str1.remove(str1.size() - 1, 1);
      }
    }

    else {
      output.emplace_back(s_array({ "pen", "Person", "ben" }));
      output.emplace_back(s_array({ "", "", "" }));

      if (str1.endsWith("ll") || str1.endsWith("mp") || str1.endsWith("lt")) {
        str1.remove(str1.size() - 1, 1);
      }
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array>
  GetComplementFirstCase(QString& str1, const bool male, const bool female)
  {
    if (female and not male) {
      return GetComplementFirstCaseFemale(str1);
    } else if (male and not female) {
      return GetComplementFirstCaseMale(str1);
    } else {
      return GetComplementFirstCaseNeutral(str1);
    }
  }

  [[nodiscard]] static std::list<s_array> GetComplementSecondCaseFemale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a") || str1.endsWith("â")) {
      output.emplace_back(s_array({ "ril", "Female", "ril" }));
      output.emplace_back(s_array({ "dis", "Female", "adis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "riel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "rien" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("b") || str1.endsWith("h") || str1.endsWith("w")) {
      output.emplace_back(s_array({ "ril", "Female", "ril" }));
      output.emplace_back(s_array({ "dis", "Female", "edis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "riel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "rien" }));
    }

    else if (str1.endsWith("d")) {
      output.emplace_back(s_array({ "ril", "Female", "ril" }));
      output.emplace_back(s_array({ "dis", "Female", "is" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "issiel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "issien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "riel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "rien" }));
    }

    else if (str1.endsWith("f")) {
      output.emplace_back(s_array({ "ril", "Female", "vril" }));
      output.emplace_back(s_array({ "dis", "Female", "vedis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "vriel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "vrien" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("g")) {
      output.emplace_back(s_array({ "ril", "Female", "ril" }));
      output.emplace_back(s_array({ "dis", "Female", "nis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "nissiel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "nissien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "riel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "rien" }));
    }

    else if (str1.endsWith("l")) {
      output.emplace_back(s_array({ "ril", "Female", "lil" }));
      output.emplace_back(s_array({ "dis", "Female", "dis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "liel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "lien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "dissiel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "dissien" }));
    }

    else if (str1.endsWith("n")) {
      output.emplace_back(s_array({ "ril", "Female", "dhril" }));
      output.emplace_back(s_array({ "dis", "Female", "ndis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "ndissiel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ndissien" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "dhriel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "dhrien" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "ril", "Female", "il" }));
      output.emplace_back(s_array({ "dis", "Female", "dis" }));
      output.emplace_back(s_array({ "iell", "Daughter of", "iel" }));
      output.emplace_back(s_array({ "ien", "Daughter of", "ien" }));
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array> GetComplementSecondCaseMale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a") || str1.endsWith("â")) {
      output.emplace_back(s_array({ "on", "Male", "on" }));
      output.emplace_back(s_array({ "dir", "Male", "edir" }));
      output.emplace_back(s_array({ "ron", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("b") || str1.endsWith("h") || str1.endsWith("w")) {
      output.emplace_back(s_array({ "on", "Male", "edon" }));
      output.emplace_back(s_array({ "dir", "Male", "edir" }));
      output.emplace_back(s_array({ "ron", "Male", "ron" }));
      output.emplace_back(s_array({ "ion", "Son of", "rion" }));
    }

    else if (str1.endsWith("d")) {
      output.emplace_back(s_array({ "on", "Male", "edon" }));
      output.emplace_back(s_array({ "dir", "Male", "ir" }));
      output.emplace_back(s_array({ "ron", "Male", "ron" }));
      output.emplace_back(s_array({ "ion", "Son of", "rion" }));
    }

    else if (str1.endsWith("f")) {
      output.emplace_back(s_array({ "dir", "Male", "vedir" }));
      output.emplace_back(s_array({ "ron", "Male", "vron" }));
      output.emplace_back(s_array({ "ion", "Son of", "vrion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("g")) {
      output.emplace_back(s_array({ "ion", "Son of", "nirion" }));
      output.emplace_back(s_array({ "dir", "Male", "nir" }));
      output.emplace_back(s_array({ "ron", "Male", "ron" }));
      output.emplace_back(s_array({ "ion", "Son of", "rion" }));
    }

    else if (str1.endsWith("l")) {
      output.emplace_back(s_array({ "ion", "Son of", "lion" }));
      output.emplace_back(s_array({ "dir", "Male", "dir" }));
      output.emplace_back(s_array({ "ron", "Male", "lon" }));
      output.emplace_back(s_array({ "ion", "Son of", "dirion" }));
    }

    else if (str1.endsWith("n")) {
      output.emplace_back(s_array({ "ion", "Son of", "dhrion" }));
      output.emplace_back(s_array({ "dir", "Male", "ndir" }));
      output.emplace_back(s_array({ "ron", "Male", "dhron" }));
      output.emplace_back(s_array({ "ion", "Son of", "ndirion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({ "dir", "Male", "dir" }));
      output.emplace_back(s_array({ "ron", "Male", "on" }));
      output.emplace_back(s_array({ "ion", "Son of", "dirion" }));
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array>
  GetComplementSecondCase(QString& str1, const bool male, const bool female)
  {
    if (female && not male) {
      return GetComplementSecondCaseFemale(str1);
    } else if (male and not female) {
      return GetComplementSecondCaseMale(str1);
    } else {
      std::list<s_array> output;
      if (str1.endsWith("f")) {
        output.emplace_back(s_array({ "or", "Person", "vor" }));
        str1.remove(str1.size() - 1, 1);
      }

      else {
        output.emplace_back(s_array({ "or", "Person", "or" }));

        if (str1.endsWith("a") || str1.endsWith("â")) {
          str1.remove(str1.size() - 1, 1);
        }
      }
      return output;
    }
  }

  QJsonObject data;
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_SINDARIN_HPP
