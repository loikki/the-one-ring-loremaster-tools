/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HOBBIT_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HOBBIT_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class HobbitNameGenerator : public AbstractNameGenerator
{
public:
  HobbitNameGenerator() { this->data = ReadJsonObject("data/hobbit.json"); };
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /*female*/) override
  {
    /* First name */
    QJsonArray array =
      male ? this->data["male"].toArray() : this->data["female"].toArray();
    QJsonValue value = *select_randomly(array.cbegin(), array.cend());
    std::string name = value.toString().toStdString();

    /* Last name */
    array = this->data["family"].toArray();
    value = *select_randomly(array.cbegin(), array.cend());
    name += " " + value.toString().toStdString();

    return ToTitleCase(name);
  }

private:
  QJsonObject data;
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HOBBIT_HPP
