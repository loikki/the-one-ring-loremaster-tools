/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_EASTERLING_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_EASTERLING_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class EasterlingNameGenerator : public AbstractNameGenerator
{
public:
  EasterlingNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /* female */) override
  {
    if (male) {
      return this->GetMaleName();
    } else {
      return this->GetFemaleName();
    }
  }

private:
  [[nodiscard]] std::string GetMaleName() const
  {
    const std::string str1 =
      *select_randomly(this->m1.cbegin(), this->m1.cend());
    const std::string str2 =
      *select_randomly(this->m2.cbegin(), this->m2.cend());
    const std::string str4 =
      *select_randomly(this->m4.cbegin(), this->m4.cend());
    const std::string str5 =
      *select_randomly(this->m5.cbegin(), this->m5.cend());
    std::string str3;
    do {
      str3 = *select_randomly(this->m3.cbegin(), this->m3.cend());
    } while (str3 == str1 || str3 == str5);
    return ToTitleCase(str1 + str2 + str3 + str4 + str5);
  }

  [[nodiscard]] std::string GetFemaleName() const
  {
    const std::string str1 =
      *select_randomly(this->f1.cbegin(), this->f1.cend());
    const std::string str2 =
      *select_randomly(this->f2.cbegin(), this->f2.cend());
    const std::string str4 =
      *select_randomly(this->f4.cbegin(), this->f4.cend());
    const std::string str5 =
      *select_randomly(this->f5.cbegin(), this->f5.cend());
    std::string str3;
    do {
      str3 = *select_randomly(this->f3.cbegin(), this->f3.cend());
    } while (str3 == str1 || str3 == str5);
    return ToTitleCase(str1 + str2 + str3 + str4 + str5);
  }

  const std::array<std::string, 10> m1 = { "",  "b", "br", "d", "g",
                                           "k", "m", "n",  "s", "y" };
  const std::array<std::string, 7> m2 = { "ô", "ö", "o", "e", "i", "a", "u" };
  const std::array<std::string, 18> m3 = {
    "b",  "c", "dd", "dg", "kt", "l",  "ld",  "lf", "lt",
    "lw", "m", "mr", "r",  "rg", "rl", "rth", "st", "z",
  };
  const std::array<std::string, 7> m4 = { "a", "i", "î", "ö", "o", "u", "ü" };
  const std::array<std::string, 15> m5 = {
    "",  "",   "ç",  "ch", "d",   "g",  "k", "l",
    "n", "nd", "ng", "r",  "rth", "st", "z",
  };
  const std::array<std::string, 12> f1 = { "",  "",  "d", "g", "k", "m",
                                           "n", "r", "t", "v", "y", "z" };
  const std::array<std::string, 10> f2 = { "a", "u", "î", "i", "a",
                                           "u", "î", "i", "e", "o" };
  const std::array<std::string, 9> f3 = { "g", "gk", "l", "lk", "n",
                                          "r", "rk", "t", "z" };
  const std::array<std::string, 4> f4 = { "a", "e", "i", "u" };
  const std::array<std::string, 11> f5 = { "",   "",   "l", "n",  "g", "ge",
                                           "ke", "le", "n", "ne", "z" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_EASTERLING_HPP
