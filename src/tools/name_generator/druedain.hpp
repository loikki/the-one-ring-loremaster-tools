/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DRUEDAIN_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DRUEDAIN_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class DruedainNameGenerator : public AbstractNameGenerator
{
public:
  DruedainNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool /* male */,
                                    const bool /* female */) override
  {
    const std::string str1 =
      *select_randomly(this->l1.cbegin(), this->l1.cend());
    const std::string str2 =
      *select_randomly(this->l2.cbegin(), this->l2.cend());
    const std::string str3 =
      *select_randomly(this->l3.cbegin(), this->l3.cend());
    const std::string str4 =
      *select_randomly(this->l4.cbegin(), this->l4.cend());
    std::string str5;
    do {
      str5 = *select_randomly(this->l5.cbegin(), this->l5.cend());
    } while (str3 == str5);
    const std::string str6 =
      *select_randomly(this->l6.cbegin(), this->l6.cend());

    std::string name = ToTitleCase(str1) + str2 + "n-";
    name += str3 + str4 + str5 + str6 + "-" + str1 + str2 + "n";
    return name;
  }
  [[nodiscard]] bool HasGender() const override { return false; };

private:
  const std::array<std::string, 11> l1 = { "bh", "dh", "gh", "kh", "qh", "rh",
                                           "th", "vh", "wh", "xh", "zh" };
  const std::array<std::string, 3> l2 = { "â", "ô", "û" };
  const std::array<std::string, 9> l3 = { "b", "d", "g", "h", "m",
                                          "n", "r", "v", "z" };
  const std::array<std::string, 4> l4 = { "a", "e", "o", "u" };
  const std::array<std::string, 6> l5 = { "d", "m", "n", "r", "v", "z" };
  const std::array<std::string, 4> l6 = { "a", "i", "o", "u" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DRUEDAIN_HPP
