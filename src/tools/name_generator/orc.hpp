/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ORC_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ORC_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class OrcNameGenerator : public AbstractNameGenerator
{
public:
  OrcNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool /*male*/,
                                    const bool /*female*/) override
  {
    const double probability = GetProbability();
    if (probability < kLowProbability) {
      const std::string str1 =
        *select_randomly(this->l1.cbegin(), this->l1.cend());
      const std::string str2 =
        *select_randomly(this->l2.cbegin(), this->l2.cend());
      const std::string str3 =
        *select_randomly(this->l3.cbegin(), this->l3.cend());
      const std::string str2_2 =
        *select_randomly(this->l2.cbegin(), this->l2.cend());
      const std::string str4 =
        *select_randomly(this->l4.cbegin(), this->l4.cend());
      return ToTitleCase(str1 + str2 + str3 + str2_2 + str4);
    } else {
      const std::string str2 =
        *select_randomly(this->l2.cbegin(), this->l2.cend());
      const std::string str3 =
        *select_randomly(this->l3.cbegin(), this->l3.cend());
      const std::string str4 =
        *select_randomly(this->l4.cbegin(), this->l4.cend());
      const std::string str5 =
        *select_randomly(this->l5.cbegin(), this->l5.cend());
      return ToTitleCase(str5 + str3 + str2 + str4);
    }
  }
  [[nodiscard]] bool HasGender() const override { return false; };

private:
  const float kLowProbability = 0.5;
  const std::array<std::string, 17> l1 = {
    "b", "br", "c", "cr", "d", "dr", "g",  "gh", "gr",
    "k", "kr", "l", "m",  "r", "s",  "sh", "sr",
  };
  const std::array<std::string, 11> l2 = { "a", "e", "i", "o", "u", "a",
                                           "e", "i", "o", "u", "au" };
  const std::array<std::string, 31> l3 = {
    "cb", "cd", "cr",  "db",  "dd",  "fd", "fth", "g",  "gb", "gd", "gg",
    "gl", "gr", "gz",  "h",   "lcm", "ld", "lf",  "lg", "rb", "rc", "rd",
    "rg", "rz", "shn", "thr", "z",   "zb", "zg",  "zr", "zz",
  };
  const std::array<std::string, 17> l4 = {
    "c", "d",  "dh", "f", "g",  "gh", "kh", "l",
    "r", "rg", "sh", "t", "th", "",   "",   "",
  };
  const std::array<std::string, 4> l5 = { "a", "o", "u", "au" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ORC_HPP
