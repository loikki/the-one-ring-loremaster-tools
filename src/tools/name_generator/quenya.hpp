/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_QUENYA_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_QUENYA_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>
#include <initializer_list>
#include <iostream>

using s_array = std::array<QString, 3>;

class QuenyaNameGenerator : public AbstractNameGenerator
{
public:
  [[nodiscard]] bool HasNeutral() const override { return true; };

  QuenyaNameGenerator() { this->data = ReadJsonObject("data/quenya.json"); };
  [[nodiscard]] std::string GetName(const bool male, const bool female) override
  {
    const QJsonObject first = this->data["first"].toObject();
    const QJsonObject second = this->data["second"].toObject();

    /* Output variables */
    QString str1;
    QString str1_desc;
    std::list<s_array> output;

    const double probability = GetProbability();
    if (probability < kLowProbability) {
      /* Get a random entry */
      QStringList keys = first.keys();
      str1 = *select_randomly(keys.cbegin(), keys.cend());
      str1_desc = first[str1].toString();

      /* Get complement */
      output = QuenyaNameGenerator::GetComplementFirstCase(str1, male, female);
    } else {
      /* Get a random entry */
      QStringList keys = second.keys();
      str1 = *select_randomly(keys.cbegin(), keys.cend());
      str1_desc = second[str1].toString();

      /* Get complement */
      output = QuenyaNameGenerator::GetComplementSecondCase(str1, male, female);
      const s_array tmp = *select_randomly(output.cbegin(), output.cend());
    }

    const s_array tmp = *select_randomly(output.cbegin(), output.cend());

    /* Format output */
    std::string name = (str1 + tmp[2]).toStdString();
    QString description = str1 + " (" + str1_desc;
    if (tmp[2].isEmpty()) {
      description += +")";
    } else {
      description += +") + " + tmp[0] + " (" + tmp[1] + ")";
    }
    return ToTitleCase(name) + " = " + description.toStdString();
  }

private:
  const float kLowProbability = 0.5;

  [[nodiscard]] static std::list<s_array> GetComplementFirstCaseFemale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a")) {
      if (str1.endsWith("ya")) {
        output.emplace_back(s_array({ "më", "Woman who does", "yamë" }));
        output.emplace_back(s_array({ "ë", "Woman who does", "yë" }));
        output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
        output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
        output.emplace_back(s_array({ "issë", "Woman who does", "yissë" }));
        output.emplace_back(s_array({ "wendë", "Maiden", "yawen" }));
        output.emplace_back(s_array({ "níssë", "Female", "yanis" }));
        output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
        str1.remove(str1.size() - 2, 2);
      }

      else {
        output.emplace_back(s_array({ "më", "Woman who does", "amë" }));
        output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
        output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
        output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
        output.emplace_back(s_array({ "issë", "Woman who does", "issë" }));
        output.emplace_back(s_array({ "wendë", "Maiden", "awen" }));
        output.emplace_back(s_array({ "níssë", "Female", "anis" }));
        output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("e") || str1.endsWith("ë")) {
      output.emplace_back(s_array({ "më", "Woman who does", "emë" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "issë" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "ewen" }));
      output.emplace_back(s_array({ "níssë", "Female", "enis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("o")) {
      output.emplace_back(s_array({ "më", "Woman who does", "omë" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "issë" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "owen" }));
      output.emplace_back(s_array({ "níssë", "Female", "onis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("u")) {
      output.emplace_back(s_array({ "më", "Woman who does", "umë" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "issë" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "uwen" }));
      output.emplace_back(s_array({ "níssë", "Female", "unis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("l") || str1.endsWith("n")) {
      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "wen" }));
      output.emplace_back(s_array({ "níssë", "Female", "dis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
    }

    else if (str1.endsWith("r")) {

      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "të" }));
      output.emplace_back(s_array({ "ië", "Feminine", "tië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "tien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "wen" }));
      output.emplace_back(s_array({ "níssë", "Female", "dis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "tiel" }));
    }

    else if (str1.endsWith("s")) {
      output.emplace_back(s_array({ "më", "Woman who does", "smë" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "rë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "rië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "rien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "risse" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "swen" }));
      output.emplace_back(s_array({ "níssë", "Female", "snis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "riel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("th") || str1.endsWith("v")) {
      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "ië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "ien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "wen" }));
      output.emplace_back(s_array({ "níssë", "Female", "nis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "iel" }));
    }

    else if (str1.endsWith("t")) {
      output.emplace_back(s_array({ "më", "Woman who does", "tmë" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "cë" }));
      output.emplace_back(s_array({ "ië", "Feminine", "cië" }));
      output.emplace_back(s_array({ "ien", "Feminine", "cien" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "cisse" }));
      output.emplace_back(s_array({ "wendë", "Maiden", "twen" }));
      output.emplace_back(s_array({ "níssë", "Female", "tnis" }));
      output.emplace_back(s_array({ "iel", "Daughter of", "ciel" }));
      str1.remove(str1.size() - 1, 1);
    }

    else {
      throw std::logic_error("This should not happen");
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array> GetComplementFirstCaseMale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a")) {
      if (str1.endsWith("ya")) {
        output.emplace_back(s_array({ "o", "Man who does", "yo" }));
        output.emplace_back(s_array({ "on", "Masculine", "yon" }));
        output.emplace_back(s_array({ "no", "Man who does", "yano" }));
        output.emplace_back(s_array({ "mo", "Man who does", "yamo" }));
        output.emplace_back(s_array({ "nér", "Masculine", "yaner" }));
        output.emplace_back(s_array({ "ion", "Son of", "ion" }));
        str1.remove(str1.size() - 2, 2);
      } else {
        output.emplace_back(s_array({ "o", "Man who does", "o" }));
        output.emplace_back(s_array({ "on", "Masculine", "on" }));
        output.emplace_back(s_array({ "no", "Man who does", "ano" }));
        output.emplace_back(s_array({ "mo", "Man who does", "amo" }));
        output.emplace_back(s_array({ "nér", "Masculine", "aner" }));
        output.emplace_back(s_array({ "ion", "Son of", "ion" }));
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("e") || str1.endsWith("ë")) {
      output.emplace_back(s_array({ "o", "Man who does", "o" }));
      output.emplace_back(s_array({ "on", "Masculine", "on" }));
      output.emplace_back(s_array({ "no", "Man who does", "eno" }));
      output.emplace_back(s_array({ "mo", "Man who does", "emo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "ener" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({}));
      output.emplace_back(s_array({}));
      output.emplace_back(s_array({}));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("o")) {
      output.emplace_back(s_array({ "o", "Man who does", "o" }));
      output.emplace_back(s_array({ "on", "Masculine", "on" }));
      output.emplace_back(s_array({ "no", "Man who does", "ono" }));
      output.emplace_back(s_array({ "mo", "Man who does", "omo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "oner" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      output.emplace_back(s_array({}));
      output.emplace_back(s_array({}));
      output.emplace_back(s_array({}));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("u")) {
      output.emplace_back(s_array({ "o", "Man who does", "o" }));
      output.emplace_back(s_array({ "on", "Masculine", "on" }));
      output.emplace_back(s_array({ "no", "Man who does", "uno" }));
      output.emplace_back(s_array({ "mo", "Man who does", "umo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "uner" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("l") || str1.endsWith("n")) {
      output.emplace_back(s_array({ "o", "Man who does", "o" }));
      output.emplace_back(s_array({ "on", "Masculine", "on" }));
      output.emplace_back(s_array({ "no", "Man who does", "do" }));
      output.emplace_back(s_array({ "mo", "Man who does", "mo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "der" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "o", "Man who does", "no" }));
      output.emplace_back(s_array({ "on", "Masculine", "non" }));
      output.emplace_back(s_array({ "no", "Man who does", "do" }));
      output.emplace_back(s_array({ "mo", "Man who does", "mo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "der" }));
      output.emplace_back(s_array({ "ion", "Son of", "nion" }));
    }

    else if (str1.endsWith("s")) {
      output.emplace_back(s_array({ "o", "Man who does", "ro" }));
      output.emplace_back(s_array({ "on", "Masculine", "ron" }));
      output.emplace_back(s_array({ "no", "Man who does", "sno" }));
      output.emplace_back(s_array({ "mo", "Man who does", "smo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "sner" }));
      output.emplace_back(s_array({ "ion", "Son of", "rion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("t")) {
      output.emplace_back(s_array({ "o", "Man who does", "co" }));
      output.emplace_back(s_array({ "on", "Masculine", "con" }));
      output.emplace_back(s_array({ "no", "Man who does", "tno" }));
      output.emplace_back(s_array({ "mo", "Man who does", "tmo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "tner" }));
      output.emplace_back(s_array({ "ion", "Son of", "cion" }));
      str1.remove(str1.size() - 1, 1);
    }

    else if (str1.endsWith("th") || str1.endsWith("v")) {
      output.emplace_back(s_array({ "o", "Man who does", "o" }));
      output.emplace_back(s_array({ "on", "Masculine", "on" }));
      output.emplace_back(s_array({ "no", "Man who does", "no" }));
      output.emplace_back(s_array({ "mo", "Man who does", "mo" }));
      output.emplace_back(s_array({ "nér", "Masculine", "ner" }));
      output.emplace_back(s_array({ "ion", "Son of", "ion" }));
    } else {
      throw std::logic_error("This should not happen");
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array>
  GetComplementFirstCase(QString& str1, const bool male, const bool female)
  {
    std::list<s_array> output;
    if (female && not male) {
      output = GetComplementFirstCaseFemale(str1);
    } else if (male && not female) {
      output = GetComplementFirstCaseMale(str1);
    } else {
      if (str1.endsWith("ë")) {
        output.emplace_back(s_array({ "dur", "Servant of", "edur" }));
        output.emplace_back(s_array({ "dil", "Friend of", "edil" }));
        output.emplace_back(s_array({ "nil", "Friend of", "enil" }));
        output.emplace_back(s_array({ "quen", "Person", "equen" }));
        output.emplace_back(s_array({ "wë", "Person", "ewë" }));
        output.emplace_back(s_array({ "", "", "" }));
      } else {
        output.emplace_back(s_array({ "dur", "Servant of", "dur" }));
        output.emplace_back(s_array({ "dil", "Friend of", "dil" }));
        output.emplace_back(s_array({ "nil", "Friend of", "nil" }));
        output.emplace_back(s_array({ "quen", "Person", "quen" }));
        output.emplace_back(s_array({ "wë", "Person", "wë" }));
        output.emplace_back(s_array({ "", "", "" }));
        if (str1.endsWith("s")) {
          str1.remove(str1.size() - 1, 1);
        }
      }
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array> GetComplementSecondCaseFemale(
    QString& str1)
  {
    std::list<s_array> output;
    if (str1.endsWith("a")) {
      if (str1.endsWith("ya")) {
        output.emplace_back(s_array({ "rë", "Woman who does", "yarë" }));
        output.emplace_back(s_array({ "indë", "Woman who does", "yinde" }));
        output.emplace_back(s_array({ "llë", "Woman who does", "yallë" }));
        output.emplace_back(s_array({ "më", "Woman who does", "yamë" }));
        output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
        output.emplace_back(s_array({ "issë", "Woman who does", "yisse" }));
        str1.remove(str1.size() - 2, 2);
      }

      else {
        output.emplace_back(s_array({ "rë", "Woman who does", "arë" }));
        output.emplace_back(s_array({ "indë", "Woman who does", "inde" }));
        output.emplace_back(s_array({ "llë", "Woman who does", "allë" }));
        output.emplace_back(s_array({ "më", "Woman who does", "amë" }));
        output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
        output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
        str1.remove(str1.size() - 1, 1);
      }
    }

    else if (str1.endsWith("c") || str1.endsWith("l") || str1.endsWith("n") ||
             str1.endsWith("m") || str1.endsWith("p") || str1.endsWith("th") ||
             str1.endsWith("v")) {
      output.emplace_back(s_array({ "rë", "Woman who does", "rë" }));
      output.emplace_back(s_array({ "indë", "Woman who does", "inde" }));
      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "ë" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
    }

    else if (str1.endsWith("r")) {
      output.emplace_back(s_array({ "rë", "Woman who does", "rë" }));
      output.emplace_back(s_array({ "indë", "Woman who does", "inde" }));
      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "të" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));

    }

    else if (str1.endsWith("t") || str1.endsWith("h")) {
      output.emplace_back(s_array({ "rë", "Woman who does", "rë" }));
      output.emplace_back(s_array({ "indë", "Woman who does", "inde" }));
      output.emplace_back(s_array({ "më", "Woman who does", "më" }));
      output.emplace_back(s_array({ "ë", "Woman who does", "cë" }));
      output.emplace_back(s_array({ "issë", "Woman who does", "isse" }));
      str1.remove(str1.size() - 1, 1);
    }

    else {
      throw std::logic_error("This should not happen");
    }
    return output;
  }

  [[nodiscard]] static std::list<s_array>
  GetComplementSecondCase(QString& str1, const bool male, const bool female)
  {
    std::list<s_array> output;
    if (female && not male) {
      output = GetComplementSecondCaseFemale(str1);
    } else if (male && not female) {
      if (str1.endsWith("a")) {
        output.emplace_back(s_array({ "ro", "Man who does", "aro" }));
        output.emplace_back(s_array({ "o", "Man who does", "o" }));
        output.emplace_back(s_array({ "no", "Man who does", "ano" }));
        output.emplace_back(s_array({ "mo", "Man who does", "amo" }));
        str1.remove(str1.size() - 1, 1);
      }

      else if (str1.endsWith("c") || str1.endsWith("m") || str1.endsWith("p") ||
               str1.endsWith("th") || str1.endsWith("v")) {
        output.emplace_back(s_array({ "ro", "Man who does", "ro" }));
        output.emplace_back(s_array({ "o", "Man who does", "o" }));
        output.emplace_back(s_array({ "no", "Man who does", "no" }));
        output.emplace_back(s_array({ "mo", "Man who does", "mo" }));
      }

      else if (str1.endsWith("c") || str1.endsWith("l") || str1.endsWith("n") ||
               str1.endsWith("r")) {
        output.emplace_back(s_array({ "ro", "Man who does", "ro" }));
        output.emplace_back(s_array({ "o", "Man who does", "o" }));
        output.emplace_back(s_array({ "no", "Man who does", "do" }));
        output.emplace_back(s_array({ "mo", "Man who does", "mo" }));
      }

      else if (str1.endsWith("t")) {
        output.emplace_back(s_array({ "ro", "Man who does", "tro" }));
        output.emplace_back(s_array({ "o", "Man who does", "co" }));
        output.emplace_back(s_array({ "no", "Man who does", "tno" }));
        output.emplace_back(s_array({ "mo", "Man who does", "tmo" }));
        str1.remove(str1.size() - 1, 1);
      } else {
        throw std::logic_error("This should not happen");
      }
    } else {
      if (str1.endsWith("a")) {
        output.emplace_back(s_array({ "r", "Person who does", "r" }));
        output.emplace_back(s_array({ "", "", "" }));
      } else {
        output.emplace_back(s_array({ "", "", "" }));
      }
    }
    return output;
  }

  QJsonObject data;
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_QUENYA_HPP
