/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_MAIAR_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_MAIAR_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class MaiarNameGenerator : public AbstractNameGenerator
{
public:
  MaiarNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /*female*/) override
  {
    if (male) {
      return this->GetMaleName();
    } else {
      return this->GetFemaleName();
    }
  }

private:
  const int kSkip = 6;
  [[nodiscard]] std::string GetMaleName()
  {
    const std::string str1 =
      *select_randomly(this->m1.cbegin(), this->m1.cend());
    const std::string* str2 =
      &*select_randomly(this->m2.cbegin(), this->m2.cend());
    const std::string str3 =
      *select_randomly(this->m3.cbegin(), this->m3.cend());
    const std::string* str2_2 =
      &*select_randomly(this->m2.cbegin(), this->m2.cend());
    if (str2 - &*this->m2.cbegin() > kSkip) {
      while (str2_2 - &*this->m2.cbegin() > kSkip) {
        str2_2 = &*select_randomly(this->m2.cbegin(), this->m2.cend());
      }
    }
    const std::string str4 =
      *select_randomly(this->m4.cbegin(), this->m4.cend());
    const std::string str5 =
      *select_randomly(this->m5.cbegin(), this->m5.cend());
    std::string name = str1 + *str2 + str3 + *str2_2 + str4 + str5;
    return ToTitleCase(name);
  }

  [[nodiscard]] std::string GetFemaleName()
  {
    const std::string str1 =
      *select_randomly(this->f1.cbegin(), this->f1.cend());
    const std::string* str2 =
      &*select_randomly(this->f2.cbegin(), this->f2.cend());
    const std::string str3 =
      *select_randomly(this->f3.cbegin(), this->f3.cend());
    const std::string* str2_2 =
      &*select_randomly(this->f2.cbegin(), this->f2.cend());
    if (str2 - &*this->f2.cbegin() > kSkip) {
      while (str2_2 - &*this->f2.cbegin() > kSkip) {
        str2_2 = &*select_randomly(this->f2.cbegin(), this->f2.cend());
      }
    }
    const std::string str4 =
      *select_randomly(this->f4.cbegin(), this->f4.cend());
    const std::string str5 =
      *select_randomly(this->f5.cbegin(), this->f5.cend());
    std::string name = str1 + *str2 + str3 + *str2_2 + str4 + str5;
    return ToTitleCase(name);
  }

  const std::array<std::string, 12> m1 = { "c", "k", "l", "m",  "n", "p",
                                           "r", "s", "t", "th", "",  "" };
  const std::array<std::string, 13> m2 = { "a",  "e",  "o",  "i",  "u",
                                           "ó",  "é",  "ai", "eo", "io",
                                           "eö", "uo", "ua" };
  const std::array<std::string, 16> m3 = {
    "l",  "ll", "lm", "ln", "ls", "m",  "md", "n",
    "nd", "nm", "nw", "r",  "s",  "ss", "t",  "w",
  };
  const std::array<std::string, 8> m4 = { "l", "m", "n", "nd",
                                          "r", "s", "t", "th" };
  const std::array<std::string, 8> m5 = { "o", "e", "ë", "ó", "", "", "", "" };
  const std::array<std::string, 12> f1 = { "f",  "l", "m", "n", "ph", "s",
                                           "sh", "w", "y", "z", "",   "" };
  const std::array<std::string, 13> f2 = { "a",  "e",  "o",  "i",  "u",
                                           "ó",  "é",  "ie", "ui", "ia",
                                           "ea", "ae", "ua" };
  const std::array<std::string, 12> f3 = { "l",  "lm", "ln", "ls", "n",  "nn",
                                           "ph", "r",  "s",  "sh", "ss", "th" };
  const std::array<std::string, 6> f4 = { "r", "n", "s", "th", "l", "m" };
  const std::array<std::string, 10> f5 = { "a", "e", "ë", "é", "ó",
                                           "",  "",  "",  "",  "" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_MAIAR_HPP
