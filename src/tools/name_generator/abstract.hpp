/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ABSTRACT_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ABSTRACT_HPP

/* Standard includes */
#include <random>

class AbstractNameGenerator
{
public:
  AbstractNameGenerator& operator=(AbstractNameGenerator&&) = delete;
  AbstractNameGenerator(AbstractNameGenerator&&) = delete;
  AbstractNameGenerator& operator=(const AbstractNameGenerator&) = delete;
  AbstractNameGenerator(const AbstractNameGenerator&) = delete;
  AbstractNameGenerator() = default;
  [[nodiscard]] virtual std::string GetName(const bool /*male*/,
                                            const bool /*female*/) = 0;
  [[nodiscard]] virtual bool HasGender() const { return true; };
  [[nodiscard]] virtual bool HasNeutral() const { return false; };
  virtual ~AbstractNameGenerator() = default;
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ABSTRACT_HPP
