/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DUNLENDING_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DUNLENDING_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class DunlendingNameGenerator : public AbstractNameGenerator
{
public:
  DunlendingNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /*female*/) override
  {
    if (male) {
      return this->GetMaleName();
    } else {
      return this->GetFemaleName();
    }
  }

private:
  const float kLowProbability = 0.25;
  const float kMediumProbability = 0.75;
  const int kMaleSkip = 4;
  const int kFemaleSkip = 10;

  std::string GetMaleName()
  {
    const double probability = GetProbability();
    const std::string str1 =
      *select_randomly(this->m1.cbegin(), this->m1.cend());
    const std::string str2 =
      *select_randomly(this->m2.cbegin(), this->m2.cend());
    const std::string* str5 =
      &*select_randomly(this->m5.cbegin(), this->m5.cend());

    std::string name;
    if (probability < kLowProbability) {
      while (str5 - &*this->m5.cbegin() < kMaleSkip) {
        str5 = &*select_randomly(this->m5.cbegin(), this->m5.cend());
      }
      name = str1 + str2 + *str5;
    } else {
      std::string str3 = *select_randomly(this->m3.cbegin(), this->m3.cend());
      const std::string str4;
      do {
        str3 = *select_randomly(this->m3.cbegin(), this->m3.cend());
      } while (str3 == str1 || str3 == *str5);

      if (probability < kMediumProbability) {
        name = str1 + str2 + str3 + str4 + *str5;
      } else {
        name = str1 + str2 + str3 + str4 + "doc";
      }
    }
    return ToTitleCase(name);
  }

  std::string GetFemaleName()
  {
    const double probability = GetProbability();
    const std::string str1 =
      *select_randomly(this->f1.cbegin(), this->f1.cend());
    const std::string str2 =
      *select_randomly(this->f2.cbegin(), this->f2.cend());
    const std::string* str5 =
      &*select_randomly(this->f5.cbegin(), this->f5.cend());

    std::string name;
    if (probability < kLowProbability) {
      while (str5 - &*this->f5.cbegin() < kFemaleSkip) {
        str5 = &*select_randomly(this->f5.cbegin(), this->f5.cend());
      }
      name = str1 + str2 + *str5;
    } else {
      std::string str3 = *select_randomly(this->f3.cbegin(), this->f3.cend());
      const std::string str4;
      do {
        str3 = *select_randomly(this->f3.cbegin(), this->f3.cend());
      } while (str3 == str1 || str3 == *str5);

      name = str1 + str2 + str3 + str4 + *str5;
    }
    return ToTitleCase(name);
  }

  const std::array<std::string, 12> m1 = { "br", "c", "d", "dr", "g", "gr",
                                           "h",  "m", "r", "s",  "v", "z" };
  const std::array<std::string, 3> m2 = { "a", "e", "o" };
  const std::array<std::string, 19> m3 = {
    "d", "dd", "dr", "g", "gr", "gd", "ld", "lm", "m",  "mn",
    "n", "nn", "nr", "r", "rm", "rn", "rr", "rl", "rs",
  };
  const std::array<std::string, 3> m4 = { "a", "i", "o" };
  const std::array<std::string, 11> m5 = { "c", "d",  "g",  "l",  "ld", "lt",
                                           "n", "nd", "nt", "rn", "th" };
  const std::array<std::string, 9> f1 = { "b", "c", "d", "h", "m",
                                          "n", "s", "v", "w" };
  const std::array<std::string, 4> f2 = { "a", "e", "i", "o" };
  const std::array<std::string, 17> f3 = {
    "c", "cr", "d", "dr", "g",  "l",   "m", "mn", "mr",
    "n", "nr", "r", "rh", "rl", "rth", "s", "t",
  };
  const std::array<std::string, 4> f4 = { "a", "e", "i", "o" };
  const std::array<std::string, 18> f5 = {
    "",  "",   "",   "",   "",   "",   "c",  "d",  "n",
    "s", "ld", "ft", "ss", "dh", "rf", "rd", "rn", "th",
  };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DUNLENDING_HPP
