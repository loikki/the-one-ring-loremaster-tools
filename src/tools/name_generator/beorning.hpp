/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BEORNING_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BEORNING_HPP

/* Own include */
#include "../../random.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class BeorningNameGenerator : public AbstractNameGenerator
{
public:
  BeorningNameGenerator() = default;
  std::string GetName(const bool male, const bool /* female */) override
  {
    std::string name;
    if (male) {
      name = *select_randomly(this->l1.cbegin(), this->l1.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
    } else {
      name = *select_randomly(this->l3.cbegin(), this->l3.cend());
      name += *select_randomly(this->l4.cbegin(), this->l4.cend());
    }
    return name;
  }

private:
  const std::array<std::string, 83> l1 = {
    "Ac",   "Ag",    "Ald",   "Aln",  "Aran", "Arn",  "Ba",   "Bar",  "Bald",
    "Bear", "Beorn", "Beran", "Borg", "Both", "Brer", "Dag",  "Darn", "Dreng",
    "Dug",  "Eld",   "Erad",  "Eran", "Ern",  "Fer",  "Forn", "Frid", "Froth",
    "Gal",  "Glum",  "Gluth", "Grim", "Har",  "Hart", "Heim", "Heor", "Hroth",
    "Ig",   "Ingel", "Is",    "Iw",   "Jal",  "Jar",  "Jarn", "Jorn", "Log",
    "Lor",  "Lyd",   "Lyth",  "Mag",  "Mar",  "Morn", "Moth", "Nard", "Ned",
    "Nef",  "Nor",   "Old",   "Ord",  "Ot",   "Oth",  "Rand", "Rath", "Raeg",
    "Ric",  "Rod",   "Sceot", "Sig",  "Skal", "Skol", "Stig", "Tar",  "Theod",
    "Thor", "Throt", "Treo",  "Val",  "Vald", "Vig",  "Vul",  "Wal",  "Wald",
    "Wid",  "Wul"
  };
  const std::array<std::string, 64> l2 = {
    "ac",   "ald",  "angar", "ard",  "aric",  "bald",  "bar",  "beorn",
    "bert", "bold", "brand", "dac",  "dar",   "dhor",  "dam",  "dan",
    "fald", "fara", "fast",  "forn", "gac",   "geir",  "gils", "grim",
    "hame", "har",  "helm",  "here", "hyrde", "kald",  "kar",  "karl",
    "kin",  "mar",  "mark",  "moth", "mund",  "ohd",   "ond",  "or",
    "oric", "rand", "rath",  "rek",  "ric",   "rot",   "sel",  "sorn",
    "stin", "styr", "tan",   "tar",  "taric", "thorn", "torn", "treo",
    "var",  "vat",  "vir",   "vith", "wald",  "war",   "wine", "wulf"
  };
  const std::array<std::string, 87> l3 = {
    "Ac",    "Aer",   "Amal", "Arin", "Ava",   "Bar",   "Bear", "Beorn", "Bera",
    "Beran", "Birn",  "Bog",  "Brer", "Bruni", "Din",   "Dis",  "Dom",   "Dyr",
    "Eir",   "Esil",  "Eth",  "Ey",   "Fast",  "Faye",  "Feor", "Fyn",   "Gail",
    "Gel",   "Gerth", "Gis",  "Grim", "Gud",   "Hall",  "Har",  "Hild",  "Hrim",
    "Huld",  "Hun",   "Ilin", "Ingi", "Ior",   "Is",    "Jen",  "Jern",  "Jil",
    "Jor",   "Kat",   "Kath", "Kay",  "Kyn",   "Leot",  "Lif",  "Lin",   "Lyn",
    "Maer",  "Mag",   "Mar",  "Mel",  "Nel",   "Nor",   "Nyr",  "Od",    "Ol",
    "Ovi",   "Ovin",  "Raeg", "Raen", "Ran",   "Rhon",  "Ril",  "Sal",   "Sig",
    "Sol",   "Svan",  "Treo", "Ul",   "Ulin",  "Ulvin", "Una",  "Vel",   "Ven",
    "Vil",   "Vyl",   "Waen", "Wen",  "Win",   "Wyl"
  };
  const std::array<std::string, 72> l4 = {
    "a",     "aen",   "aeya", "anda",  "ara",  "ava",   "aya",    "bi",
    "bina",  "bwyn",  "byn",  "da",    "dira", "dis",   "dora",   "eith",
    "elde",  "ena",   "era",  "eva",   "ewyn", "fast",  "firth",  "frida",
    "fyn",   "garth", "gifu", "ginny", "gun",  "helda", "hena",   "hera",
    "hild",  "hild",  "hild", "hild",  "hild", "hild",  "la",     "laug",
    "lin",   "loth",  "nida", "nis",   "nwyn", "ny",    "olin",   "ora",
    "otta",  "owyn",  "rin",  "risa",  "rlin", "run",   "thrith", "tina",
    "tira",  "tyn",   "vera", "vild",  "vor",  "vyn",   "wed",    "wild",
    "winne", "wyn",   "wyn",  "wyn",   "wyn",  "wyn",   "wyn",    "wyn",
  };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BEORNING_HPP
