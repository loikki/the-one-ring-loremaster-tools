/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ROHIRRIM_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ROHIRRIM_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class RohirrimNameGenerator : public AbstractNameGenerator
{
public:
  RohirrimNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /*female*/) override
  {
    if (male) {
      return this->GetMaleName();
    } else {
      return this->GetFemaleName();
    }
  }

private:
  const float kLowProbability = 0.5;
  std::string GetMaleName()
  {
    const double probability = GetProbability();
    if (probability < kLowProbability) {
      const std::string str1 =
        *select_randomly(this->m1.cbegin(), this->m1.cend());
      const std::string str2 =
        *select_randomly(this->m2.cbegin(), this->m2.cend());
      return ToTitleCase(str1 + str2);
    } else {
      const std::string str3 =
        *select_randomly(this->m3.cbegin(), this->m3.cend());
      return ToTitleCase(str3);
    }
  }

  std::string GetFemaleName()
  {
    const std::string str1 =
      *select_randomly(this->f1.cbegin(), this->f1.cend());
    const std::string str2 =
      *select_randomly(this->f2.cbegin(), this->f2.cend());
    return ToTitleCase(str1 + str2);
  }

  const std::array<std::string, 29> m1 = {
    "Éad",   "Al",   "Bal",  "Déor", "Dún",  "Dern", "Eó",  "Elf",
    "Erken", "Fast", "Fen",  "Fol",  "Fréa", "Frum", "Ful", "Gár",
    "Gam",   "Gléo", "Gold", "Grim", "Guth", "Há",   "Héo", "Here",
    "Heru",  "Hold", "Léo",  "Théo", "Wíd",
  };
  const std::array<std::string, 37> m2 = {
    "bald", "beam",  "blod", "brand", "ca",    "canstan", "cred", "da",
    "dan",  "dig",   "dor",  "dred",  "ere",   "fa",      "fara", "fred",
    "gar",  "gel",   "grim", "helm",  "heort", "here",    "láf",  "leth",
    "ling", "mód",   "man",  "mer",   "mod",   "mund",    "nere", "or",
    "red",  "thain", "tor",  "ulf",   "wine",
  };
  const std::array<std::string, 89> m3 = {
    "Éadig",    "Éadmód",   "Éoblod",    "Éogar",    "Éoheort",    "Éohere",
    "Éomód",    "Éoman",    "Éomer",     "Éomund",   "Éorcanstan", "Éored",
    "Éorl",     "Éothain",  "Éowine",    "Aldor",    "Baldor",     "Baldred",
    "Bregdan",  "Brego",    "Brytta",    "Ceorl",    "Déor",       "Déorbrand",
    "Déorgar",  "Déorhelm", "Déorthain", "Déorwine", "Dúnhere",    "Dernfara",
    "Derngar",  "Dernhelm", "Dernwine",  "Elfhelm",  "Elfwine",    "Erkenbrand",
    "Fasthelm", "Fastred",  "Fengel",    "Folca",    "Folcred",    "Folcwine",
    "Fréa",     "Fréaláf",  "Fréawine",  "Fram",     "Freca",      "Frumgar",
    "Fulgar",   "Fulgrim",  "Fulor",     "Fulthain", "Gálmód",     "Gárbald",
    "Gárulf",   "Gárwine",  "Gamling",   "Gléobeam", "Gléomer",    "Gléothain",
    "Gléowine", "Goldwine", "Gríma",     "Gram",     "Grimbold",   "Guthbrand",
    "Guthláf",  "Guthmer",  "Guthred",   "Háma",     "Héostor",    "Haleth",
    "Helm",     "Herefara", "Herubrand", "Herumer",  "Heruthain",  "Heruwine",
    "Holdred",  "Holdwine", "Léod",      "Léofa",    "Léofara",    "Léofred",
    "Léofwine", "Léonere",  "Wídfara",   "Walda",    "Wulf",
  };
  const std::array<std::string, 41> f1 = {
    "Éad",  "Éor",   "Ald",  "Bald", "Bey",  "Ceol", "Cyne", "Déor", "Dún",
    "Dere", "Dern",  "Eó",   "Ea",   "Ead",  "Eal",  "Elf",  "Folc", "Frum",
    "Gam",  "Gar",   "Gléo", "God",  "Gold", "Guth", "Há",   "Héo",  "Here",
    "Heru", "Hold",  "Léo",  "Léof", "Leof", "Maer", "Maet", "Sae",  "Somer",
    "Théo", "Théod", "Tid",  "Wíd",  "Waer",
  };
  const std::array<std::string, 26> f2 = {
    "burh", "dis", "doina", "gyth", "hild", "ith",  "lid",  "lida", "lith",
    "nild", "rid", "rith",  "run",  "ryth", "wara", "well", "wen",  "wena",
    "wine", "wyn", "wyn",   "wyn",  "wyn",  "hild", "hild", "hild",
  };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_ROHIRRIM_HPP
