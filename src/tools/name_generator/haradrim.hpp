/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HARADRIM_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HARADRIM_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class HaradrimNameGenerator : public AbstractNameGenerator
{
public:
  HaradrimNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool male,
                                    const bool /* female */) override
  {
    if (male) {
      return this->GetMaleName();
    } else {
      return this->GetFemaleName();
    }
  }

private:
  const float kLowProbability = 0.25;
  const float kMediumProbability = 0.5;

  [[nodiscard]] std::string GetMaleName()
  {
    const double probability = GetProbability();
    const std::string str1 =
      *select_randomly(this->m1.cbegin(), this->m1.cend());
    const std::string str2 =
      *select_randomly(this->m2.cbegin(), this->m2.cend());
    const std::string str4 =
      *select_randomly(this->m4.cbegin(), this->m4.cend());
    const std::string str5 =
      *select_randomly(this->m5.cbegin(), this->m5.cend());
    const std::string str6 =
      *select_randomly(this->m6.cbegin(), this->m6.cend());
    std::string str3;
    do {
      str3 = *select_randomly(this->m3.cbegin(), this->m3.cend());
    } while (str3 == str1 || str3 == str6);

    std::string name;
    if (probability < kLowProbability) {
      name = str1 + str2 + str3 + str4 + str6;
    } else {
      if (probability < kMediumProbability) {
        name = "Al" + str1 + str2 + str3 + str4 + str6;
      } else {
        const std::string str2_2 =
          *select_randomly(this->m2.cbegin(), this->m2.cend());
        std::string str5_2;
        do {
          str5_2 = *select_randomly(this->m5.cbegin(), this->m5.cend());
        } while (str5_2 == str6 || str5_2 == str3);
        name = str1 + str2 + str3 + str2_2 + str5_2 + str4 + str6;
      }
    }

    return ToTitleCase(name);
  }

  [[nodiscard]] std::string GetFemaleName()
  {
    const double probability = GetProbability();
    const std::string str1 =
      *select_randomly(this->f1.cbegin(), this->f1.cend());
    const std::string str2 =
      *select_randomly(this->f2.cbegin(), this->f2.cend());
    const std::string str4 =
      *select_randomly(this->f4.cbegin(), this->f4.cend());
    const std::string str5 =
      *select_randomly(this->f5.cbegin(), this->f5.cend());
    const std::string str6 =
      *select_randomly(this->f6.cbegin(), this->f6.cend());
    std::string str3;
    do {
      str3 = *select_randomly(this->f3.cbegin(), this->f3.cend());
    } while (str3 == str1 || str3 == str6);

    std::string name;
    if (probability < kMediumProbability) {
      name = str1 + str2 + str3 + str4 + str6;
    } else {
      const std::string str2_2 =
        *select_randomly(this->f2.cbegin(), this->f2.cend());
      std::string str5_2;
      do {
        str5_2 = *select_randomly(this->f5.cbegin(), this->f5.cend());
      } while (str5_2 == str6 || str5_2 == str3);
      name = str1 + str2 + str3 + str2_2 + str5_2 + str4 + str6;
    }

    return ToTitleCase(name);
  }

  const std::array<std::string, 16> m1 = {
    "",  "",  "b", "d", "gh", "h", "j", "kh",
    "l", "m", "n", "q", "s",  "t", "w", "y",
  };
  const std::array<std::string, 21> m2 = {
    "aa", "a", "a", "i", "u", "u", "a", "a", "i", "u", "u",
    "a",  "a", "i", "u", "u", "a", "a", "i", "u", "u",
  };
  const std::array<std::string, 23> m3 = {
    "b", "br", "dh", "dn", "f", "kr", "ld", "'m", "mr", "'n", "n", "nn",
    "q", "qq", "r",  "rw", "s", "s'", "sh", "tb", "th", "wf", "z",
  };
  const std::array<std::string, 20> m4 = {
    "aa", "oo", "ay", "a", "i", "o", "a", "i", "o", "a",
    "i",  "o",  "a",  "i", "o", "a", "a", "a", "o", "o",
  };
  const std::array<std::string, 7> m5 = { "d", "f", "m", "n", "nn", "r", "z" };
  const std::array<std::string, 11> m6 = { "",  "b", "d", "h", "kr", "l",
                                           "m", "n", "r", "s", "th" };
  const std::array<std::string, 13> f1 = { "",  "d", "f", "gh", "h", "kh", "m",
                                           "n", "r", "s", "t",  "w", "y" };
  const std::array<std::string, 3> f2 = { "a", "i", "u" };
  const std::array<std::string, 25> f3 = {
    "d", "dh", "f",  "h",  "l", "ll", "lr", "ld", "m",  "ml", "n", "nn", "nh",
    "r", "rr", "rl", "rh", "s", "sh", "sr", "w",  "wd", "wl", "z", "zh",
  };
  const std::array<std::string, 13> f4 = { "ay", "y", "aa", "a", "a", "a", "a",
                                           "a",  "i", "i",  "i", "i", "i" };
  const std::array<std::string, 9> f5 = { "b", "dh", "h",  "l", "m",
                                          "n", "r",  "th", "y" };
  const std::array<std::string, 9> f6 = { "",  "d", "h",  "h", "h",
                                          "l", "n", "nd", "r" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HARADRIM_HPP
