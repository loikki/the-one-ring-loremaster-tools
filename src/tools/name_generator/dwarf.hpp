/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DWARF_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DWARF_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

/* Standard library */
#include <array>

class DwarfNameGenerator : public AbstractNameGenerator
{
public:
  DwarfNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool /*male*/,
                                    const bool /*female*/) override
  {
    const double probability = GetProbability();
    std::string name = *select_randomly(this->l1.cbegin(), this->l1.cend());

    if (probability < kLowProbability) {
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l3.cbegin(), this->l3.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l4.cbegin(), this->l4.cend());
    } else {
      name += *select_randomly(this->l5.cbegin(), this->l5.cend());
      name += *select_randomly(this->l6.cbegin(), this->l6.cend());
      name += *select_randomly(this->l7.cbegin(), this->l7.cend());
    }
    return ToTitleCase(name);
  }
  [[nodiscard]] bool HasGender() const override { return false; };

private:
  const float kLowProbability = 0.5;
  const std::array<std::string, 21> l1 = {
    "b", "br", "d",  "dr", "dw", "f",  "fl", "fr", "g",  "gl",  "gr",
    "k", "kh", "kr", "l",  "m",  "mh", "n",  "t",  "th", "thr",
  };
  const std::array<std::string, 5> l2 = { "a", "e", "i", "o", "u" };
  const std::array<std::string, 20> l3 = {
    "b",  "f", "fr", "l",  "lb", "lr", "lv", "m",  "mb", "ml",
    "mr", "n", "nd", "nr", "r",  "rb", "rl", "rv", "s",  "sr",
  };
  const std::array<std::string, 4> l4 = { "k", "m", "n", "r" };
  const std::array<std::string, 7> l5 = { "a", "ai", "e", "i", "o", "oi", "u" };
  const std::array<std::string, 9> l6 = { "b", "d", "f", "g", "k",
                                          "l", "m", "n", "t" };
  const std::array<std::string, 9> l7 = { "a", "e", "i", "o", "u",
                                          "",  "",  "",  "" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_DWARF_HPP
