/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BALROG_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BALROG_HPP

/* Own include */
#include "../../random.hpp"
#include "../../utils.hpp"
#include "abstract.hpp"

class BalrogNameGenerator : public AbstractNameGenerator
{
public:
  BalrogNameGenerator() = default;
  [[nodiscard]] std::string GetName(const bool /*male*/,
                                    const bool /*female*/) override
  {
    std::string name;

    const double probability = GetProbability();
    if (probability < kLowProbability) {
      name = *select_randomly(this->l1.cbegin(), this->l1.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l3.cbegin(), this->l3.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l5.cbegin(), this->l5.cend());
    } else if (probability < kMediumProbability) {
      name = *select_randomly(this->l1.cbegin(), this->l1.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l3.cbegin(), this->l3.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l4.cbegin(), this->l4.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l5.cbegin(), this->l5.cend());
    } else {
      name = *select_randomly(this->l1.cbegin(), this->l1.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l4.cbegin(), this->l4.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l3.cbegin(), this->l3.cend());
      name += *select_randomly(this->l2.cbegin(), this->l2.cend());
      name += *select_randomly(this->l5.cbegin(), this->l5.cend());
    }
    return ToTitleCase(name);
  }
  [[nodiscard]] bool HasGender() const override { return false; };

private:
  const float kLowProbability = 0.6;
  const float kMediumProbability = 0.8;
  const std::array<std::string, 11> l1 = { "b",  "bh", "d", "dh", "g", "h",
                                           "kh", "n",  "r", "v",  "z" };
  const std::array<std::string, 11> l2 = { "a", "o", "u", "a", "o", "u",
                                           "a", "o", "u", "e", "i" };
  const std::array<std::string, 32> l3 = {
    "br", "bn", "dh", "dr",  "dhr", "gn", "gr", "gd", "gz", "kn", "kg",
    "kv", "kz", "ld", "lg",  "lm",  "ln", "lv", "lz", "mz", "mg", "md",
    "nz", "ng", "nd", "thr", "thm", "tr", "zc", "zg", "zk", "zz"
  };
  const std::array<std::string, 9> l4 = { "b", "d", "k", "l", "m",
                                          "n", "t", "v", "z" };
  const std::array<std::string, 7> l5 = { "c", "d", "g", "k", "l", "n", "r" };
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_BALROG_HPP
