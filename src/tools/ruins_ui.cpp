/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "ruins_ui.hpp"
#include "../random.hpp"

/* Other libraries */
#include <QString>

void
RuinsUi::Roll()
{
  /* Builders */
  const std::string orig_builders = *select_randomly(
    this->original_builders.cbegin(), this->original_builders.cend());
  this->builders->setText(QString::fromStdString(orig_builders));

  /* Purpose */
  const std::string orig_purpose = *select_randomly(
    this->original_purpose.cbegin(), this->original_purpose.cend());
  this->purpose->setText(QString::fromStdString(orig_purpose));

  /* Appearance */
  const std::string cur_appearance = *select_randomly(
    this->current_appearance.cbegin(), this->current_appearance.cend());
  this->appearance->setText(QString::fromStdString(cur_appearance));

  /* Usage */
  const std::string cur_usage =
    *select_randomly_weighted(this->current_usage.cbegin(),
                              this->current_usage.cend(),
                              this->current_usage_weights.cbegin(),
                              this->current_usage_weights.cend());
  this->usage->setText(QString::fromStdString(cur_usage));
}

void
RuinsUi::ConnectSignalSlots()
{
  /* Buttons */
  QObject::connect(
    this->new_button, &QPushButton::clicked, [=]() { this->Roll(); });
  QObject::connect(
    this->done_button, &QPushButton::clicked, [=]() { this->accept(); });
}
