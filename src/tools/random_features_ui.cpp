/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "random_features_ui.hpp"
#include "../random.hpp"
#include "../utils.hpp"

RandomFeaturesUi::RandomFeaturesUi()
  : Ui_RandomFeatures()
{
  this->setupUi(this);

  /* Read the data */
  QJsonObject data_read = ReadJsonObject("data/features.json");
  this->positives = data_read["positive"].toArray();
  this->negatives = data_read["negative"].toArray();

  /* Set the combo box */
  this->features_type_combo->addItem(positive_features);
  this->features_type_combo->addItem(negative_features);
  this->features_type_combo->addItem(both_features);

  /* Connect the signals */
  this->ConnectSignalSlots();

  /* Create a first set of features */
  this->NewFeatures();
};

void
RandomFeaturesUi::NewFeatures()
{
  QString text;
  QString type = this->features_type_combo->currentText();
  for (int i = 0; i < kNumberFeatures; i++) {
    /* Find which type of features to use */
    double prob = GetProbability();
    bool use_pos = type == positive_features;
    use_pos =
      use_pos || (type == both_features && prob < kProbabilityPositiveFeatures);

    /* Get a random feature */
    QJsonValue val;
    if (use_pos) {
      val = *select_randomly(this->positives.cbegin(), this->positives.cend());
    } else {
      val = *select_randomly(this->negatives.cbegin(), this->negatives.cend());
    }

    text += val.toString() + "\n";
  }

  this->features_label->setText(text);
};

void
RandomFeaturesUi::ConnectSignalSlots()
{
  /* Button */
  QObject::connect(this->new_features_button, &QPushButton::clicked, [=]() {
    this->NewFeatures();
  });
  QObject::connect(
    this->done_button, &QPushButton::clicked, [=]() { this->accept(); });

  /* Combobox */
  QObject::connect(this->features_type_combo,
                   &QComboBox::currentIndexChanged,
                   [=]() { this->NewFeatures(); });
}
