/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HPP
#define LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HPP

/* Own includes */
#include "../gui/ui_name_generator.h"
#include "name_generator/abstract.hpp"
#include "name_generator/balrog.hpp"
#include "name_generator/beorning.hpp"
#include "name_generator/bree.hpp"
#include "name_generator/dale.hpp"
#include "name_generator/druedain.hpp"
#include "name_generator/dunlending.hpp"
#include "name_generator/dwarf.hpp"
#include "name_generator/easterling.hpp"
#include "name_generator/haradrim.hpp"
#include "name_generator/hobbit.hpp"
#include "name_generator/maiar.hpp"
#include "name_generator/orc.hpp"
#include "name_generator/quenya.hpp"
#include "name_generator/rohirrim.hpp"
#include "name_generator/sindarin.hpp"

/* Other includes */
#include <QDialog>

/* Forward declaration */
class MainWindow;

using tuple_gen = std::tuple<QString, AbstractNameGenerator*>;
using list_gen = std::vector<tuple_gen>;

class NameGeneratorUi
  : public QDialog
  , public Ui_name_generator
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  NameGeneratorUi& operator=(NameGeneratorUi&&) = delete;
  NameGeneratorUi(NameGeneratorUi&&) = delete;
  NameGeneratorUi& operator=(const NameGeneratorUi&) = delete;
  NameGeneratorUi(const NameGeneratorUi&) = delete;

  NameGeneratorUi();
  ~NameGeneratorUi() override;

protected:
  const int kNumberNames = 10;

  void ConnectSignalSlots();
  void NewNames() const;
  void ChangeRace() const;
  list_gen generators;
};

#endif // LOREMASTER_TOOLS_SRC_NAME_GENERATOR_HPP
