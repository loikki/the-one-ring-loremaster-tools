/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "name_generator_ui.hpp"

NameGeneratorUi::NameGeneratorUi()
  : Ui_name_generator()
{
  this->setupUi(this);

  this->generators.push_back(
    tuple_gen(tr("Balrog"), new BalrogNameGenerator()));
  this->generators.push_back(tuple_gen(tr("Bree"), new BreeNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Beorning / Woodmen"), new BeorningNameGenerator()));
  this->generators.push_back(tuple_gen(tr("Dale"), new DaleNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Druedain"), new DruedainNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Dunlending"), new DunlendingNameGenerator()));
  this->generators.push_back(tuple_gen(tr("Dwarf"), new DwarfNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Easterling"), new EasterlingNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Haradrim"), new HaradrimNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Hobbit"), new HobbitNameGenerator()));
  this->generators.push_back(tuple_gen(tr("Maiar"), new MaiarNameGenerator()));
  this->generators.push_back(tuple_gen(tr("Orc"), new OrcNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Quenya"), new QuenyaNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Rohirrim"), new RohirrimNameGenerator()));
  this->generators.push_back(
    tuple_gen(tr("Sindarin"), new SindarinNameGenerator()));

  /* Write the labels in the combo box */
  for (tuple_gen gen : this->generators) {
    this->race->addItem(std::get<0>(gen));
  }

  this->ConnectSignalSlots();
  this->ChangeRace();
};

NameGeneratorUi::~NameGeneratorUi()
{
  for (tuple_gen gen : this->generators) {
    // NOLINTNEXTLINE
    delete std::get<1>(gen);
  }
}

void
NameGeneratorUi::ConnectSignalSlots()
{
  /* Buttons */
  QObject::connect(
    this->new_names_button, &QPushButton::clicked, [=]() { this->NewNames(); });
  QObject::connect(
    this->done_button, &QPushButton::clicked, [=]() { this->accept(); });

  /* Index changed */
  QObject::connect(
    this->race, &QComboBox::currentIndexChanged, [=]() { this->ChangeRace(); });
}

void
NameGeneratorUi::NewNames() const
{
  int index = this->race->currentIndex();
  std::string text_names;
  const bool is_male = this->male->isChecked();
  const bool is_female = this->female->isChecked();

  for (int ind = 0; ind < this->kNumberNames; ind++) {
    text_names +=
      std::get<1>(this->generators[index])->GetName(is_male, is_female) + "\n";
  }
  this->names->setText(QString::fromStdString(text_names));
}

void
NameGeneratorUi::ChangeRace() const
{
  const int index = this->race->currentIndex();
  const AbstractNameGenerator* gen = std::get<1>(this->generators[index]);

  const bool has_neutral = gen->HasNeutral();
  const bool has_gender = gen->HasGender();

  /* Set auto exclusive */
  this->male->setAutoExclusive(not has_neutral);
  this->female->setAutoExclusive(not has_neutral);

  /* Disable button without gender */
  this->male->setCheckable(has_gender);
  this->female->setCheckable(has_gender);

  /* Ensure that the check box are in a correct state */
  if (has_gender) {
    this->male->setChecked(false);
    this->female->setChecked(true);
  } else {
    this->male->setChecked(false);
    this->female->setChecked(false);
  }

  /* Update the ui */
  this->male->update();
  this->female->update();

  /* Get some new names */
  this->NewNames();
}
