/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "rolls_probabilities_ui.hpp"
#include "../random.hpp"
#include "../utils.hpp"

/* Other libraries */
#include <QLineSeries>
#include <QValueAxis>

RollsProbabilitiesUi::RollsProbabilitiesUi()
  : Ui_RollsDialog()
{
  this->setupUi(this);
  this->output->setChart(chart);
  this->output->setRubberBand(QChartView::RectangleRubberBand);
  this->chart->legend()->hide();

  /* Set the graph */
  this->output->setRenderHint(QPainter::Antialiasing);

  /* Add the type of rolls */
  for (int i = 0; i < kRollCount; i++) {
    // NOLINTNEXTLINE
    this->roll_type->addItem(kRollString[i]);
  }

  /* Connect the signals */
  this->ConnectSignalSlots();

  /* Draw a first graph */
  this->CreateGraph();
};

void
RollsProbabilitiesUi::ResetAxes()
{
  this->chart->axes(Qt::Horizontal)[0]->setRange(kTargetNumberMin,
                                                 kTargetNumberMax);
  this->chart->axes(Qt::Vertical)[0]->setRange(0., 100.);
}

void
RollsProbabilitiesUi::CreateGraph()
{
  /* Cleanup previous graph */
  this->chart->removeAllSeries();
  if (not this->chart->axes().isEmpty()) {
    this->chart->removeAxis(this->chart->axes().back());
    this->chart->removeAxis(this->chart->axes().back());
  }

  /* Get the stats */
  int current_number_rolls = this->number_rolls->value();
  int number_dice = this->skill_spin->value();
  auto type = static_cast<RollType>(this->roll_type->currentIndex());
  bool weary = this->is_weary->isChecked();
  bool miserable = this->is_miserable->isChecked();
  RollStatistics stats = RollsProbabilitiesUi::ComputeProbabilities(
    current_number_rolls, type, number_dice, weary, miserable);

  /* Compute the lines */
  auto* serie = new QLineSeries();
  for (const RollPoint& point : stats.success) {
    serie->append(point.x, 100. * point.y);
  }

  /* Update the chart */
  auto* vertical = new QValueAxis();
  vertical->setRange(0., 100.);
  auto* horizontal = new QValueAxis();
  horizontal->setRange(kTargetNumberMin, kTargetNumberMax);

  /* Add the axis */
  this->chart->addAxis(horizontal, Qt::AlignBottom);
  this->chart->addAxis(vertical, Qt::AlignLeft);
  this->chart->addSeries(serie);
  serie->attachAxis(horizontal);
  serie->attachAxis(vertical);

  /* Add labels */
  horizontal->setTitleText(tr("Target Number"));
  vertical->setTitleText(tr("Probability"));

  /* Set the chart */
  QString text =
    tr("Average value: ") + QString::number(stats.average_value, 'f', 2) + "\n";
  text += tr("Average number of 6: ") +
          QString::number(stats.average_tengwar, 'f', 2) + "\n";
  this->average->setText(text);
}

RollStatistics
RollsProbabilitiesUi::ComputeProbabilities(const int number_rolls,
                                           const RollType roll_type,
                                           const int number_dice,
                                           const bool is_weary,
                                           const bool is_miserable)
{
  int gandalf = 0;
  int sauron = 0;
  std::vector<int> number_rolls_at_value(
    kFeatDifferentValues + number_dice * kSkillDiceFaces + 1, 0);
#pragma omp parallel for
  for (int i = 0; i < number_rolls; i++) {
    int feat = RollFeatDice(roll_type);

    if (feat == kGandalf) {
      gandalf++;
      continue;
    } else if (is_miserable && feat == kSauron) {
      sauron++;
      continue;
    }

    /* Loop over all the available dices */
    int current = feat == kSauron ? 0 : feat;
    for (int dice = 0; dice < number_dice; dice++) {
      int roll = RollSkillDice(is_weary);
      current += roll;
    }

    /* Store the result */
    number_rolls_at_value[current]++;
  }

  /* Add gandalf's rune */
  number_rolls_at_value.back() += gandalf;

  /* Format the output */
  RollStatistics output;
  output.success = std::vector<RollPoint>(
    kTargetNumberMax - kTargetNumberMin + 1, { .x = -1, .y = -1 });

  /* Accumulate results */
  int total_score = 0;
  int number_success = 0;
  auto lowest_non_null_element = static_cast<int>(number_rolls_at_value.size());
  for (int i = static_cast<int>(number_rolls_at_value.size()) - 1; i >= 0;
       i--) {
    total_score += number_rolls_at_value[i] * i;
    number_success += number_rolls_at_value[i];
    if (i >= kTargetNumberMin && i <= kTargetNumberMax) {
      output.success[i - kTargetNumberMin] = {
        .x = i,
        .y =
          static_cast<float>(number_success) / static_cast<float>(number_rolls)
      };
    }
    if (number_rolls_at_value[i] != 0) {
      lowest_non_null_element = i;
    }
  }

  /* Compute scalars */
  output.average_value = static_cast<float>(total_score) /
                         static_cast<float>(number_rolls - sauron - gandalf);
  output.average_tengwar =
    static_cast<float>(number_dice) / static_cast<float>(kSkillDiceFaces);

  /* Ensure to have an element within the required target numbers */
  for (int i = kTargetNumberMin; i <= kTargetNumberMax; i++) {
    const int ind = i - kTargetNumberMin;
    if (output.success[ind].x == -1) {
      if (i < lowest_non_null_element) {
        output.success[ind] = { .x = i, .y = 1. };
      } else {
        float prob =
          static_cast<float>(gandalf) / static_cast<float>(number_rolls);
        output.success[ind] = { .x = i, .y = prob };
      }
    }
  }
  return output;
}

void
RollsProbabilitiesUi::ConnectSignalSlots()
{
  /* Button */
  QObject::connect(
    this->reset_axes, &QPushButton::clicked, [=]() { this->ResetAxes(); });
  QObject::connect(
    this->done_button, &QPushButton::clicked, [=]() { this->accept(); });

  /* Check box */
  QObject::connect(
    this->is_weary, &QCheckBox::stateChanged, [=]() { this->CreateGraph(); });
  QObject::connect(this->is_miserable, &QCheckBox::stateChanged, [=]() {
    this->CreateGraph();
  });

  /* SpinBox */
  QObject::connect(
    this->skill_spin, &QSpinBox::valueChanged, [=]() { this->CreateGraph(); });

  /* ComboBox */
  QObject::connect(this->roll_type, &QComboBox::currentIndexChanged, [=]() {
    this->CreateGraph();
  });
}
