/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_SERVER_HPP
#define LOREMASTER_TOOLS_SRC_SERVER_HPP

/* Local includes */
#include "../fellowship.hpp"
#include "../server_logs.hpp"
#include "api/version.hpp"
#include "server_component.hpp"

/* Other includes */
#include <oatpp-swagger/Controller.hpp>
#include <oatpp/network/Server.hpp>
#include <oatpp/network/tcp/server/ConnectionProvider.hpp>

/* Forward declaration */
class FellowshipController;

class TheOneRingServer : public QObject
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  explicit TheOneRingServer(std::shared_ptr<Fellowship> new_fellowship,
                            std::string new_cors)
    : fellowship(std::move(new_fellowship))
    , cors(std::move(new_cors))
  {
    /* Init oatpp Environment */
    oatpp::base::Environment::init();
  }
  TheOneRingServer& operator=(TheOneRingServer&&) = delete;
  TheOneRingServer(TheOneRingServer&&) = delete;
  TheOneRingServer(const TheOneRingServer&) = delete;
  TheOneRingServer& operator=(const TheOneRingServer&) = delete;

  ~TheOneRingServer() override
  {
    if (this->IsRunning()) {
      this->Stop();
    }
    /* Destroy oatpp Environment */
    oatpp::base::Environment::destroy();
  }

  void UpdateCors(std::string new_cors)
  {
    this->cors = std::move(new_cors);
    /* Update the cors value of fellowship_controller does not work */
  }
  void Start();
  void Stop();
  void SendUpdateUi() { emit this->UpdateUi(); }

  [[nodiscard]] bool IsRunning() const { return this->server != nullptr; }

protected:
  static void Run(oatpp::network::Server* server) { server->run(); }
  std::unique_ptr<oatpp::network::Server> server{ nullptr };
  std::thread server_thread;
  std::shared_ptr<Fellowship> fellowship;
  std::shared_ptr<FellowshipController> fellowship_controller{ nullptr };
  std::string cors;

signals:
  void UpdateUi();
};

#endif // LOREMASTER_TOOLS_SRC_SERVER_HPP
