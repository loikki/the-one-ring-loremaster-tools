/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Local includes */
#include "server.hpp"
#include "api/fellowship_api.hpp"

void
TheOneRingServer::Start()
{
  ServerComponent components;

  /* Endpoints */
  this->fellowship_controller.reset(new FellowshipController());
  fellowship_controller->Init(this, this->fellowship, this->cors);
  auto version_controller = std::make_shared<VersionController>();
  version_controller->Init(this->cors);

  /* Get router component */
  // NOLINTNEXTLINE
  OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router);
  oatpp::web::server::api::Endpoints endpoints;
  endpoints.append(
    router->addController(fellowship_controller)->getEndpoints());
  endpoints.append(router->addController(version_controller)->getEndpoints());

#ifndef PROD
  /* Swagger */
  auto swagger_controller = oatpp::swagger::Controller::createShared(endpoints);
  router->addController(swagger_controller);
#endif

  /* Get connection handler component */
  // NOLINTNEXTLINE
  OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,
                  connectionHandler);

  /* Get connection provider component */
  // NOLINTNEXTLINE
  OATPP_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>,
                  connectionProvider);

  /* Create server which takes provided TCP connections and passes them to
   * HTTP connection handler */
  this->server.reset(
    new oatpp::network::Server(connectionProvider, connectionHandler));

  /* Priny info about server port */
  const char* port =
    static_cast<const char*>(connectionProvider->getProperty("port").getData());
  LogInfo("Server running on port " + std::string(port));

#ifndef PROD
  LogInfo("http://localhost:" + std::string(port) +
          "/swagger/ui for details on the API");
#endif
  LogInfo("The frontend is available at the following address " FRONTEND_URL);

  /* Run server */
  this->server_thread = std::thread(TheOneRingServer::Run, this->server.get());
}

void
TheOneRingServer::Stop()
{
  this->server->stop();
  this->server_thread.join();
  this->server.reset(nullptr);
  LogInfo("Server has been stopped");
}
