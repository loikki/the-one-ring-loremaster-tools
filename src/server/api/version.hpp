/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_API_VERSION_HPP
#define LOREMASTER_TOOLS_SRC_API_VERSION_HPP

/* Other includes */
#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/macro/component.hpp>
#include <oatpp/web/server/api/ApiController.hpp>

#include OATPP_CODEGEN_BEGIN(ApiController) ///< Begin Codegen

class VersionController : public oatpp::web::server::api::ApiController
{
public:
  void Init(std::string new_cors) {
    this->cors = new_cors;
  }

  /**
   * Constructor with object mapper.
   * @param objectMapper - default object mapper used to serialize/deserialize
   * DTOs.
   */
  // NOLINTNEXTLINE
  explicit VersionController(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>,
                                            objectMapper))
    : oatpp::web::server::api::ApiController(objectMapper)
  {
  }

  /**
   * @brief Version endpoint
   */
  ADD_CORS(version,
           this->cors
           );
  ENDPOINT_INFO(version)
  {
    info->summary = "Get the server version";
    info->addResponse<String>(Status::CODE_200, "text/plain");
  }
  ENDPOINT("GET", "/version", version)
  {
    std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
      ResponseFactory::createResponse(Status::CODE_200, THE_ONE_RING_VERSION);
    response->putHeader("content-type", "text/plain");
    return response;
  }
protected:
  std::string cors;
};

#include OATPP_CODEGEN_END(ApiController) ///< End Codegen

#endif // LOREMASTER_TOOLS_SRC_API_VERSION_HPP
