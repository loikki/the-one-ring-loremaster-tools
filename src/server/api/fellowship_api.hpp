/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_API_FELLOWSHIP_API_HPP
#define LOREMASTER_TOOLS_SRC_API_FELLOWSHIP_API_HPP

/* Local includes */
#include "../../fellowship.hpp"
#include "../../server_logs.hpp"
#include "data_transfer_object.hpp"
#include "../server.hpp"

/* Other includes */
#include <QJsonObject>
#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/macro/component.hpp>
#include <oatpp/web/server/api/ApiController.hpp>
#include <QJsonDocument>

#include OATPP_CODEGEN_BEGIN(ApiController) ///< Begin Codegen

class FellowshipController : public oatpp::web::server::api::ApiController
{
public:
  void Init(TheOneRingServer* new_server, std::shared_ptr<Fellowship> new_fellowship, std::string new_cors) {
    this->server = new_server;
    this->fellowship = new_fellowship;
    this->cors = new_cors;
  }

  /**
   * Constructor with object mapper.
   * @param objectMapper - default object mapper used to serialize/deserialize
   * DTOs.
   */
  // NOLINTNEXTLINE
  explicit FellowshipController(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>,
                                            objectMapper))
    : oatpp::web::server::api::ApiController(objectMapper)
  {
  }

  /**
   * @brief Fellowship Info
   */
  ADD_CORS(fellowship_info,
           this->cors
           );
  ENDPOINT_INFO(fellowship_info)
  {
    info->summary = "Provide all the information related to the fellowship";
    info->addResponse<String>(Status::CODE_200, "application/json");
  }
  ENDPOINT("GET",
           "/fellowship",
           fellowship_info)
  {
    LogDebug("Requesting fellowship info");

    /* Convert json to string */
    QJsonObject fellowship_json = this->fellowship->GetJson();
    QJsonDocument doc(fellowship_json);
    std::string return_value = doc.toJson(QJsonDocument::Compact).toStdString();

    /* Create response */
    std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
      ResponseFactory::createResponse(Status::CODE_200, return_value);
    response->putHeader("content-type", "application / json");
    return response;
  }

  /**
   * @brief Player Info
   */
  ADD_CORS(player_info,
           this->cors
           );
  ENDPOINT_INFO(player_info)
  {
    info->summary = "Provide all the information related to a player";
    info->addResponse<String>(Status::CODE_200, "application/json");
    info->addResponse<String>(Status::CODE_400, "text/plain");
    info->pathParams["player_name"].description =
      "Name of the player (not the character)";
  }
  ENDPOINT("GET",
           "/player/{player_name}",
           player_info,
           PATH(String, player_name))
  {
    /* Get the player name */
    const std::string& name = *player_name;
    LogDebug("Requesting player info for " + name);

    /* Get the player */
    Player* player = this->fellowship->GetPlayer(name);
    if (player == nullptr) {
      LogInfo("Player '" + name + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find " + name);
      response->putHeader("content-type", "text/plain");
      return response;
    }

    /* Convert it to a string */
    QJsonObject player_json = player->GetJson();
    QJsonDocument doc(player_json);
    std::string return_value = doc.toJson(QJsonDocument::Compact).toStdString();

    std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
      ResponseFactory::createResponse(Status::CODE_200, return_value);
    response->putHeader("content-type", "application / json");
    return response;
  }

  /**
   * @brief Endurance lost
   */
  ADD_CORS(player_endurance_lost,
           this->cors
           );
  ENDPOINT_INFO(player_endurance_lost)
  {
    info->summary = "Remove endurance to a player";
    info->addConsumes<Object<EnduranceLostDto>>("application/json");
    info->addResponse<String>(Status::CODE_200, "text/plain");
    info->addResponse<String>(Status::CODE_400, "text/plain");
    info->pathParams["player_name"].description =
      "Name of the player (not the character)";
  }
  ENDPOINT("POST",
           "/player/{player_name}/endurance-lost",
           player_endurance_lost,
           PATH(String, player_name),
           BODY_DTO(Object<EnduranceLostDto>, body)
           )
  {
    /* Get data */
    const std::string& name = *player_name;
    const int endurance = body->endurance;

    LogInfo("Player " + name + " lost " + std::to_string(endurance) + " endurance");

    /* Get the player */
    Player* player = this->fellowship->GetPlayer(name);
    if (player == nullptr) {
      LogInfo("Player '" + name + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find " + name);
      response->putHeader("content-type", "text/plain");
      return response;
    }

    player->statistics.RemoveEndurance(endurance);
    this->server->SendUpdateUi();
    return createResponse(Status::CODE_200, "OK");
  }

  /**
   * @brief Hope lost
   */
  ADD_CORS(player_hope_lost,
           this->cors
           );
  ENDPOINT_INFO(player_hope_lost)
  {
    info->summary = "Remove hope to a player";
    info->addConsumes<Object<HopeLostDto>>("application/json");
    info->addResponse<String>(Status::CODE_200, "text/plain");
    info->addResponse<String>(Status::CODE_400, "text/plain");
    info->pathParams["player_name"].description =
      "Name of the player (not the character)";
  }
  ENDPOINT("POST",
           "/player/{player_name}/hope-lost",
           player_hope_lost,
           PATH(String, player_name),
           BODY_DTO(Object<HopeLostDto>, body)
           )
  {
    /* Get data */
    const std::string& name = *player_name;
    const int hope = body->hope;

    LogInfo("Player " + name + " lost " + std::to_string(hope) + " hope");

    /* Get the player */
    Player* player = this->fellowship->GetPlayer(name);
    if (player == nullptr) {
      LogInfo("Player '" + name + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find " + name);
      response->putHeader("content-type", "text/plain");
      return response;
    }

    player->statistics.RemoveHope(hope);
    this->server->SendUpdateUi();
    return createResponse(Status::CODE_200, "OK");
  }

  /**
   * @brief Level up
   */
  ADD_CORS(player_level_up,
           this->cors
           );
  ENDPOINT_INFO(player_level_up)
  {
    info->summary = "Level up a player";
    info->addConsumes<Object<LevelUpDto>>("application/json");
    info->addResponse<String>(Status::CODE_200, "text/plain");
    info->addResponse<String>(Status::CODE_400, "text/plain");
    info->pathParams["player_name"].description =
      "Name of the player (not the character)";
  }
  ENDPOINT("POST",
           "/player/{player_name}/level-up",
           player_level_up,
           PATH(String, player_name),
           BODY_DTO(Object<LevelUpDto>, body)
           )
  {
    /* Get data */
    const std::string& name = *player_name;
    const int adventure_points = body->adventure_points;
    const int skill_points = body->skill_points;

    LogInfo("Player " + name + " level up: " + std::to_string(adventure_points) + " adventure points / " + std::to_string(skill_points) + " skill points spent");

    /* Get the player */
    Player* player = this->fellowship->GetPlayer(name);
    if (player == nullptr) {
      LogInfo("Player '" + name + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find " + name);
      response->putHeader("content-type", "text/plain");
      return response;
    }

    /* Convert */
    std::list<int> skills;
    std::copy(body->skills->begin(), body->skills->end(), std::back_inserter(skills));
    std::list<int> combat;
    std::copy(body->combat_proficiencies->begin(),
              body->combat_proficiencies->end(),
              std::back_inserter(combat));

    /* Update player */
    player->statistics.SpendExperience(skill_points, adventure_points);
    player->statistics.UpdateCombatProficiencies(combat);
    player->statistics.UpdateSkills(skills);
    player->statistics.heroic.valour = body->valour;
    player->statistics.heroic.wisdom = body->wisdom;
    player->statistics.heroic.rewards = body->rewards;
    player->statistics.heroic.virtues = body->virtues;
    this->server->SendUpdateUi();
    return createResponse(Status::CODE_200, "OK");
  }


  /**
   * @brief Update Equipment
   */
  ADD_CORS(player_update_equipment,
           this->cors
           );
  ENDPOINT_INFO(player_update_equipment)
  {
    info->summary = "Update a piece of equipment";
    info->addConsumes<Object<UpdateEquipmentDto>>("application/json");
    info->addResponse<String>(Status::CODE_200, "text/plain");
    info->addResponse<String>(Status::CODE_400, "text/plain");
    info->pathParams["player_name"].description =
      "Name of the player (not the character)";
    info->pathParams["equipment_id"].description =
      "Index of the equipment";
  }
  ENDPOINT("POST",
           "/player/{player_name}/update-equipment/{equipment_id}",
           player_update_equipment,
           PATH(String, player_name),
           PATH(Int32, equipment_id),
           BODY_DTO(Object<UpdateEquipmentDto>, body)
           )
  {
    /* Get data */
    const std::string& name = *player_name;
    const bool wearing = body->wearing;

    LogInfo("Player " + name + " updating equipment " + std::to_string(equipment_id));

    /* Get the player */
    Player* player = this->fellowship->GetPlayer(name);
    if (player == nullptr) {
      LogInfo("Player '" + name + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find " + name);
      response->putHeader("content-type", "text/plain");
      return response;
    }

    if (equipment_id >= static_cast<int>(player->equipment.equipment.size()) ||
        equipment_id < 0) {
      LogInfo("Equipment '" + std::to_string(equipment_id) + "' not found");
      std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> response =
        ResponseFactory::createResponse(Status::CODE_400,
                                        "Failed to find equipment " + std::to_string(equipment_id));
      response->putHeader("content-type", "text/plain");
      return response;
    }

    player->equipment.equipment[equipment_id]->wearing = wearing;
    this->server->SendUpdateUi();
    return createResponse(Status::CODE_200, "OK");
  }

protected:
  std::shared_ptr<Fellowship> fellowship{ nullptr };
  TheOneRingServer* server { nullptr};
  std::string cors;
};

#include OATPP_CODEGEN_END(ApiController) ///< End Codegen

#endif // LOREMASTER_TOOLS_SRC_API_FELLOWSHIP_API_HPP
