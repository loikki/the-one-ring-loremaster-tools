/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_API_DATA_TRANSFER_OBJECT_HPP
#define LOREMASTER_TOOLS_SRC_API_DATA_TRANSFER_OBJECT_HPP


#include "oatpp/parser/json/mapping/ObjectMapper.hpp"
#include "oatpp/web/server/HttpConnectionHandler.hpp"
#include "oatpp/network/Server.hpp"
#include "oatpp/network/tcp/server/ConnectionProvider.hpp"
#include "oatpp/core/macro/codegen.hpp"

/* Begin DTO code-generation */
#include OATPP_CODEGEN_BEGIN(DTO)

class EnduranceLostDto : public oatpp::DTO {

  DTO_INIT(EnduranceLostDto, DTO /* Extends */)
  DTO_FIELD_INFO(endurance) {
    info->description = "Endurance lost";
  }
  DTO_FIELD(Int32, endurance);
};

class HopeLostDto : public oatpp::DTO {

  DTO_INIT(HopeLostDto, DTO /* Extends */)

  DTO_FIELD_INFO(hope) {
    info->description = "Hope lost";
  }
  DTO_FIELD(Int32, hope);
};

class UpdateEquipmentDto: public oatpp::DTO {
  DTO_INIT(UpdateEquipmentDto, DTO /* Extends */)

  DTO_FIELD_INFO(wearing) {
    info->description = "Is the player wearing this item?";
  }
  DTO_FIELD(Boolean, wearing);
};

class LevelUpDto : public oatpp::DTO {

  DTO_INIT(LevelUpDto, DTO /* Extends */)
  DTO_FIELD_INFO(adventure_points) {
    info->description = "Adventure points to add";
  }
  DTO_FIELD(Int32, adventure_points);

  DTO_FIELD_INFO(combat_proficiencies) {
    info->description = "New combat proficiencies values";
  }
  DTO_FIELD(List<Int32>, combat_proficiencies);

  DTO_FIELD_INFO(skill_points) {
    info->description = "Skills points to add";
  }
  DTO_FIELD(Int32, skill_points);

  DTO_FIELD_INFO(skills) {
    info->description = "New skills values";
  }
  DTO_FIELD(List<Int32>, skills);

  DTO_FIELD_INFO(valour) {
    info->description = "New value of valour";
  }
  DTO_FIELD(Int32, valour);

  DTO_FIELD_INFO(wisdom) {
    info->description = "New value of wisdom";
  }
  DTO_FIELD(Int32, wisdom);

  DTO_FIELD_INFO(virtues) {
    info->description = "New text for the virtues";
  }
  DTO_FIELD(String, virtues);

  DTO_FIELD_INFO(rewards) {
    info->description = "New text for the rewards";
  }
  DTO_FIELD(String, rewards);
};

/* End DTO code-generation */
#include OATPP_CODEGEN_END(DTO)

#endif // LOREMASTER_TOOLS_SRC_API_DATA_TRANSFER_OBJECT_HPP
