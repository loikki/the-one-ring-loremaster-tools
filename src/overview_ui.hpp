/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_OVERVIEW_UI_HPP
#define LOREMASTER_TOOLS_SRC_OVERVIEW_UI_HPP

/* Own includes */
#include "gui/ui_overview.h"

/* Other includes */
#include <QObject>

/* Forward declaration */
class Fellowship;

class OverviewUi
  : public QWidget
  , public Ui_overview
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  OverviewUi& operator=(OverviewUi&&) = delete;
  OverviewUi(OverviewUi&&) = delete;
  OverviewUi(const OverviewUi&) = delete;
  OverviewUi& operator=(const OverviewUi&) = delete;
  OverviewUi(QWidget& parent, std::shared_ptr<Fellowship> fellowship);

  ~OverviewUi() override;

  void ShortRest();
  void LongRest();
  void ConnectSignalSlots();
  void RemoveAllSummaries();
  void UpdateUi();

protected:
  /* Attributes */
  QWidget& parent;
  QGridLayout* players_layout;
  std::shared_ptr<Fellowship> fellowship;

signals:
  void SaveToPlayer();
};

#endif // LOREMASTER_TOOLS_SRC_ADVERSARY_HPP
