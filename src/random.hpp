/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_RANDOM_HPP
#define LOREMASTER_TOOLS_SRC_RANDOM_HPP

/* Standard library */
#include <algorithm>
#include <array>
#include <chrono>
#include <iterator>
#include <random>

/* Other libraries */
#include <QObject>
#include <QString>

const int kTengwar = 6;
const int kGandalf = 12;
const int kSauron = 11;
const int kSkillDiceFaces = 6;
const int kFeatDiceFaces = 12;

enum RollType
{
  kRollFavoured,
  kRollNormal,
  kRollIllFavoured,
  kRollCount,
};

const std::array<QString, kRollCount> kRollString = {
  QObject::tr("Favoured"),
  QObject::tr("Normal"),
  QObject::tr("Ill Favoured"),
};

bool inline IsSuccessful(int roll, int feat, int target_number)
{
  return roll >= target_number || feat == kGandalf;
}

int inline RollDice(int type)
{
  std::uniform_int_distribution distribution(1, type);
  unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  static std::default_random_engine generator(seed1);
  return distribution(generator);
}

int inline RollSkillDice(const bool is_weary)
{
  int roll = RollDice(kSkillDiceFaces);
  if (is_weary && roll < 4) {
    roll = 0;
  }
  return roll;
}

int inline RollFeatDice(const RollType roll_type)
{
  int feat = RollDice(kFeatDiceFaces);

  /* Favoured */
  if (roll_type == kRollFavoured) {
    int second = RollDice(kFeatDiceFaces);
    if (second != kSauron && feat != kSauron) {
      feat = std::max(feat, second);
    } else {
      if (second == kGandalf || feat == kGandalf) {
        feat = kGandalf;
      } else {
        feat = std::min(feat, second);
      }
    }
  }
  /* Ill favoured */
  else if (roll_type == kRollIllFavoured) {
    int second = RollDice(kFeatDiceFaces);
    if (feat == kSauron || second == kSauron) {
      feat = kSauron;
    } else {
      feat = std::min(feat, second);
    }
  }
  return feat;
}

double inline GetProbability()
{
  static std::uniform_real_distribution<double> distribution(0.0, 1.0);
  unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  static std::default_random_engine generator(seed1);
  return distribution(generator);
}

template<typename Iter, typename RandomGenerator>
Iter
select_randomly(Iter start, Iter end, RandomGenerator& generator)
{
  std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
  std::advance(start, dis(generator));
  return start;
}

template<typename Iter>
Iter
select_randomly(Iter start, Iter end)
{
  static std::random_device device;
  static std::mt19937 gen(device());
  return select_randomly(start, end, gen);
}

template<typename Iter, typename WeightIter, typename RandomGenerator>
Iter
select_randomly_weighted(Iter start,
                         Iter end,
                         WeightIter weights_start,
                         WeightIter weights_end,
                         RandomGenerator& generator)
{
  std::discrete_distribution<> dis(weights_start, weights_end);
  int distance = dis(generator);
  if (distance >= std::distance(start, end)) {
    throw std::length_error("Weights are not matching the iterator size");
  }
  std::advance(start, distance);
  return start;
}

template<typename Iter, typename WeightIter>
Iter
select_randomly_weighted(Iter start,
                         Iter end,
                         WeightIter weights_start,
                         WeightIter weights_end)
{
  static std::random_device device;
  static std::mt19937 gen(device());
  return select_randomly_weighted(start, end, weights_start, weights_end, gen);
}

template<typename Iter>
void
shuffle(Iter start, Iter end)
{
  static std::random_device device;
  static std::mt19937 gen(device());
  std::shuffle(start, end, gen);
}

#endif // LOREMASTER_TOOLS_SRC_RANDOM_HPP
