/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "fellowship.hpp"

/* Other libraries */
#include <QDir>
#include <QStringList>

void
Fellowship::LoadAllPlayers(const std::string& directory)
{
  /* Get the list of files */
  QDir dir(QString::fromStdString(directory));
  QStringList list = dir.entryList({ "*.pl" });

  /* Load each character */
  for (auto it = list.cbegin(); it != list.cend(); ++it) {
    if (it != list.cbegin() || not this->players[0].IsEmpty()) {
      this->NewPlayer();
    }
    this->players.back().ReadFromFile(directory + "/" + it->toStdString());
  }

  /* Update the fellowship points */
  this->fellowship_points = static_cast<int>(this->players.size());
}

void
Fellowship::SaveAllPlayers(const std::string& directory)
{
  for (const Player& player : this->players) {
    player.SaveToFile(directory);
  }
}

void
Fellowship::RemovePlayer(const Player* player)
{
  for (auto it = this->players.cbegin(); it != this->players.cend(); ++it) {
    if (&(*it) == player) {
      this->players.erase(it);
      /* Update the fellowship points */
      this->fellowship_points--;
      break;
    }
  }
  /* Ensure that we still have at least one player */
  if (this->players.empty()) {
    this->players.emplace_back(Player());
  }
}

Player*
Fellowship::GetPlayer(const std::string& name)
{
  auto player = std::find_if(
    this->players.begin(), this->players.end(), [name](const Player& current) {
      return current.player_name == name;
    });
  Player* return_value = nullptr;
  if (player != this->players.end()) {
    return_value = &*player;
  }
  return return_value;
}

QJsonObject
Fellowship::GetJson() const
{
  QJsonObject object;
  object["fellowship_points"] = QString::number(this->fellowship_points);
  if (this->patron != nullptr) {
    object["patron"] = this->patron->GetJson();
  } else {
    object["patron"] = "";
  }
  return object;
}
