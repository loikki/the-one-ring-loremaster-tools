/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "combat_overview_ui.hpp"
#include "adversary/adversary.hpp"
#include "adversary/adversary_summary_ui.hpp"
#include "adversary/adversary_ui.hpp"
#include "encounter.hpp"
#include "fellowship.hpp"
#include "player/player_summary_combat_ui.hpp"

CombatOverviewUi::CombatOverviewUi(QWidget& parent_ref,
                                   std::shared_ptr<Fellowship> fellowship_ptr,
                                   std::shared_ptr<Encounter> encounter_ptr)
  : Ui_combat_overview()
  , parent(parent_ref)
  , fellowship(std::move(fellowship_ptr))
  , encounter(std::move(encounter_ptr))
{
  this->setupUi(&parent_ref);

  this->players_layout.reset(new QHBoxLayout(this->player_combat_overview));
  this->adversaries_layout.reset(new QHBoxLayout(this->adv_overview));

  this->UpdateUi();
  this->ConnectSignalSlots();
}

void
CombatOverviewUi::ConnectSignalSlots()
{
  QObject::connect(
    this->fellowship_points_spin, &QSpinBox::valueChanged, [=](int value) {
      this->fellowship->fellowship_points = value;
    });
}

void
CombatOverviewUi::UpdateUi()
{
  /* Save the player in order to be safe */
  emit this->SaveToPlayer();

  /* Cleanup current window */
  this->RemoveAllSummaries();

  /* Add all summaries */
  /* players */
  for (Player& player : *this->fellowship) {
    /* Skip empty players */
    if (player.IsEmpty()) {
      continue;
    };
    auto* summary = new PlayerSummaryCombatUi(this->parent, player);
    this->players_layout->addWidget(summary);
  }

  /* adversaries */
  for (Adversary& adversary : *this->encounter) {
    /* Skip empty adversaries */
    if (adversary.IsEmpty()) {
      continue;
    };
    auto* summary = new AdversarySummaryUi(this->parent, adversary);
    this->adversaries_layout->addWidget(summary);
  }

  /* Update fellowship points */
  this->fellowship_points_spin->setValue(this->fellowship->fellowship_points);
}

void
CombatOverviewUi::RemoveAllSummaries()
{
  /* player */
  for (int i = this->players_layout->count() - 1; i >= 0; --i) {
    QLayoutItem* item = this->players_layout->itemAt(i);
    this->players_layout->removeItem(item);
    delete item->widget();
    delete item;
  }

  /* Adversaries */
  for (int i = this->adversaries_layout->count() - 1; i >= 0; --i) {
    QLayoutItem* item = this->adversaries_layout->itemAt(i);
    this->adversaries_layout->removeItem(item);
    delete item->widget();
    delete item;
  }
}

CombatOverviewUi::~CombatOverviewUi()
{
  this->RemoveAllSummaries();
}
