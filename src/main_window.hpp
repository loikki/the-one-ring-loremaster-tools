/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_MAIN_HPP
#define LOREMASTER_TOOLS_SRC_MAIN_HPP

/* Own includes */
#include "encounter.hpp"
#include "gui/ui_main_window.h"
#include "patron_ui.hpp"
#include "player/player_ui.hpp"
#include "server/server.hpp"

/* Other libraries */
#include <QMainWindow>

/* Forward declaration */
class AdversaryUi;
class CombatOverviewUi;
class OverviewUi;
class Fellowship;
class ServerLogsUi;

enum Tabs
{
  kTabPlayers = 0,
  kTabAdversaries,
  kTabCombatOverview,
  kTabOverview,
  kTabPatron,
  kTabServerLogs,
  kTabCount,
};

class MainWindow : public QObject
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  MainWindow& operator=(MainWindow&&) = delete;
  MainWindow(MainWindow&&) = delete;

  MainWindow(QMainWindow& main_window, QApplication& app);
  MainWindow& operator=(const MainWindow&) = delete;
  MainWindow(const MainWindow&) = delete;

  ~MainWindow() override;

  [[nodiscard]] std::string GetDirectory() const
  {
    return this->settings.directory;
  };
  [[nodiscard]] QMainWindow& GetMainWindow() const
  {
    return this->main_window;
  };
  [[nodiscard]] Ui_TheOneRing* GetMainUi() const
  {
    return this->main_ui.get();
  };
  [[nodiscard]] QStatusBar* GetStatusBar() const
  {
    return this->main_ui->statusBar;
  };
  [[nodiscard]] PlayerUi* GetPlayerUi() const { return this->player_ui.get(); };
  [[nodiscard]] AdversaryUi* GetAdversaryUi() const
  {
    return this->adversary_ui.get();
  };
  [[nodiscard]] Settings& GetSettings() { return this->settings; };
  [[nodiscard]] bool HasServerLogsFocus() const
  {
    return this->main_ui->tabWidget->currentIndex() == kTabServerLogs;
  }
  [[nodiscard]] Fellowship& GetFellowship() { return *this->fellowship; }
  [[nodiscard]] Encounter& GetEncounter() { return *this->encounter; }
  [[nodiscard]] std::string GetEncounterDirectory() const
  {
    return this->settings.directory + "/Encounters/";
  }
  void UpdateCors();

  // NOLINTNEXTLINE
public slots:
  void TabChanged() const;

protected:
  void SaveEncounter() const;
  void ChangeLanguage(const std::string& lang);
  static std::map<std::string, std::string> ExtractLanguages(
    bool reverse_order = false);
  void SetDirectory();
  void GetNewLanguage();
  void LoadEncounter() const;
  void LoadAllPlayers() const;
  void ConnectSignalSlots();
  static void NameGenerator();
  void RandomEvent() const;
  static void RandomFeatures();
  static void RollsProbabilities();
  void ShuffleList() const;
  static void RuinsGenerator();
  void StartOrStopServer();
  void SaveAllPlayers() const;
  void RewardExperience() const;
  void ChangeTargetNumber() const;

  /* Attribute */
  QApplication& app;
  QMainWindow& main_window;
  Settings settings;
  std::shared_ptr<Fellowship> fellowship;
  std::shared_ptr<Encounter> encounter;
  std::unique_ptr<Ui_TheOneRing> main_ui;
  std::unique_ptr<QTranslator> translator;
  std::unique_ptr<PlayerUi> player_ui;
  std::unique_ptr<AdversaryUi> adversary_ui;
  std::unique_ptr<CombatOverviewUi> combat_overview_ui;
  std::unique_ptr<OverviewUi> overview_ui;
  std::unique_ptr<ServerLogsUi> server_logs_ui;
  std::unique_ptr<PatronUi> patron_ui;
  TheOneRingServer server;

#ifdef OWN_TEST
  friend class TestOverviewUi;

public:
  void ResetPlayerUi(PlayerUi* new_player_ui)
  {
    this->player_ui.reset(new_player_ui);
  }
  void ResetAdversaryUi(AdversaryUi* new_adversary_ui)
  {
    this->adversary_ui.reset(new_adversary_ui);
  }
  std::shared_ptr<Fellowship> GetSharedFellowship()
  {
    return this->fellowship;
  }
  std::shared_ptr<Encounter> GetSharedEncounter()
  {
    return this->encounter;
  }
  OverviewUi* GetOverviewUi()
  {
    return this->overview_ui.get();
  }
#endif
};

#endif // LOREMASTER_TOOLS_SRC_MAIN_HPP
