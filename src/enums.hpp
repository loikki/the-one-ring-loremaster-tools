/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_ENUMS_HPP
#define LOREMASTER_TOOLS_SRC_ENUMS_HPP

/* Standard library */
#include <array>

/* Other libraries */
#include <QObject>
#include <QString>

enum Skills
{
  kSkillsAwe = 0,
  kSkillsAthletics,
  kSkillsAwareness,
  kSkillsHunting,
  kSkillsSong,
  kSkillsCraft,
  kSkillsEnhearten,
  kSkillsTravel,
  kSkillsInsight,
  kSkillsHealing,
  kSkillsCourtesy,
  kSkillsBattle,
  kSkillsPersuade,
  kSkillsStealth,
  kSkillsScan,
  kSkillsExplore,
  kSkillsRiddle,
  kSkillsLore,
  kSkillsCount,
};

enum CombatProficiencies
{
  kCombatProficienciesAxes = 0,
  kCombatProficienciesBows,
  kCombatProficienciesSpears,
  kCombatProficienciesSwords,
  kCombatProficienciesCount,
};

static const std::array<std::string, kCombatProficienciesCount>
  kCombatProficienciesString = { "Axes", "Bows", "Spears", "Swords" };

enum LivingStandard
{
  kLivingStandardPoor = 0,
  kLivingStandardFrugal,
  kLivingStandardCommon,
  kLivingStandardProsperous,
  kLivingStandardRich,
  kLivingStandardVeryRich,
  kLivingStandardCount,
};

static const std::array<int, kLivingStandardCount - 1>
  kLivingStandardLimits = { 0, 30, 90, 180, 300 };

static const std::array<QString, kLivingStandardCount> kLivingStandardString = {
  QObject::tr("Poor"),       QObject::tr("Frugal"), QObject::tr("Common"),
  QObject::tr("Prosperous"), QObject::tr("Rich"),   QObject::tr("Very Rich"),
};

enum JourneyRole
{
  kJourneyRoleGuide = 0,
  kJourneyRoleHunter,
  kJourneyRoleLookout,
  kJourneyRoleScout,
  kJourneyRoleCount,
};

static const std::array<QString, kJourneyRoleCount> kJourneyRoleString = {
  QObject::tr("Guide"),
  QObject::tr("Hunter"),
  QObject::tr("Lookout"),
  QObject::tr("Scout")
};

enum EventType
{
  kEventTypeTerribleMisfortune = 0,
  kEventTypeDespair,
  kEventTypeIllChoice,
  kEventTypeMishap,
  kEventTypeShortcut,
  kEventTypeChanceMeeting,
  kEventTypeJoyfulSight,
  kEventTypeCount,
};

static const std::array<QString, kEventTypeCount> kEventTypeString = {
  QObject::tr("Terrible Misfortune"),
  QObject::tr("Despair"),
  QObject::tr("Ill Choice"),
  QObject::tr("Mishap"),
  QObject::tr("Shortcut"),
  QObject::tr("Chance Meeting"),
  QObject::tr("Joyful Sight"),
};

enum EquipmentType
{
  kEquipmentTypeEquipment = 0,
  kEquipmentTypeWeapon,
  kEquipmentTypeArmour,
  kEquipmentTypeShield,
  kEquipmentTypeHelm,
};

enum ArmourType
{
  kArmourTypeArmour = 0,
  kArmourTypeShield,
  kArmourTypeHelm,
  kArmourTypeCount,
};

static const std::array<QString, kArmourTypeCount> kArmourTypeString = {
  QObject::tr("Armour"),
  QObject::tr("Shield"),
  QObject::tr("Helm"),
};

#endif // LOREMASTER_TOOLS_SRC_ENUMS_HPP
