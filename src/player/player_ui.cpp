/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Local includes */
#include "player_ui.hpp"
#include "../enums.hpp"
#include "../fellowship.hpp"
#include "../utils.hpp"

/* Other libraries */
#include <QFileDialog>
#include <QMouseEvent>

PlayerUi::PlayerUi(QWidget& parent_ref,
                   std::string new_directory,
                   std::shared_ptr<Fellowship> fellowship_ptr,
                   bool hide_image,
                   bool hide_skills,
                   bool lock_player_bool)
  : Ui_player()
  , parent(parent_ref)
  , fellowship(std::move(fellowship_ptr))
  , directory(std::move(new_directory))
{
  this->setupUi(&parent_ref);
  this->UpdateCurrentPlayer(-1);

  /* Manage tables */
  this->m_equipment_table_manager =
    std::make_unique<EquipmentTableManager>(*this->object_table);
  this->m_armour_table_manager =
    std::make_unique<ArmourTableManager>(*this->armour_table);
  this->m_weapon_table_manager =
    std::make_unique<WeaponTableManager>(*this->weapon_table);

  this->ConnectSignalSlots();

  this->HideSkills(hide_skills);
  this->HideImage(hide_image);
  this->lock_player->setChecked(lock_player_bool);

  emit this->UpdateStatusBar(tr("Player ready"));
}

PlayerUi::~PlayerUi()
{
  this->choice->blockSignals(true);
  this->name->blockSignals(true);
}

void
PlayerUi::LockPlayer(bool lock)
{
  /* Warn the main window */
  emit this->lockChanged(lock);

  lock = !lock;

  /* Main */
  this->player_name->setEnabled(lock);
  this->player_label->setEnabled(lock);
  this->features->setEnabled(lock);
  this->features_label->setEnabled(lock);
  this->culture->setEnabled(lock);
  this->culture_label->setEnabled(lock);
  this->calling->setEnabled(lock);
  this->calling_label->setEnabled(lock);
  this->shadow_path->setEnabled(lock);
  this->shadow_path_label->setEnabled(lock);
  this->name->setEnabled(lock);
  this->name_label->setEnabled(lock);
  this->living->setEnabled(lock);
  this->living_label->setEnabled(lock);

  /* strength */
  this->strength->setEnabled(lock);
  this->strength_label->setEnabled(lock);
  this->strength_tn->setEnabled(lock);
  this->strength_tn_label->setEnabled(lock);
  this->endurance->setEnabled(lock);
  this->endurance_label->setEnabled(lock);
  for (int i = 0; i < this->strength_skills->count(); ++i) {
    this->strength_skills->itemAt(i)->widget()->setEnabled(lock);
  }
  this->axes->setEnabled(lock);
  this->axes_label->setEnabled(lock);
  this->bows->setEnabled(lock);
  this->bows_label->setEnabled(lock);
  this->spears->setEnabled(lock);
  this->spears_label->setEnabled(lock);
  this->swords->setEnabled(lock);
  this->swords_label->setEnabled(lock);

  /* heart */
  this->heart->setEnabled(lock);
  this->heart_label->setEnabled(lock);
  this->heart_tn->setEnabled(lock);
  this->heart_tn_label->setEnabled(lock);
  this->hope->setEnabled(lock);
  this->hope_label->setEnabled(lock);
  for (int i = 0; i < this->heart_skills->count(); ++i) {
    this->heart_skills->itemAt(i)->widget()->setEnabled(lock);
  }
  this->valour->setEnabled(lock);
  this->valour_label->setEnabled(lock);
  this->rewards->setEnabled(lock);

  /* Wits */
  this->wits->setEnabled(lock);
  this->wits_label->setEnabled(lock);
  this->wits_tn->setEnabled(lock);
  this->wits_tn_label->setEnabled(lock);
  this->parry->setEnabled(lock);
  this->parry_label->setEnabled(lock);
  for (int i = 0; i < this->wits_skills->count(); ++i) {
    this->wits_skills->itemAt(i)->widget()->setEnabled(lock);
  }
  this->wisdom->setEnabled(lock);
  this->wisdom_label->setEnabled(lock);
  this->virtues->setEnabled(lock);

  /* Experience */
  this->adventure_points_label->setEnabled(lock);
  this->skill_points_label->setEnabled(lock);
  this->current_label->setEnabled(lock);
  this->total_label->setEnabled(lock);
  this->current_adventure_points->setEnabled(lock);
  this->total_adventure_points->setEnabled(lock);
  this->current_skill_points->setEnabled(lock);
  this->total_skill_points->setEnabled(lock);
}

void
PlayerUi::ConnectSignalSlots()
{
  /* Change name */
  QObject::connect(
    this->name, &QLineEdit::textChanged, [=](const QString& text) {
      int current = this->choice->currentIndex();
      this->player->name = text.toStdString();
      this->choice->setItemText(current,
                                QString::fromStdString(this->player->name));
    });

  /* Lock player */
  QObject::connect(this->lock_player, &QCheckBox::stateChanged, [=](int state) {
    this->LockPlayer(state != 0);
  });

  /* New player */
  QObject::connect(
    this->new_button, &QPushButton::clicked, [=]() { this->NewPlayer(); });

  /* Save player */
  QObject::connect(this->save_button, &QPushButton::clicked, [=]() {
    this->SaveCurrentPlayer();
  });

  /* Load player */
  QObject::connect(
    this->load_button, &QPushButton::clicked, [=]() { this->LoadPlayer(); });

  /* Remove player */
  QObject::connect(this->remove_button, &QPushButton::clicked, [=]() {
    this->RemoveCurrentPlayer();
  });

  /* Change player */
  QObject::connect(this->choice, &QComboBox::currentIndexChanged, [=]() {
    this->SaveCurrentPlayer();
    this->UpdateCurrentPlayer(this->choice->currentIndex());
  });

  /* Strength */
  QObject::connect(
    this->strength, &QSpinBox::valueChanged, [=](const int& value) {
      this->player->statistics.attributes.strength = value;
      QString text =
        QString::number(this->player->statistics.GetStrengthTargetNumber());
      this->strength_tn->setText(text);
    });

  /* Wits */
  QObject::connect(this->wits, &QSpinBox::valueChanged, [=](const int& value) {
    this->player->statistics.attributes.wits = value;
    QString text =
      QString::number(this->player->statistics.GetWitsTargetNumber());
    this->wits_tn->setText(text);
  });

  /* Heart */
  QObject::connect(this->heart, &QSpinBox::valueChanged, [=](const int& value) {
    this->player->statistics.attributes.heart = value;
    QString text =
      QString::number(this->player->statistics.GetHeartTargetNumber());
    this->heart_tn->setText(text);
  });

  /* Load */
  auto UpdateLoad = [=](const int& /*value*/) { this->UpdateLoadUi(); };
  QObject::connect(this->current_treasure, &QSpinBox::valueChanged, UpdateLoad);

  /* Standard of living */
  QObject::connect(
    this->total_treasure, &QSpinBox::valueChanged, [=](const int& /*value*/) {
      this->UpdateStandardOfLivingUi();
    });

  /* Image */
  this->image->installEventFilter(this);

  /* Equipment: new item */
  QObject::connect(
    this->m_equipment_table_manager.get(),
    &EquipmentTableManager::NewItem,
    [=]() {
      m_equipment_table_manager->SaveTo(this->player->equipment.equipment);
      this->player->equipment.equipment.push_back(
        std::make_shared<Equipment>());
      m_equipment_table_manager->Update(this->player->equipment.equipment);
    });

  QObject::connect(
    this->m_armour_table_manager.get(), &ArmourTableManager::NewItem, [=]() {
      m_armour_table_manager->SaveTo(this->player->equipment.equipment);
      this->player->equipment.equipment.push_back(std::make_shared<Armour>());
      m_armour_table_manager->Update(this->player->equipment.equipment);
    });

  QObject::connect(
    this->m_weapon_table_manager.get(), &WeaponTableManager::NewItem, [=]() {
      m_weapon_table_manager->SaveTo(this->player->equipment.equipment);
      this->player->equipment.equipment.push_back(std::make_shared<Weapon>());
      m_weapon_table_manager->Update(this->player->equipment.equipment);
    });

  /* Equipment: delete item */
  auto delete_item = [=](const std::shared_ptr<Equipment>& equip,
                         EquipmentTableManager* table_manager) {
    table_manager->SaveTo(this->player->equipment.equipment);
    int index = 0;
    for (const std::shared_ptr<Equipment>& current :
         this->player->equipment.equipment) {
      if (current == equip) {
        player->equipment.equipment.erase(player->equipment.equipment.begin() +
                                          index);
        break;
      }
      index += 1;
    }
    table_manager->Update(this->player->equipment.equipment);
    this->UpdateProtectionUi();
    this->UpdateLoadUi();
  };

  QObject::connect(m_equipment_table_manager.get(),
                   &EquipmentTableManager::RemoveItem,
                   [=](const std::shared_ptr<Equipment>& equip) {
                     delete_item(equip, m_equipment_table_manager.get());
                   });

  QObject::connect(m_weapon_table_manager.get(),
                   &WeaponTableManager::RemoveItem,
                   [=](const std::shared_ptr<Equipment>& equip) {
                     delete_item(equip, m_weapon_table_manager.get());
                   });

  QObject::connect(m_armour_table_manager.get(),
                   &ArmourTableManager::RemoveItem,
                   [=](const std::shared_ptr<Equipment>& equip) {
                     delete_item(equip, m_armour_table_manager.get());
                   });

  /* Equipment: update item */
  auto update_item = [=](int row,
                         const std::shared_ptr<Equipment>& equip,
                         const EquipmentTableManager* table_manager) {
    table_manager->SaveItem(row, equip);
    this->UpdateProtectionUi();
    this->UpdateLoadUi();
  };

  QObject::connect(m_equipment_table_manager.get(),
                   &EquipmentTableManager::UpdatedItem,
                   [=](int row, const std::shared_ptr<Equipment>& equip) {
                     update_item(row, equip, m_equipment_table_manager.get());
                   });

  QObject::connect(m_weapon_table_manager.get(),
                   &WeaponTableManager::UpdatedItem,
                   [=](int row, const std::shared_ptr<Equipment>& equip) {
                     update_item(row, equip, m_weapon_table_manager.get());
                   });

  QObject::connect(m_armour_table_manager.get(),
                   &ArmourTableManager::UpdatedItem,
                   [=](int row, const std::shared_ptr<Equipment>& equip) {
                     update_item(row, equip, m_armour_table_manager.get());
                   });

  /* Equipment: change type */
  auto change_type = [=](int row,
                         const std::shared_ptr<Armour>& armour,
                         ArmourTableManager* table_manager) {
    table_manager->SaveTo(player->equipment.equipment);
    ArmourType new_type = table_manager->GetType(row);
    delete_item(std::dynamic_pointer_cast<Equipment>(armour),
                dynamic_cast<EquipmentTableManager*>(table_manager));

    switch (new_type) {
      case kArmourTypeArmour:
        this->player->equipment.equipment.push_back(std::make_shared<Armour>());
        break;
      case kArmourTypeShield:
        this->player->equipment.equipment.push_back(std::make_shared<Shield>());
        break;
      case kArmourTypeHelm:
        this->player->equipment.equipment.push_back(std::make_shared<Helm>());
        break;
      default:
        throw std::logic_error("Unknown armour type");
    }

    *player->equipment.equipment.back() = *armour;
    table_manager->Update(player->equipment.equipment);

    this->UpdateProtectionUi();
    this->UpdateLoadUi();
  };

  QObject::connect(m_armour_table_manager.get(),
                   &ArmourTableManager::ChangeType,
                   [=](int row, const std::shared_ptr<Armour>& armour) {
                     change_type(row, armour, m_armour_table_manager.get());
                   });
};

void
PlayerUi::LoadPlayer(std::string filename)
{
  if (filename.length() == 0) {
    /* Get the directory */
    const std::string default_directory = this->GetDirectory();
    const QString qdefault_directory =
      QString::fromStdString(default_directory);

    /* Ensure that the directory exists */
    if (!QDir().mkpath(qdefault_directory)) {
      throw std::ios_base::failure("Failed to created the directory '" +
                                   default_directory + "'");
    };
    filename = QFileDialog::getOpenFileName(&this->parent,
                                            tr("Open Player"),
                                            qdefault_directory,
                                            tr("Player file (*.pl)"))
                 .toStdString();

    /* Check if we received something */
    if (filename.length() == 0) {
      return;
    }
  }

  /* Read it from the file */
  this->player->ReadFromFile(filename);

  /* Update Ui */
  this->UpdateUi();

  emit this->UpdateStatusBar(tr("Player loaded"));
}

std::string
PlayerUi::GetDirectory() const
{
  return this->directory + "/Players/";
}

void
PlayerUi::UpdateUi()
{
  /* Update the player */
  const int player_index = this->choice->currentIndex();
  if (this->player != &(*this->fellowship)[player_index]) {
    this->SaveCurrentPlayer();
    this->player = &(*this->fellowship)[player_index];
  }

  /* Update the UI */
  this->features->setText(QString::fromStdString(this->player->features));
  this->living->setText(
    // NOLINTNEXTLINE
    kLivingStandardString[this->player->GetStandardOfLiving()]);
  this->shadow_path->setText(QString::fromStdString(this->player->shadow_path));
  this->culture->setText(QString::fromStdString(this->player->culture));
  this->name->setText(QString::fromStdString(this->player->name));
  this->player_name->setText(QString::fromStdString(this->player->player_name));
  this->calling->setText(QString::fromStdString(this->player->calling));

  const PlayerStatistics& stats = this->player->statistics;
  this->strength->setValue(stats.attributes.strength);
  this->strength_tn->setText(QString::number(stats.GetStrengthTargetNumber()));
  this->endurance->setValue(stats.attributes.max_endurance);
  this->heart->setValue(stats.attributes.heart);
  this->heart_tn->setText(QString::number(stats.GetHeartTargetNumber()));
  this->hope->setValue(stats.attributes.max_hope);
  this->wits->setValue(stats.attributes.wits);
  this->wits_tn->setText(QString::number(stats.GetWitsTargetNumber()));
  this->parry->setValue(stats.attributes.parry);

  /* Update the skills */
  this->UpdateSkillsInUi();

  /* Update main stats */
  this->valour->setValue(stats.heroic.valour);
  this->rewards->setText(QString::fromStdString(stats.heroic.rewards));
  this->wisdom->setValue(stats.heroic.wisdom);
  this->virtues->setText(QString::fromStdString(stats.heroic.virtues));

  /* Update the equipment */
  this->UpdateEquipmentUi();

  /* Status */
  this->wounded->setChecked(stats.wounded);
  this->injury->setText(QString::fromStdString(stats.injury));
  this->shadow->setValue(stats.heroic.shadow);
  this->shadow_scars->setValue(stats.heroic.shadow_scars);

  /* Experience */
  const Experience& exp = stats.experience;
  this->current_adventure_points->setValue(exp.current_adventure);
  this->total_adventure_points->setValue(exp.total_adventure);
  this->current_skill_points->setValue(exp.current_skill);
  this->total_skill_points->setValue(exp.total_skill);

  /* Set derived values  */
  this->UpdateLoadUi();
  this->UpdateProtectionUi();
  PlayerEquipment& equip = this->player->equipment;
  this->current_treasure->setValue(equip.treasure.current);
  SetValueWithBlock(this->total_treasure, equip.treasure.total);
  if (this->player->image.isNull()) {
    this->image->setText(tr("Click me to set an image"));
  } else {
    this->image->setPixmap(QPixmap::fromImage(this->player->image));
  }
}

void
PlayerUi::UpdateSkillsInUi()
{
  const struct Proficiencies& stats = this->player->statistics.proficiencies;

  /* Strength  */
  this->awe_favorite->setChecked(stats.skills_favorite[kSkillsAwe]);
  this->awe->setValue(stats.skills[kSkillsAwe]);
  this->athletics_favorite->setChecked(stats.skills_favorite[kSkillsAthletics]);
  this->athletics->setValue(stats.skills[kSkillsAthletics]);
  this->awareness_favorite->setChecked(stats.skills_favorite[kSkillsAwareness]);
  this->awareness->setValue(stats.skills[kSkillsAwareness]);
  this->hunting_favorite->setChecked(stats.skills_favorite[kSkillsHunting]);
  this->hunting->setValue(stats.skills[kSkillsHunting]);
  this->song_favorite->setChecked(stats.skills_favorite[kSkillsSong]);
  this->song->setValue(stats.skills[kSkillsSong]);
  this->craft_favorite->setChecked(stats.skills_favorite[kSkillsCraft]);
  this->craft->setValue(stats.skills[kSkillsCraft]);

  /* Heart */
  this->enhearten_favorite->setChecked(stats.skills_favorite[kSkillsEnhearten]);
  this->enhearten->setValue(stats.skills[kSkillsEnhearten]);
  this->travel_favorite->setChecked(stats.skills_favorite[kSkillsTravel]);
  this->travel->setValue(stats.skills[kSkillsTravel]);
  this->insight_favorite->setChecked(stats.skills_favorite[kSkillsInsight]);
  this->insight->setValue(stats.skills[kSkillsInsight]);
  this->healing_favorite->setChecked(stats.skills_favorite[kSkillsHealing]);
  this->healing->setValue(stats.skills[kSkillsHealing]);
  this->courtesy_favorite->setChecked(stats.skills_favorite[kSkillsCourtesy]);
  this->courtesy->setValue(stats.skills[kSkillsCourtesy]);
  this->battle_favorite->setChecked(stats.skills_favorite[kSkillsBattle]);
  this->battle->setValue(stats.skills[kSkillsBattle]);

  /* Wits */
  this->persuade_favorite->setChecked(stats.skills_favorite[kSkillsPersuade]);
  this->persuade->setValue(stats.skills[kSkillsPersuade]);
  this->stealth_favorite->setChecked(stats.skills_favorite[kSkillsStealth]);
  this->stealth->setValue(stats.skills[kSkillsStealth]);
  this->scan_favorite->setChecked(stats.skills_favorite[kSkillsScan]);
  this->scan->setValue(stats.skills[kSkillsScan]);
  this->explore_favorite->setChecked(stats.skills_favorite[kSkillsExplore]);
  this->explore->setValue(stats.skills[kSkillsExplore]);
  this->riddle_favorite->setChecked(stats.skills_favorite[kSkillsRiddle]);
  this->riddle->setValue(stats.skills[kSkillsRiddle]);
  this->lore_favorite->setChecked(stats.skills_favorite[kSkillsLore]);
  this->lore->setValue(stats.skills[kSkillsLore]);

  /* Fight */
  this->axes->setValue(stats.combat[kCombatProficienciesAxes]);
  this->bows->setValue(stats.combat[kCombatProficienciesBows]);
  this->spears->setValue(stats.combat[kCombatProficienciesSpears]);
  this->swords->setValue(stats.combat[kCombatProficienciesSwords]);
}

void
PlayerUi::NewPlayer()
{
  this->fellowship->NewPlayer();
  this->UpdateCurrentPlayer(-1);

  emit this->UpdateStatusBar(tr("New player created"));
}

void
PlayerUi::RemoveCurrentPlayer()
{
  /* Remove the player from the list */
  this->fellowship->RemovePlayer(this->player);

  /* Update the current player */
  this->UpdateCurrentPlayer(-1);

  emit this->UpdateStatusBar(tr("Player removed"));
}

void
PlayerUi::UpdateCurrentPlayer(int index)
{
  if (index < 0) {
    index = static_cast<int>(this->fellowship->size()) + index;
  }

  /* Block the signals */
  this->choice->blockSignals(true);

  /* Reset the combo box */
  this->choice->clear();
  for (const Player& current : *this->fellowship) {
    this->choice->addItem(QString::fromStdString(current.name));
  }

  /* Set the current index */
  this->choice->setCurrentIndex(index);
  this->player = &(*this->fellowship)[index];

  /* Unblock the signals */
  this->choice->blockSignals(false);

  /* Update UI */
  this->UpdateUi();
}

void
PlayerUi::SaveCurrentPlayer()
{
  this->SaveToPlayer();
  this->player->SaveToFile(this->GetDirectory());
  emit this->UpdateStatusBar(tr("Player saved"));
}

void
PlayerUi::HideSkills(bool hide)
{
  /* Strength */
  for (int i = 0; i < this->strength_skills->count(); ++i) {
    this->strength_skills->itemAt(i)->widget()->setVisible(!hide);
  }
  /* Heart */
  for (int i = 0; i < this->heart_skills->count(); ++i) {
    this->heart_skills->itemAt(i)->widget()->setVisible(!hide);
  }
  /* Wits */
  for (int i = 0; i < this->wits_skills->count(); ++i) {
    this->wits_skills->itemAt(i)->widget()->setVisible(!hide);
  }
}

void
PlayerUi::HideImage(bool hide)
{
  this->image->setVisible(!hide);
}

void
PlayerUi::SaveToPlayer()
{
  /* Update the texts */
  this->player->features = this->features->text().toStdString();
  this->player->shadow_path = this->shadow_path->text().toStdString();
  this->player->culture = this->culture->text().toStdString();
  this->player->name = this->name->text().toStdString();
  this->player->player_name = this->player_name->text().toStdString();
  this->player->calling = this->calling->text().toStdString();

  /* Update the main stats */
  PlayerStatistics& stats = this->player->statistics;
  stats.attributes.strength = this->strength->value();
  stats.attributes.max_endurance = this->endurance->value();
  stats.attributes.heart = this->heart->value();
  stats.attributes.max_hope = this->hope->value();
  stats.attributes.wits = this->wits->value();
  stats.attributes.parry = this->parry->value();

  /* Update the skills */
  this->SaveSkillsToPlayer();

  /* Update main stats */
  stats.heroic.valour = this->valour->value();
  stats.heroic.rewards = this->rewards->toPlainText().toStdString();
  stats.heroic.wisdom = this->wisdom->value();
  stats.heroic.virtues = this->virtues->toPlainText().toStdString();

  /* Status */
  stats.wounded = this->wounded->isChecked();
  stats.injury = this->injury->toPlainText().toStdString();
  stats.heroic.shadow = this->shadow->value();
  stats.heroic.shadow_scars = this->shadow_scars->value();

  /* Experience */
  Experience& exp = stats.experience;
  exp.current_adventure = this->current_adventure_points->value();
  exp.total_adventure = this->total_adventure_points->value();
  exp.current_skill = this->current_skill_points->value();
  exp.total_skill = this->total_skill_points->value();

  /* Set derived values  */
  PlayerEquipment& equip = this->player->equipment;
  equip.treasure.current = this->current_treasure->value();
  equip.treasure.total = this->total_treasure->value();
  this->player->image = this->image->pixmap().toImage();

  /* Equipment */
  this->m_equipment_table_manager->SaveTo(this->player->equipment.equipment);
  this->m_armour_table_manager->SaveTo(this->player->equipment.equipment);
  this->m_weapon_table_manager->SaveTo(this->player->equipment.equipment);
}

void
PlayerUi::SaveSkillsToPlayer()
{
  struct Proficiencies& stats = this->player->statistics.proficiencies;

  /* Strength  */
  stats.skills_favorite[kSkillsAwe] = this->awe_favorite->isChecked();
  stats.skills[kSkillsAwe] = this->awe->value();
  stats.skills_favorite[kSkillsAthletics] =
    this->athletics_favorite->isChecked();
  stats.skills[kSkillsAthletics] = this->athletics->value();
  stats.skills_favorite[kSkillsAwareness] =
    this->awareness_favorite->isChecked();
  stats.skills[kSkillsAwareness] = this->awareness->value();
  stats.skills_favorite[kSkillsHunting] = this->hunting_favorite->isChecked();
  stats.skills[kSkillsHunting] = this->hunting->value();
  stats.skills_favorite[kSkillsSong] = this->song_favorite->isChecked();
  stats.skills[kSkillsSong] = this->song->value();
  stats.skills_favorite[kSkillsCraft] = this->craft_favorite->isChecked();
  stats.skills[kSkillsCraft] = this->craft->value();

  /* Heart */
  stats.skills_favorite[kSkillsEnhearten] =
    this->enhearten_favorite->isChecked();
  stats.skills[kSkillsEnhearten] = this->enhearten->value();
  stats.skills_favorite[kSkillsTravel] = this->travel_favorite->isChecked();
  stats.skills[kSkillsTravel] = this->travel->value();
  stats.skills_favorite[kSkillsInsight] = this->insight_favorite->isChecked();
  stats.skills[kSkillsInsight] = this->insight->value();
  stats.skills_favorite[kSkillsHealing] = this->healing_favorite->isChecked();
  stats.skills[kSkillsHealing] = this->healing->value();
  stats.skills_favorite[kSkillsCourtesy] = this->courtesy_favorite->isChecked();
  stats.skills[kSkillsCourtesy] = this->courtesy->value();
  stats.skills_favorite[kSkillsBattle] = this->battle_favorite->isChecked();
  stats.skills[kSkillsBattle] = this->battle->value();

  /* Wits */
  stats.skills_favorite[kSkillsPersuade] = this->persuade_favorite->isChecked();
  stats.skills[kSkillsPersuade] = this->persuade->value();
  stats.skills_favorite[kSkillsStealth] = this->stealth_favorite->isChecked();
  stats.skills[kSkillsStealth] = this->stealth->value();
  stats.skills_favorite[kSkillsScan] = this->scan_favorite->isChecked();
  stats.skills[kSkillsScan] = this->scan->value();
  stats.skills_favorite[kSkillsExplore] = this->explore_favorite->isChecked();
  stats.skills[kSkillsExplore] = this->explore->value();
  stats.skills_favorite[kSkillsRiddle] = this->riddle_favorite->isChecked();
  stats.skills[kSkillsRiddle] = this->riddle->value();
  stats.skills_favorite[kSkillsLore] = this->lore_favorite->isChecked();
  stats.skills[kSkillsLore] = this->lore->value();

  /* Fight */
  stats.combat[kCombatProficienciesAxes] = this->axes->value();
  stats.combat[kCombatProficienciesBows] = this->bows->value();
  stats.combat[kCombatProficienciesSpears] = this->spears->value();
  stats.combat[kCombatProficienciesSwords] = this->swords->value();
}

bool
PlayerUi::eventFilter(QObject* /* watched */, QEvent* event)
{
  /* Check if we have a mouse event */
  if (event->type() != QEvent::MouseButtonPress) {
    return false;
  }
  auto* mouse_event = dynamic_cast<QMouseEvent*>(event);

  /* Check if we are interacting with an interesting object */
  if (not this->image->underMouse()) {
    return false;
  }

  /* Right button */
  if (mouse_event->button() == Qt::RightButton) {
    /* Get the image */
    QString filename = QFileDialog::getOpenFileName(
      &this->parent, tr("Open Image"), "", tr("Image (*.jpeg *.jpg *.png)"));
    if (filename.size() == 0) {
      return false;
    }
    /* Add the image to the label */
    QPixmap new_image(filename);
    new_image = new_image.scaled(this->image->size(), Qt::KeepAspectRatio);
    this->image->setPixmap(new_image);
  } else if (mouse_event->button() == Qt::LeftButton) {
    /* Left button */
    this->image->setPixmap(QPixmap());
  }

  return false;
};
