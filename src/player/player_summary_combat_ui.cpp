/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_summary_combat_ui.hpp"
#include "../utils.hpp"

PlayerSummaryCombatUi::PlayerSummaryCombatUi(QWidget& parent_ref,
                                             Player& player_ref)
  : QFrame(&parent_ref)
  , Ui_PlayerSummaryCombat()
  , player(player_ref)
  , parent(parent_ref)
{
  this->setupUi(this);
  this->ConnectSignalSlots();
  this->UpdateUi();
}

void
PlayerSummaryCombatUi::UpdateUi()
{
  this->player_box->setTitle(QString::fromStdString(this->player.name));
  this->parry->setText(QString::number(this->player.GetTotalParry()));
  this->helm->setChecked(this->player.equipment.IsWearingHelm());
  this->shield->setChecked(this->player.equipment.IsWearingShield());
  SetCheckedWithBlock(this->wounded, this->player.statistics.wounded);

  this->SetHope();
  this->SetEndurance();
};

void
PlayerSummaryCombatUi::ConnectSignalSlots()
{
  QObject::connect(
    this->shield, &QCheckBox::stateChanged, [=]() { this->UpdateShield(); });
  QObject::connect(
    this->helm, &QCheckBox::stateChanged, [=]() { this->UpdateHelm(); });
  QObject::connect(
    this->wounded, &QCheckBox::stateChanged, [=]() { this->UpdateWound(); });
};

void
PlayerSummaryCombatUi::ClickedOnBar(const bool under_endurance)
{
  /* Get some strings */
  QString title(tr("Hope"));
  QString description(tr("Hope lost"));
  if (under_endurance) {
    title = tr("Endurance");
    description = tr("Endurance lost");
  }

  /* Get the value */
  bool is_ok = false;
  int value = QInputDialog::getInt(&this->parent,
                                   title,
                                   description,
                                   /* value */ 0,
                                   /* min */ -kMaxSpinBox,
                                   /* max */ kMaxSpinBox,
                                   /* step */ 1,
                                   &is_ok);

  /* Check if we received something */
  if (not is_ok) {
    return;
  }

  /* Update the value */
  if (under_endurance) {
    this->player.statistics.RemoveEndurance(value);
    this->SetEndurance();
  } else {
    this->player.statistics.RemoveHope(value);
    this->SetHope();
  }
}

void
PlayerSummaryCombatUi::mousePressEvent(QMouseEvent* event)
{
  QFrame::mousePressEvent(event);

  const bool under_endurance = this->endurance->underMouse();
  const bool under_hope = this->hope->underMouse();
  if (event->button() != Qt::LeftButton ||
      (not under_endurance && not under_hope)) {
    return;
  }

  /* Quickly deal with status */
  this->ClickedOnBar(under_endurance);
};

void
PlayerSummaryCombatUi::SetHope()
{
  const int max_hope = this->player.statistics.attributes.max_hope;
  const int current_hope = this->player.statistics.attributes.current_hope;
  const int total_shadow = this->player.statistics.GetTotalShadow();

  this->hope->setMaximum(max_hope);
  this->hope->setValue(current_hope);
  QString format = "%v / " + QString::number(max_hope);
  format += tr(" (Shadow ") + QString::number(total_shadow) + ")";
  this->hope->setFormat(format);

  /* Avoid issue with 0/0 */
  if (max_hope == 0) {
    return;
  }

  /* Set style */
  if (this->player.IsMiserable()) {
    this->hope->setStyleSheet(kProgressBar + kProgressBarRed);
  } else {
    this->hope->setStyleSheet(kProgressBar + kProgressBarBlue);
  }
};

void
PlayerSummaryCombatUi::SetEndurance()
{
  const int max_endurance = this->player.statistics.attributes.max_endurance;
  const int current_endurance =
    this->player.statistics.attributes.current_endurance;
  const int total_load = this->player.GetCurrentLoadAndFatigue(
    /* wear_all */ false, /* use_temporary */ true);

  this->endurance->setMaximum(max_endurance);
  this->endurance->setValue(current_endurance);
  QString format = "%v / " + QString::number(max_endurance);
  format += tr(" (Load ") + QString::number(total_load) + ")";
  this->endurance->setFormat(format);

  /* Avoid issue with 0/0 */
  if (max_endurance == 0) {
    return;
  }

  /* Set style */
  if (this->player.IsWeary()) {
    this->endurance->setStyleSheet(kProgressBar + kProgressBarRed);
  } else {
    this->endurance->setStyleSheet(kProgressBar + kProgressBarGreen);
  }
};

void
PlayerSummaryCombatUi::UpdateShield()
{
  this->player.equipment.WearShield(this->shield->isChecked());
  this->parry->setText(QString::number(this->player.GetTotalParry()));
  this->SetEndurance();
};

void
PlayerSummaryCombatUi::UpdateHelm()
{
  this->player.equipment.WearHelm(this->helm->isChecked());
  this->SetEndurance();
};

void
PlayerSummaryCombatUi::UpdateWound()
{
  const bool is_wounded = this->wounded->isChecked();
  this->player.statistics.wounded = is_wounded;

  /* Get some information about the injury */
  if (is_wounded) {
    bool is_ok = false;
    std::string injury = QInputDialog::getText(&this->parent,
                                               tr("Injury"),
                                               tr("Injury description"),
                                               QLineEdit::Normal,
                                               "",
                                               &is_ok)
                           .toStdString();

    /* Check if we received something */
    if (not is_ok) {
      return;
    }
    this->player.statistics.injury = injury;
  } else {
    this->player.statistics.injury = "";
  }
};
