/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_statistics.hpp"
#include "../utils.hpp"

#include <iostream>

PlayerStatistics::PlayerStatistics()
{
  std::fill(
    this->proficiencies.skills.begin(), this->proficiencies.skills.end(), 0);
  std::fill(this->proficiencies.skills_favorite.begin(),
            this->proficiencies.skills_favorite.end(),
            0);
  std::fill(
    this->proficiencies.combat.begin(), this->proficiencies.combat.end(), 0);
}

void
PlayerStatistics::RemoveEndurance(int lost)
{
  this->attributes.current_endurance =
    std::max(0, this->attributes.current_endurance - lost);
  this->attributes.current_endurance = std::min(
    this->attributes.max_endurance, this->attributes.current_endurance);
}

void
PlayerStatistics::RemoveHope(int lost)
{
  this->attributes.current_hope =
    std::max(0, this->attributes.current_hope - lost);
  this->attributes.current_hope =
    std::min(this->attributes.max_hope, this->attributes.current_hope);
}

void
PlayerStatistics::AddToJson(QJsonObject& object) const
{
  object["strength"] = this->attributes.strength;
  object["endurance"] = this->attributes.max_endurance;
  object["endurance_current"] = this->attributes.current_endurance;
  object["heart"] = this->attributes.heart;
  object["hope"] = this->attributes.max_hope;
  object["hope_current"] = this->attributes.current_hope;
  object["wits"] = this->attributes.wits;
  object["parry"] = this->attributes.parry;

  QJsonArray array = ArrayToJson(this->proficiencies.skills);
  object["skills"] = array;

  array = ArrayToJson(this->proficiencies.skills_favorite);
  object["skills_favorite"] = array;

  array = ArrayToJson(this->proficiencies.combat);
  object["combat_skills"] = array;

  object["valour"] = this->heroic.valour;
  object["rewards"] = QString::fromStdString(this->heroic.rewards);
  object["wisdom"] = this->heroic.wisdom;
  object["virtues"] = QString::fromStdString(this->heroic.virtues);
  object["shadow"] = this->heroic.shadow;
  object["shadow_scars"] = this->heroic.shadow_scars;

  object["fatigue"] = this->fatigue;
  object["wounded"] = this->wounded;
  object["injury"] = QString::fromStdString(this->injury);
  object["current_skill_points"] = this->experience.current_skill;
  object["total_skill_points"] = this->experience.total_skill;
  object["current_adventure_points"] = this->experience.current_adventure;
  object["total_adventure_points"] = this->experience.total_adventure;
}

void
PlayerStatistics::RestoreFromJson(const QJsonObject& object)
{
  this->attributes.strength = object["strength"].toInt();
  this->attributes.max_endurance = object["endurance"].toInt();
  this->attributes.current_endurance = object["endurance_current"].toInt();
  this->attributes.heart = object["heart"].toInt();
  this->attributes.max_hope = object["hope"].toInt();
  this->attributes.current_hope = object["hope_current"].toInt();
  this->attributes.wits = object["wits"].toInt();
  this->attributes.parry = object["parry"].toInt();

  JsonToIntArray(this->proficiencies.skills, object["skills"]);
  JsonToBoolArray(this->proficiencies.skills_favorite,
                  object["skills_favorite"]);
  JsonToIntArray(this->proficiencies.combat, object["combat_skills"]);

  this->heroic.valour = object["valour"].toInt();
  this->heroic.rewards = object["rewards"].toString().toStdString();
  this->heroic.wisdom = object["wisdom"].toInt();
  this->heroic.virtues = object["virtues"].toString().toStdString();
  this->heroic.shadow = object["shadow"].toInt();
  this->heroic.shadow_scars = object["shadow_scars"].toInt();

  this->fatigue = object["fatigue"].toInt();
  this->wounded = object["wounded"].toBool();
  this->injury = object["injury"].toString().toStdString();
  if (object.contains("current_adventure_points")) {
    this->experience.current_skill = object["current_skill_points"].toInt();
    this->experience.total_skill = object["total_skill_points"].toInt();
    this->experience.current_adventure =
      object["current_adventure_points"].toInt();
    this->experience.total_adventure = object["total_adventure_points"].toInt();
  }
}

void
// NOLINTNEXTLINE
PlayerStatistics::AddExperience(int skill, int adventure, bool add_wits)
{
  const int total_skill = add_wits ? skill + this->attributes.wits : skill;

  this->experience.current_skill += total_skill;
  this->experience.total_skill += total_skill;
  this->experience.current_adventure += adventure;
  this->experience.total_adventure += adventure;
}

void
// NOLINTNEXTLINE
PlayerStatistics::SpendExperience(int skill, int adventure)
{
  if (skill > this->experience.current_skill ||
      adventure > this->experience.current_adventure) {
    throw std::logic_error("Spending more experience than available");
  }

  this->experience.current_skill -= skill;
  this->experience.current_adventure -= adventure;
}

void
PlayerStatistics::UpdateCombatProficiencies(std::list<int> new_values)
{
  if (new_values.size() != kCombatProficienciesCount) {
    throw std::logic_error(
      "Combat proficiencies list does not have the correct size");
  }
  auto new_value = new_values.begin();
  for (auto& element : this->proficiencies.combat) {
    element = *new_value;
    std::advance(new_value, 1);
  }
}

void
PlayerStatistics::UpdateSkills(std::list<int> new_values)
{
  if (new_values.size() != kSkillsCount) {
    throw std::logic_error("Combat skills list does not have the correct size");
  }
  auto new_value = new_values.begin();
  for (auto& element : this->proficiencies.skills) {
    element = *new_value;
    std::advance(new_value, 1);
  }
}
