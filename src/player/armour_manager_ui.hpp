/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_ARMOUR_UI_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_ARMOUR_UI_HPP

/* Own includes */
#include "equipment_table_manager_ui.hpp"

/* Other includes */
#include <QComboBox>

enum ArmourColumn
{
  kArmourColumnName = 0,
  kArmourColumnWearing,
  kArmourColumnProtection,
  kArmourColumnParry,
  kArmourColumnLoad,
  kArmourColumnType,
  kArmourColumnQualities,
  kArmourColumnUnlockedQualities,
  kArmourColumnNotes,
  kArmourColumnDelete,
  kArmourColumnCount,
};

static const std::array<QString, kArmourColumnCount> kArmourColumnString = {
  QObject::tr("Name"),       QObject::tr("Wearing"),
  QObject::tr("Protection"), QObject::tr("Parry"),
  QObject::tr("Load"),       QObject::tr("Type"),
  QObject::tr("Qualities"),  QObject::tr("Unlocked Qualities"),
  QObject::tr("Notes"),      QObject::tr("Delete")
};

class ArmourTableManager : public EquipmentTableManager
{
  // NOLINTNEXTLINE
  Q_OBJECT

protected:
  void SetHeaders() override
  {
    QStringList labels;
    for (int i = 0; i < kArmourColumnCount; i++) {
      labels.append(kArmourColumnString[i]);
    }
    m_table.setHorizontalHeaderLabels(labels);
  };

  void AddItem(int row, const std::shared_ptr<Equipment>& equip) override
  {
    EquipmentTableManager::AddItem(row, equip);

    /* Cast to armour */
    const std::shared_ptr<Armour>& armour =
      std::dynamic_pointer_cast<Armour>(equip);
    if (armour == nullptr) {
      throw std::logic_error("Trying to cast a non armour object");
    }

    /* Protection */
    auto* protection = new QSpinBox();
    protection->setValue(armour->protection);
    m_table.setCellWidget(row, kArmourColumnProtection, protection);
    QObject::connect(protection, &QSpinBox::valueChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Parry */
    auto* parry = new QSpinBox();
    parry->setValue(armour->parry_modifier);
    m_table.setCellWidget(row, kArmourColumnParry, parry);
    QObject::connect(
      parry, &QSpinBox::valueChanged, [=]() { emit UpdatedItem(row, equip); });

    /* Type */
    auto* type = new QComboBox();
    for (int i = 0; i < kArmourTypeCount; i++) {
      type->addItem(kArmourTypeString[i]);
    }

    /* Set the type */
    if (armour->IsShield()) {
      type->setCurrentIndex(kArmourTypeShield);
    } else if (armour->IsHelm()) {
      type->setCurrentIndex(kArmourTypeHelm);
    } else {
      type->setCurrentIndex(kArmourTypeArmour);
    }

    m_table.setCellWidget(row, kArmourColumnType, type);
    QObject::connect(type, &QComboBox::currentIndexChanged, [=]() {
      emit ChangeType(row, armour);
    });
  }

  [[nodiscard]] int GetNumberColumns() const override
  {
    return kArmourColumnCount;
  }

  [[nodiscard]] int ConvertColumn(EquipmentColumn col) const override
  {
    switch (col) {
      case kEquipmentColumnName:
        return kArmourColumnName;
      case kEquipmentColumnWearing:
        return kArmourColumnWearing;
      case kEquipmentColumnLoad:
        return kArmourColumnLoad;
      case kEquipmentColumnQualities:
        return kArmourColumnQualities;
      case kEquipmentColumnUnlockedQualities:
        return kArmourColumnUnlockedQualities;
      case kEquipmentColumnNotes:
        return kArmourColumnNotes;
      case kEquipmentColumnDelete:
        return kArmourColumnDelete;
      default:
        throw std::logic_error("Unknown column");
    }
  }

signals:
  void ChangeType(int row, const std::shared_ptr<Armour>& armour);

public:
  explicit ArmourTableManager(QTableWidget& table)
    : EquipmentTableManager(table, false)
  {
    Init();

    /* Set the column size */
    m_table.horizontalHeader()->setSectionResizeMode(kArmourColumnProtection,
                                                     QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(kArmourColumnParry,
                                                     QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(kArmourColumnType,
                                                     QHeaderView::Interactive);
  };

  [[nodiscard]] bool UseEquipment(
    const std::shared_ptr<Equipment>& equip) const override
  {
    return equip->IsArmour() || equip->IsShield() || equip->IsHelm();
  }

  void SaveItem(int row, const std::shared_ptr<Equipment>& equip) const override
  {
    EquipmentTableManager::SaveItem(row, equip);

    /* Cast to armour */
    auto* armour = dynamic_cast<Armour*>(equip.get());
    if (armour == nullptr) {
      throw std::logic_error("Trying to cast a non armour object");
    }

    /* Protection */
    auto* protection =
      dynamic_cast<QSpinBox*>(m_table.cellWidget(row, kArmourColumnProtection));
    armour->protection = protection->value();

    /* Parry */
    auto* parry =
      dynamic_cast<QSpinBox*>(m_table.cellWidget(row, kArmourColumnParry));
    armour->parry_modifier = parry->value();
  }

  [[nodiscard]] ArmourType GetType(int row) const
  {
    auto* type =
      dynamic_cast<QComboBox*>(m_table.cellWidget(row, kArmourColumnType));
    return static_cast<ArmourType>(type->currentIndex());
  };
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_ARMOUR_UI_HPP
