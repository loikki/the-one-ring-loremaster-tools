/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_EQUIPMENT_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_EQUIPMENT_HPP

/* Own includes */
#include "../enums.hpp"

/* Other libraries */
#include <QJsonObject>

class Equipment
{
public:
  [[nodiscard]] virtual QJsonObject GetJson() const;
  virtual void RestoreFromJson(const QJsonObject& object);
  [[nodiscard]] virtual int GetParry() const { return 0; }
  [[nodiscard]] virtual int GetProtection() const { return 0; }
  [[nodiscard]] virtual int GetDamage() const { return 0; }
  [[nodiscard]] virtual int GetOneHandInjury() const { return 0; }
  [[nodiscard]] virtual int GetTwoHandInjury() const { return 0; }
  [[nodiscard]] virtual CombatProficiencies GetProficiency() const
  {
    return kCombatProficienciesCount;
  }
  [[nodiscard]] int GetLoad(bool wear_all) const
  {
    return (wear_all || wearing) ? load : 0;
  }
  [[nodiscard]] virtual bool IsWearingHelm() const { return false; }
  [[nodiscard]] virtual bool IsWearingShield() const { return false; }
  virtual void WearHelm(bool /* unused */){};
  virtual void WearShield(bool /* unused */){};
  [[nodiscard]] virtual bool IsWeapon() const { return false; }
  [[nodiscard]] virtual bool IsArmour() const { return false; }
  [[nodiscard]] virtual bool IsShield() const { return false; }
  [[nodiscard]] virtual bool IsHelm() const { return false; }
  virtual void SetProtection(int /* unused */) {}
  virtual void SetParry(int /* unused */) {}
  virtual void SetDamage(int /* unused */) {}
  virtual void SetOneHandInjury(int /* unused */) {}
  virtual void SetTwoHandInjury(int /* unused */) {}
  virtual void SetProficiency(enum CombatProficiencies /* unused */) {}

  std::string name{ "New Item" };
  int load{ 0 };
  std::string notes;
  bool wearing{ true };
  std::vector<std::string> qualities;
  int unlocked_qualities{ 0 };

  /* Default */
  virtual ~Equipment() = default;
  Equipment() = default;
  Equipment(const Equipment&) = default;
  Equipment& operator=(const Equipment& other) = default;
  Equipment(Equipment&&) = default;
  Equipment& operator=(Equipment&&) = default;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_EQUIPMENT_HPP
