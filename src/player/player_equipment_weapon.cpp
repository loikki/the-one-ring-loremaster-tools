/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_equipment_weapon.hpp"
#include "../utils.hpp"

QJsonObject
Weapon::GetJson() const
{
  QJsonObject object = Equipment::GetJson();
  object["type"] = kEquipmentTypeWeapon;
  object["damage"] = this->damage;
  object["one_hand_injury"] = this->one_hand_injury;
  object["two_hand_injury"] = this->two_hand_injury;
  object["proficiency"] = this->proficiency;

  return object;
}

void
Weapon::RestoreFromJson(const QJsonObject& object)
{
  Equipment::RestoreFromJson(object);
  this->damage = object["damage"].toInt();
  this->one_hand_injury = object["one_hand_injury"].toInt();
  this->two_hand_injury = object["two_hand_injury"].toInt();
  this->proficiency =
    static_cast<CombatProficiencies>(object["proficiency"].toInt());
}
