/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_equipment_equipment.hpp"
#include "../utils.hpp"

QJsonObject
Equipment::GetJson() const
{
  QJsonObject object;
  object["type"] = kEquipmentTypeEquipment;
  object["name"] = QString::fromStdString(this->name);
  object["load"] = this->load;
  object["notes"] = QString::fromStdString(this->notes);
  object["wearing"] = this->wearing;

  QJsonArray qual;
  for (const auto& element : this->qualities) {
    qual.append(QString::fromStdString(element));
  }
  object["qualities"] = qual;
  object["unlocked_qualities"] = this->unlocked_qualities;

  return object;
}

void
Equipment::RestoreFromJson(const QJsonObject& object)
{
  this->name = object["name"].toString().toStdString();
  this->load = object["load"].toInt();
  this->notes = object["notes"].toString().toStdString();
  this->wearing = object["wearing"].toBool();

  this->qualities.clear();
  for (const auto& element : object["qualities"].toArray()) {
    this->qualities.push_back(element.toString().toStdString());
  }
  this->unlocked_qualities = object["unlocked_qualities"].toInt();
}
