/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_HPP

/* Own headers */
#include "../enums.hpp"
#include "player_equipment.hpp"
#include "player_statistics.hpp"

/* Other libraries */
#include <QImage>
#include <QJsonObject>

class Player
{

public:
  [[nodiscard]] bool IsEmpty() const { return this->name.length() == 0; }

  [[nodiscard]] int GetTotalParry() const
  {
    return this->statistics.GetParry() + this->equipment.GetParry();
  }

  [[nodiscard]] int GetTotalProtection() const
  {
    return this->equipment.GetProtection();
  }

  [[nodiscard]] std::string GetFilename(const std::string& directory) const;

  void DeleteFile(const std::string& directory) const;

  [[nodiscard]] QJsonObject GetJson() const;

  void RestoreFromJson(const QJsonObject& object);

  void ShortRest();

  void LongRest();

  void SaveToFile(const std::string& directory) const;

  void ReadFromFile(const std::string& filename);

  [[nodiscard]] enum LivingStandard GetStandardOfLiving() const;

  [[nodiscard]] int GetCurrentLoadAndFatigue(bool wear_all = false,
                                             bool use_temporary = false) const
  {
    return this->statistics.fatigue +
           this->equipment.GetCurrentLoad(wear_all, use_temporary);
  }

  [[nodiscard]] bool IsMiserable() const
  {
    return this->statistics.IsMiserable();
  }

  [[nodiscard]] bool IsWeary() const
  {
    const int load = this->GetCurrentLoadAndFatigue();
    return load >= this->statistics.attributes.current_endurance;
  }

  /* Attributes */
  std::string name;
  std::string player_name;
  std::string features;
  std::string culture;
  std::string calling;
  std::string shadow_path;
  JourneyRole journey_role{ kJourneyRoleGuide };
  QImage image;

  PlayerStatistics statistics;
  PlayerEquipment equipment;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_HPP
