/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_SUMMARY_COMBAT_PLAYER_UI_HPP
#define LOREMASTER_TOOLS_SRC_SUMMARY_COMBAT_PLAYER_UI_HPP

/* Own includes */
#include "../gui/ui_player_summary_combat.h"
#include "player.hpp"

/* Other includes */
#include <QFrame>
#include <QInputDialog>
#include <QMouseEvent>

/* Forward declaration */
class Player;

class PlayerSummaryCombatUi
  : public QFrame
  , public Ui_PlayerSummaryCombat
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  PlayerSummaryCombatUi(QWidget& parent, Player& player);
  void UpdateUi();

protected:
  const int kMaxSpinBox = 1000;

  void ConnectSignalSlots();
  void ClickedOnBar(const bool under_endurance);
  void mousePressEvent(QMouseEvent* event) override;
  void SetHope();
  void SetEndurance();
  void UpdateShield();
  void UpdateHelm();
  void UpdateWound();

  /* Attributes */
  Player& player;
  QWidget& parent;
};

#endif // LOREMASTER_TOOLS_SRC_SUMMARY_COMBAT_PLAYER_UI_HPP
