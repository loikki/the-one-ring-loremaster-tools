/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_SUMMARY_PLAYER_UI_HPP
#define LOREMASTER_TOOLS_SRC_SUMMARY_PLAYER_UI_HPP

/* Own includes */
#include "../gui/ui_player_summary.h"
#include "player.hpp"

/* Other includes */
#include <QFrame>
#include <QMouseEvent>

class PlayerSummaryUi
  : public QFrame
  , public Ui_PlayerSummary
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  PlayerSummaryUi(QWidget& parent, Player& player);

  void UpdateUi();

protected:
  void SetStatus();

  void UpdateJourneyRole()
  {
    this->player.journey_role =
      static_cast<JourneyRole>(this->travel_role->currentIndex());
  }

  void UpdateShadow()
  {
    this->player.statistics.heroic.shadow = this->shadow->value();
    this->SetHope();
  }

  void UpdateShadowScars()
  {
    this->player.statistics.heroic.shadow_scars = this->shadow_scars->value();
    this->SetHope();
  }

  void UpdateFatigue()
  {
    this->player.statistics.fatigue = this->fatigue->value();
    this->SetEndurance();
  }

  void UpdateTemporaryLoad()
  {
    this->player.equipment.temporary_load = this->temporary_load->value();
    this->SetEndurance();
  }

  void ConnectSignalSlots();

  void ClickedOnBar(const bool under_endurance);

  void UpdateWound();

  void mousePressEvent(QMouseEvent* event) override;

  void SetHope();

  void SetEndurance();

  /* Attributes */
  const int kMaxSpinBox = 1000;
  QWidget& parent;
  Player& player;
};

#endif // LOREMASTER_TOOLS_SRC_SUMMARY_PLAYER_UI_HPP
