/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_UI_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_UI_HPP

/* Own include */
#include "../gui/ui_player.h"
#include "armour_manager_ui.hpp"
#include "equipment_table_manager_ui.hpp"
#include "player.hpp"
#include "weapon_manager_ui.hpp"

/* Standard library */
#include <vector>

/* Forward declaration */
class Fellowship;

class PlayerUi
  : public QTabWidget
  , public Ui_player
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  PlayerUi& operator=(PlayerUi&&) = delete;
  PlayerUi(PlayerUi&&) = delete;
  PlayerUi(const PlayerUi&) = delete;
  PlayerUi& operator=(const PlayerUi&) = delete;
  PlayerUi(QWidget& parent,
           std::string new_directory,
           std::shared_ptr<Fellowship> fellowship,
           bool hide_image,
           bool hide_skills,
           bool lock_player);
  ~PlayerUi() override;
  void SaveCurrentPlayer();
  void HideSkills(bool hide);
  void HideImage(bool hide);
  void SaveToPlayer();
  void UpdateUi();
  void LockPlayer(bool lock);
  [[nodiscard]] std::string GetDirectory() const;
  void UpdateCurrentPlayer(int index);
  void SetDirectory(std::string new_directory)
  {
    this->directory = std::move(new_directory);
  }

protected:
  void ConnectSignalSlots();
  bool eventFilter(QObject* obj, QEvent* event) override;

  void LoadImage();
  void SaveSkillsToPlayer();
  void NewPlayer();
  void RemoveCurrentPlayer();
  void LoadPlayer(std::string filename = "");
  void UpdateSkillsInUi();
  void UpdateProtectionUi()
  {
    this->total_prot->setText(
      QString::number(this->player->GetTotalProtection()));
  }
  void UpdateStandardOfLivingUi()
  {
    this->player->equipment.treasure.total = this->total_treasure->value();
    this->living->setText(
      // NOLINTNEXTLINE
      kLivingStandardString[this->player->GetStandardOfLiving()]);
  }

  void UpdateEquipmentUi()
  {
    if (m_equipment_table_manager != nullptr) {
      m_equipment_table_manager->Update(this->player->equipment.equipment);
    }
    if (m_armour_table_manager != nullptr) {
      m_armour_table_manager->Update(this->player->equipment.equipment);
    }
    if (m_weapon_table_manager != nullptr) {
      m_weapon_table_manager->Update(this->player->equipment.equipment);
    }
  };

  void UpdateLoadUi()
  {
    this->total_load->setText(
      QString::number(this->player->equipment.GetCurrentLoad(
        /* wear_all */ true, /* use_temporary */ false)));
  };

  /* Attributes */
  QWidget& parent;
  Player* player{ nullptr };
  std::shared_ptr<Fellowship> fellowship{ nullptr };
  std::string directory;
  std::unique_ptr<EquipmentTableManager> m_equipment_table_manager{ nullptr };
  std::unique_ptr<WeaponTableManager> m_weapon_table_manager{ nullptr };
  std::unique_ptr<ArmourTableManager> m_armour_table_manager{ nullptr };

  /* Only test classes can be friends */
  friend class TestMainWindow;

signals:
  void UpdateStatusBar(QString text);
  void lockChanged(bool lock);
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_UI_HPP
