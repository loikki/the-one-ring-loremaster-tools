/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_WEAPON_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_WEAPON_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment_equipment.hpp"

/* Other libraries */
#include <QJsonObject>

class Weapon : public Equipment
{
public:
  [[nodiscard]] QJsonObject GetJson() const override;
  void RestoreFromJson(const QJsonObject& object) override;
  [[nodiscard]] bool IsWeapon() const override { return true; }
  [[nodiscard]] int GetDamage() const override { return damage; }
  [[nodiscard]] int GetOneHandInjury() const override
  {
    return one_hand_injury;
  }
  [[nodiscard]] int GetTwoHandInjury() const override
  {
    return two_hand_injury;
  }
  [[nodiscard]] CombatProficiencies GetProficiency() const override
  {
    return proficiency;
  }
  void SetDamage(int value) override { damage = value; }
  void SetOneHandInjury(int value) override { one_hand_injury = value; }
  void SetTwoHandInjury(int value) override { two_hand_injury = value; }
  void SetProficiency(enum CombatProficiencies value) override
  {
    proficiency = value;
  }

  int damage;
  int one_hand_injury;
  int two_hand_injury;
  enum CombatProficiencies proficiency;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_WEAPON_HPP
