/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment_armour.hpp"
#include "player_equipment_equipment.hpp"
#include "player_equipment_helm.hpp"
#include "player_equipment_shield.hpp"
#include "player_equipment_weapon.hpp"

/* Other libraries */
#include <QJsonObject>

struct Treasure
{
  int current;
  int total;
};

class PlayerEquipment
{
public:
  PlayerEquipment() = default;

  [[nodiscard]] int GetParry() const
  {
    int parry = 0;
    for (const std::shared_ptr<Equipment>& equip : this->equipment) {
      parry += equip->GetParry();
    }
    return parry;
  }

  [[nodiscard]] int GetProtection() const
  {
    int protection = 0;
    for (const std::shared_ptr<Equipment>& equip : this->equipment) {
      protection += equip->GetProtection();
    }
    return protection;
  }

  [[nodiscard]] bool IsWearingShield() const
  {
    return std::any_of(equipment.cbegin(),
                       equipment.cend(),
                       [](const std::shared_ptr<Equipment>& equip) {
                         return equip->IsWearingShield();
                       });
  }

  [[nodiscard]] bool IsWearingHelm() const
  {
    return std::any_of(equipment.cbegin(),
                       equipment.cend(),
                       [](const std::shared_ptr<Equipment>& equip) {
                         return equip->IsWearingHelm();
                       });
  }

  void WearHelm(bool wear)
  {
    for (std::shared_ptr<Equipment>& equip : this->equipment) {
      equip->WearHelm(wear);
    }
  }

  void WearShield(bool wear)
  {
    for (std::shared_ptr<Equipment>& equip : this->equipment) {
      equip->WearShield(wear);
    }
  }

  [[nodiscard]] int GetCurrentLoad(bool wear_all = false,
                                   bool use_temporary = true) const
  {
    int load = 0;
    for (const std::shared_ptr<Equipment>& equip : this->equipment) {
      load += equip->GetLoad(wear_all);
    }
    load += this->treasure.current;
    load += use_temporary ? this->temporary_load : 0;
    return load;
  }

  void AddToJson(QJsonObject& object) const;

  void RestoreFromJson(const QJsonObject& object);

  std::vector<std::shared_ptr<Equipment>> equipment;
  Treasure treasure{ .current = 0, .total = 0 };

  int temporary_load{ 0 };
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HPP
