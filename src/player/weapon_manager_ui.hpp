/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_WEAPON_UI_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_WEAPON_UI_HPP

/* Own includes */
#include "equipment_table_manager_ui.hpp"

/* Other includes */
#include <QComboBox>

enum WeaponColumn
{
  kWeaponColumnName = 0,
  kWeaponColumnWearing,
  kWeaponColumnDamage,
  kWeaponColumnInjuryOneHand,
  kWeaponColumnInjuryTwoHands,
  kWeaponColumnLoad,
  kWeaponColumnProficiency,
  kWeaponColumnQualities,
  kWeaponColumnUnlockedQualities,
  kWeaponColumnNotes,
  kWeaponColumnDelete,
  kWeaponColumnCount,
};

static const std::array<QString, kWeaponColumnCount> kWeaponColumnString = {
  QObject::tr("Name"),
  QObject::tr("Wearing"),
  QObject::tr("Damage"),
  QObject::tr("One Hand Injury"),
  QObject::tr("Two Hands Injury"),
  QObject::tr("Load"),
  QObject::tr("Proficiency"),
  QObject::tr("Qualities"),
  QObject::tr("Unlocked Qualities"),
  QObject::tr("Notes"),
  QObject::tr("Delete")
};

class WeaponTableManager : public EquipmentTableManager
{
protected:
  void SetHeaders() override
  {
    QStringList labels;
    for (int i = 0; i < kWeaponColumnCount; i++) {
      labels.append(kWeaponColumnString[i]);
    }
    m_table.setHorizontalHeaderLabels(labels);
  };

  void AddItem(int row, const std::shared_ptr<Equipment>& equip) override
  {
    EquipmentTableManager::AddItem(row, equip);

    /* Convert */
    auto* weapon = dynamic_cast<Weapon*>(equip.get());
    if (weapon == nullptr) {
      throw std::logic_error("Trying to cast a non weapon object");
    }

    /* Damage */
    auto* damage = new QSpinBox();
    damage->setValue(weapon->damage);
    m_table.setCellWidget(row, kWeaponColumnDamage, damage);
    QObject::connect(
      damage, &QSpinBox::valueChanged, [=]() { emit UpdatedItem(row, equip); });

    /* Injury */
    auto* one_hand = new QSpinBox();
    one_hand->setValue(weapon->one_hand_injury);
    m_table.setCellWidget(row, kWeaponColumnInjuryOneHand, one_hand);
    QObject::connect(one_hand, &QSpinBox::valueChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    auto* two_hands = new QSpinBox();
    two_hands->setValue(weapon->two_hand_injury);
    m_table.setCellWidget(row, kWeaponColumnInjuryTwoHands, two_hands);
    QObject::connect(two_hands, &QSpinBox::valueChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Proficiency */
    auto* proficiencies = new QComboBox();
    for (int i = 0; i < kCombatProficienciesCount; i++) {
      proficiencies->addItem(
        QString::fromStdString(kCombatProficienciesString[i]));
    }
    proficiencies->setCurrentIndex(weapon->proficiency);
    m_table.setCellWidget(row, kWeaponColumnProficiency, proficiencies);
    QObject::connect(proficiencies, &QComboBox::currentIndexChanged, [=]() {
      emit UpdatedItem(row, equip);
    });
  }

  [[nodiscard]] int GetNumberColumns() const override
  {
    return kWeaponColumnCount;
  }

  [[nodiscard]] int ConvertColumn(EquipmentColumn col) const override
  {
    switch (col) {
      case kEquipmentColumnName:
        return kWeaponColumnName;
      case kEquipmentColumnWearing:
        return kWeaponColumnWearing;
      case kEquipmentColumnLoad:
        return kWeaponColumnLoad;
      case kEquipmentColumnQualities:
        return kWeaponColumnQualities;
      case kEquipmentColumnUnlockedQualities:
        return kWeaponColumnUnlockedQualities;
      case kEquipmentColumnNotes:
        return kWeaponColumnNotes;
      case kEquipmentColumnDelete:
        return kWeaponColumnDelete;
      default:
        throw std::logic_error("Unknown column");
    }
  }

public:
  explicit WeaponTableManager(QTableWidget& table)
    : EquipmentTableManager(table, false)
  {
    Init();

    /* Set size */
    m_table.horizontalHeader()->setSectionResizeMode(kWeaponColumnDamage,
                                                     QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(kWeaponColumnInjuryOneHand,
                                                     QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(
      kWeaponColumnInjuryTwoHands, QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(kWeaponColumnProficiency,
                                                     QHeaderView::Interactive);
  };

  [[nodiscard]] bool UseEquipment(
    const std::shared_ptr<Equipment>& equip) const override
  {
    return equip->IsWeapon();
  }

  void SaveItem(int row, const std::shared_ptr<Equipment>& equip) const override
  {
    EquipmentTableManager::SaveItem(row, equip);

    /* Convert */
    auto* weapon = dynamic_cast<Weapon*>(equip.get());
    if (weapon == nullptr) {
      throw std::logic_error("Trying to cast a non weapon object");
    }

    /* Damage */
    auto* damage =
      dynamic_cast<QSpinBox*>(m_table.cellWidget(row, kWeaponColumnDamage));
    weapon->damage = damage->value();

    /* Injury */
    auto* one_hand = dynamic_cast<QSpinBox*>(
      m_table.cellWidget(row, kWeaponColumnInjuryOneHand));
    weapon->one_hand_injury = one_hand->value();

    auto* two_hands = dynamic_cast<QSpinBox*>(
      m_table.cellWidget(row, kWeaponColumnInjuryTwoHands));
    weapon->two_hand_injury = two_hands->value();

    /* Proficiency */
    auto* proficiencies = dynamic_cast<QComboBox*>(
      m_table.cellWidget(row, kWeaponColumnProficiency));
    weapon->proficiency =
      static_cast<CombatProficiencies>(proficiencies->currentIndex());
  }
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_WEAPON_UI_HPP
