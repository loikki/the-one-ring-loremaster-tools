/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player.hpp"
#include "../utils.hpp"

/* Other includes */
#include <QFile>

std::string
Player::GetFilename(const std::string& directory) const
{
  std::string output(directory);
  output += "/";
  output += this->name;
  output += ".pl";
  return output;
}

void
Player::DeleteFile(const std::string& directory) const
{
  std::string filename = this->GetFilename(directory);
  QFile file(QString::fromStdString(filename));
  if (!file.remove()) {
    throw std::ios_base::failure("Failed to remove file '" + filename + "'");
  }
}

QJsonObject
Player::GetJson() const
{
  QJsonObject object;
  object["name"] = QString::fromStdString(this->name);
  object["player_name"] = QString::fromStdString(this->player_name);
  object["features"] = QString::fromStdString(this->features);
  object["culture"] = QString::fromStdString(this->culture);
  object["calling"] = QString::fromStdString(this->calling);
  object["shadow_path"] = QString::fromStdString(this->shadow_path);
  object["journey_role"] = this->journey_role;
  object["image"] = JsonValFromImage(this->image);
  object["is_miserable"] = this->IsMiserable();
  object["is_weary"] = this->IsWeary();
  object["total_protection"] = this->GetTotalProtection();
  object["total_parry"] = this->GetTotalParry();
  object["standard_of_living"] =
    kLivingStandardString[this->GetStandardOfLiving()];
  object["total_load_fatigue"] = this->GetCurrentLoadAndFatigue(
    /*wear_all*/ false, /* use_temporary */ true);

  this->statistics.AddToJson(object);
  this->equipment.AddToJson(object);

  return object;
}

void
Player::RestoreFromJson(const QJsonObject& object)
{
  this->name = object["name"].toString().toStdString();
  this->player_name = object["player_name"].toString().toStdString();
  this->features = object["features"].toString().toStdString();
  this->culture = object["culture"].toString().toStdString();
  this->calling = object["calling"].toString().toStdString();
  this->shadow_path = object["shadow_path"].toString().toStdString();
  this->journey_role = static_cast<JourneyRole>(object["journey_role"].toInt());
  this->image = ImageFromJsonVal(object["image"]);

  this->statistics.RestoreFromJson(object);
  this->equipment.RestoreFromJson(object);
}

void
Player::ShortRest()
{
  if (not this->statistics.wounded) {
    this->statistics.RemoveEndurance(-this->statistics.attributes.strength);
  }
}

void
Player::LongRest()
{
  if (this->statistics.wounded) {
    this->statistics.RemoveEndurance(
      -this->statistics.attributes.max_endurance);
  } else {
    this->statistics.RemoveEndurance(-this->statistics.attributes.strength);
  }
}

void
Player::SaveToFile(const std::string& directory) const
{
  /* Ensure that the output directory exists */
  if (not std::filesystem::exists(directory)) {
    std::filesystem::create_directory(directory);
  }

  /* Avoid saving empty players */
  if (this->name.empty()) {
    return;
  }
  std::string filename = this->GetFilename(directory);
  QJsonObject json = this->GetJson();
  WriteJsonToFile(json, filename);
}

void
Player::ReadFromFile(const std::string& filename)
{
  QJsonObject json = ReadJsonObject(filename);
  this->RestoreFromJson(json);
}

enum LivingStandard
Player::GetStandardOfLiving() const
{
  int treasure = this->equipment.treasure.total;
  if (treasure < kLivingStandardLimits[kLivingStandardPoor]) {
    return kLivingStandardPoor;
  } else if (treasure < kLivingStandardLimits[kLivingStandardFrugal]) {
    return kLivingStandardFrugal;
  } else if (treasure < kLivingStandardLimits[kLivingStandardCommon]) {
    return kLivingStandardCommon;
  } else if (treasure < kLivingStandardLimits[kLivingStandardProsperous]) {
    return kLivingStandardProsperous;
  } else if (treasure < kLivingStandardLimits[kLivingStandardRich]) {
    return kLivingStandardRich;
  } else {
    return kLivingStandardVeryRich;
  }
}
