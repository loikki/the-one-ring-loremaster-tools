/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_UI_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_UI_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment.hpp"

/* Standard includes */
#include <iostream>

/* Other includes */
#include <QAbstractTableModel>
#include <QCheckBox>
#include <QHeaderView>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QTableWidget>

constexpr const char* CHAR_SPLIT = "\n";

enum EquipmentColumn
{
  kEquipmentColumnName = 0,
  kEquipmentColumnWearing,
  kEquipmentColumnLoad,
  kEquipmentColumnQualities,
  kEquipmentColumnUnlockedQualities,
  kEquipmentColumnNotes,
  kEquipmentColumnDelete,
  kEquipmentColumnCount,
};

static const std::array<QString, kEquipmentColumnCount>
  kEquipmentColumnString = { QObject::tr("Name"),
                             QObject::tr("Wearing"),
                             QObject::tr("Load"),
                             QObject::tr("Qualities"),
                             QObject::tr("Unlocked Qualities"),
                             QObject::tr("Notes"),
                             QObject::tr("Delete") };

class EquipmentTableManager : public QObject
{
  // NOLINTNEXTLINE
  Q_OBJECT

protected:
  virtual void SetHeaders()
  {
    QStringList labels;
    for (int i = 0; i < kEquipmentColumnCount; i++) {
      labels.append(kEquipmentColumnString[i]);
    }
    m_table.setHorizontalHeaderLabels(labels);
  };

  [[nodiscard]] virtual int ConvertColumn(EquipmentColumn col) const
  {
    return col;
  }

  virtual void AddItem(int row, const std::shared_ptr<Equipment>& equip)
  {
    /* Name */
    auto* name = new QLineEdit(QString::fromStdString(equip->name));
    m_table.setCellWidget(row, ConvertColumn(kEquipmentColumnName), name);
    QObject::connect(
      name, &QLineEdit::textChanged, [=]() { emit UpdatedItem(row, equip); });

    /* Wearing */
    auto* wearing = new QCheckBox();
    wearing->setChecked(equip->wearing);
    m_table.setCellWidget(row, ConvertColumn(kEquipmentColumnWearing), wearing);
    QObject::connect(wearing, &QCheckBox::stateChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Load */
    auto* load = new QSpinBox();
    load->setValue(equip->load);
    m_table.setCellWidget(row, ConvertColumn(kEquipmentColumnLoad), load);
    QObject::connect(
      load, &QSpinBox::valueChanged, [=]() { emit UpdatedItem(row, equip); });

    /* Qualities */
    QStringList list;
    for (const std::string& qual : equip->qualities) {
      list.append(QString::fromStdString(qual));
    }
    QString text = list.join(CHAR_SPLIT);
    auto* qualities = new QPlainTextEdit(text);
    m_table.setCellWidget(
      row, ConvertColumn(kEquipmentColumnQualities), qualities);

    QObject::connect(qualities, &QPlainTextEdit::textChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Unlocked qualities */
    auto* unlocked_qualities = new QSpinBox();
    unlocked_qualities->setValue(equip->unlocked_qualities);
    m_table.setCellWidget(row,
                          ConvertColumn(kEquipmentColumnUnlockedQualities),
                          unlocked_qualities);
    QObject::connect(unlocked_qualities, &QSpinBox::valueChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Notes */
    auto* notes = new QPlainTextEdit(QString::fromStdString(equip->notes));
    m_table.setCellWidget(row, ConvertColumn(kEquipmentColumnNotes), notes);
    QObject::connect(notes, &QPlainTextEdit::textChanged, [=]() {
      emit UpdatedItem(row, equip);
    });

    /* Delete */
    auto* del = new QPushButton("X");
    m_table.setCellWidget(row, ConvertColumn(kEquipmentColumnDelete), del);
    QObject::connect(
      del, &QPushButton::clicked, [=]() { emit RemoveItem(equip); });
  }

public:
  explicit EquipmentTableManager(QTableWidget& table, bool init = true)
    : m_table(table)
  {
    // NOLINTNEXTLINE (manually preventing init in subclass)
    if (init) {
      Init();
    }
  }

  void Init()
  {
    /* Create header */
    m_table.setColumnCount(GetNumberColumns());
    SetHeaders();

    /* Add the new item button */
    m_table.setRowCount(1);
    AddNewItemButton(0);

    /* Stretch columns */
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnName), QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnWearing), QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnLoad), QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnQualities), QHeaderView::Stretch);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnUnlockedQualities),
      QHeaderView::Interactive);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnNotes), QHeaderView::Stretch);
    m_table.horizontalHeader()->setSectionResizeMode(
      ConvertColumn(kEquipmentColumnDelete), QHeaderView::Interactive);
  }

  void AddNewItemButton(int row)
  {
    auto* new_eq = new QPushButton("New Item");
    m_table.setCellWidget(row, 0, new_eq);
    QObject::connect(new_eq, &QPushButton::clicked, [=]() { emit NewItem(); });
  }

  virtual void SaveItem(int row, const std::shared_ptr<Equipment>& equip) const
  {
    /* Name */
    auto* name = dynamic_cast<QLineEdit*>(
      m_table.cellWidget(row, ConvertColumn(kEquipmentColumnName)));
    equip->name = name->text().toStdString();

    /* Wearing */
    auto* wearing = dynamic_cast<QCheckBox*>(
      m_table.cellWidget(row, ConvertColumn(kEquipmentColumnWearing)));
    equip->wearing = wearing->isChecked();

    /* Load */
    auto* load = dynamic_cast<QSpinBox*>(
      m_table.cellWidget(row, ConvertColumn(kEquipmentColumnLoad)));
    equip->load = load->value();

    /* Qualities */
    auto* qualities = dynamic_cast<QPlainTextEdit*>(
      m_table.cellWidget(row, ConvertColumn(kEquipmentColumnQualities)));
    QStringList texts = qualities->toPlainText().split(CHAR_SPLIT);
    equip->qualities.clear();
    for (const QString& current : texts) {
      equip->qualities.push_back(current.toStdString());
    }

    /* Unlocked Qualities */
    auto* unlocked_qualities = dynamic_cast<QSpinBox*>(m_table.cellWidget(
      row, ConvertColumn(kEquipmentColumnUnlockedQualities)));
    equip->unlocked_qualities = unlocked_qualities->value();

    /* Notes */
    auto* notes = dynamic_cast<QPlainTextEdit*>(
      m_table.cellWidget(row, ConvertColumn(kEquipmentColumnNotes)));
    equip->notes = notes->toPlainText().toStdString();
  }

  void SaveTo(std::vector<std::shared_ptr<Equipment>>& equipments) const
  {
    int current_row = 0;
    for (std::shared_ptr<Equipment>& equip : equipments) {
      if (!UseEquipment(equip)) {
        continue;
      }
      SaveItem(current_row, equip);
      current_row++;
    }
  }

  [[nodiscard]] int CountEquipment(
    const std::vector<std::shared_ptr<Equipment>>& equipments) const
  {
    return static_cast<int>(
      std::count_if(equipments.cbegin(),
                    equipments.cend(),
                    [=](const std::shared_ptr<Equipment>& equip) {
                      return UseEquipment(equip);
                    }));
  }

  void Update(const std::vector<std::shared_ptr<Equipment>>& equipments)
  {
    /* Cleanup the table */
    m_table.clearContents();

    /* Set table size */
    const int rows = CountEquipment(equipments);
    m_table.setRowCount(rows + 1);

    /* Create rows */
    int current_row = 0;
    for (const std::shared_ptr<Equipment>& equip : equipments) {
      if (!UseEquipment(equip)) {
        continue;
      }
      AddItem(current_row, equip);
      current_row++;
    }

    /* Add the new item button */
    AddNewItemButton(rows);
  };

  [[nodiscard]] virtual int GetNumberColumns() const
  {
    return kEquipmentColumnCount;
  }

  [[nodiscard]] virtual bool UseEquipment(
    const std::shared_ptr<Equipment>& equip) const
  {
    return !equip->IsArmour() && !equip->IsWeapon() && !equip->IsShield() &&
           !equip->IsHelm();
  }

signals:
  void NewItem();

  /** I don't guarantee that equip exists, don't dereference it */
  void RemoveItem(const std::shared_ptr<Equipment>& equip);
  void UpdatedItem(int row, const std::shared_ptr<Equipment>& equip);

protected:
  QTableWidget& m_table;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_UI_HPP
