/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_equipment_armour.hpp"
#include "../utils.hpp"

QJsonObject
Armour::GetJson() const
{
  QJsonObject object = Equipment::GetJson();
  object["type"] = kEquipmentTypeArmour;
  object["protection"] = this->protection;
  return object;
}

void
Armour::RestoreFromJson(const QJsonObject& object)
{
  Equipment::RestoreFromJson(object);
  this->protection = object["protection"].toInt();
}
