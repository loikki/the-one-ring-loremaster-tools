/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_STATISTICS_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_STATISTICS_HPP

/* Own includes */
#include "../enums.hpp"
#include "../settings.hpp"

/* Standard library */
#include <array>

/* Other libraries */
#include <QJsonArray>
#include <QJsonObject>
#include <QSettings>

struct Attributes
{
  int strength{ 0 };
  int current_endurance{ 0 };
  int max_endurance{ 0 };
  int heart{ 0 };
  int current_hope{ 0 };
  int max_hope{ 0 };
  int wits{ 0 };
  int parry{ 0 };
};

struct Proficiencies
{
  std::array<int, kSkillsCount> skills{};
  std::array<bool, kSkillsCount> skills_favorite{};
  std::array<int, kCombatProficienciesCount> combat{};
};

struct HeroicAttributes
{
  int valour{ 0 };
  std::string rewards;
  int wisdom{ 0 };
  std::string virtues;
  int shadow{ 0 };
  int shadow_scars{ 0 };
};

struct Experience
{
  int current_adventure{ 0 };
  int total_adventure{ 0 };
  int current_skill{ 0 };
  int total_skill{ 0 };
};

class PlayerStatistics
{
public:
  PlayerStatistics();

  [[nodiscard]] int GetTotalShadow() const
  {
    return this->heroic.shadow + this->heroic.shadow_scars;
  }

  [[nodiscard]] bool IsMiserable() const
  {
    return this->heroic.shadow + this->heroic.shadow_scars >=
           this->attributes.current_hope;
  }

  void UpdateSkills(std::list<int> new_values);
  void UpdateCombatProficiencies(std::list<int> new_values);
  void RemoveEndurance(int lost);
  void RemoveHope(int lost);

  [[nodiscard]] int GetStrengthTargetNumber() const
  {
    int max_target =
      QSettings().value(kSettingNames[kSettingPlayerMaxTarget]).toInt();
    return max_target - this->attributes.strength;
  }

  [[nodiscard]] int GetHeartTargetNumber() const
  {
    int max_target =
      QSettings().value(kSettingNames[kSettingPlayerMaxTarget]).toInt();
    return max_target - this->attributes.heart;
  }

  [[nodiscard]] int GetWitsTargetNumber() const
  {
    int max_target =
      QSettings().value(kSettingNames[kSettingPlayerMaxTarget]).toInt();
    return max_target - this->attributes.wits;
  }

  [[nodiscard]] int GetParry() const { return this->attributes.parry; }

  [[nodiscard]] int GetCurrentFatigue() const { return this->fatigue; }

  void AddToJson(QJsonObject& object) const;

  void RestoreFromJson(const QJsonObject& object);
  void AddExperience(int skill, int adventure, bool add_wits);
  void SpendExperience(int skill, int adventure);

  Attributes attributes;
  Proficiencies proficiencies;
  Experience experience;

  HeroicAttributes heroic;
  int fatigue{ 0 };
  bool wounded{ false };
  std::string injury;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_STATISTICS_HPP
