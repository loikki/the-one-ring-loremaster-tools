/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HELM_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HELM_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment_armour.hpp"

/* Other libraries */
#include <QJsonObject>

class Helm : public Armour
{
public:
  Helm() { m_type = kArmourTypeHelm; };
  [[nodiscard]] bool IsWearingHelm() const override { return wearing; }
  [[nodiscard]] bool IsHelm() const override { return true; }
  void WearHelm(bool wear) override { wearing = wear; }
  [[nodiscard]] QJsonObject GetJson() const override;
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_HELM_HPP
