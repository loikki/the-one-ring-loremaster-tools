/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_summary_ui.hpp"
#include "../utils.hpp"

/* Other libraries */
#include <QInputDialog>

PlayerSummaryUi::PlayerSummaryUi(QWidget& parent_ref, Player& player_ref)
  : QFrame(nullptr)
  , Ui_PlayerSummary()
  , parent(parent_ref)
  , player(player_ref)
{
  this->setupUi(this);
  this->ConnectSignalSlots();
  this->UpdateUi();
}

void
PlayerSummaryUi::UpdateUi()
{
  this->player_box->setTitle(QString::fromStdString(this->player.name));
  this->features->setText(QString::fromStdString(this->player.features));
  this->shadow->setValue(this->player.statistics.heroic.shadow);
  this->shadow_scars->setValue(this->player.statistics.heroic.shadow_scars);
  this->travel_role->setCurrentIndex(this->player.journey_role);
  this->fatigue->setValue(this->player.statistics.fatigue);
  this->temporary_load->setValue(this->player.equipment.temporary_load);

  this->SetStatus();
  this->SetHope();
  this->SetEndurance();
};

void
PlayerSummaryUi::SetStatus()
{
  QString text = "";
  if (this->player.statistics.wounded) {
    text += tr("wounded, ");
  }
  if (this->player.IsWeary()) {
    text += tr("weary, ");
  }
  if (this->player.IsMiserable()) {
    text += tr("miserable, ");
  }
  /* Remove ', ' */
  if (text.length() != 0) {
    text.resize(text.length() - 2);
  }
  this->status->setText(text);
}

void
PlayerSummaryUi::ConnectSignalSlots()
{
  QObject::connect(this->travel_role, &QComboBox::currentIndexChanged, [=]() {
    this->UpdateJourneyRole();
  });
  QObject::connect(
    this->shadow, &QSpinBox::valueChanged, [=]() { this->UpdateShadow(); });
  QObject::connect(this->shadow_scars, &QSpinBox::valueChanged, [=]() {
    this->UpdateShadowScars();
  });
  QObject::connect(
    this->fatigue, &QSpinBox::valueChanged, [=]() { this->UpdateFatigue(); });
  QObject::connect(this->temporary_load, &QSpinBox::valueChanged, [=]() {
    this->UpdateTemporaryLoad();
  });
};

void
PlayerSummaryUi::ClickedOnBar(const bool under_endurance)
{
  /* Get some strings */
  QString title(tr("Hope"));
  QString description(tr("Hope lost"));
  if (under_endurance) {
    title = tr("Endurance");
    description = tr("Endurance lost");
  }

  /* Get the value */
  bool is_ok = false;
  int value = QInputDialog::getInt(&this->parent,
                                   title,
                                   description,
                                   /* value */ 0,
                                   /* min */ -kMaxSpinBox,
                                   /* max */ kMaxSpinBox,
                                   /* step */ 1,
                                   &is_ok);

  /* Check if we received something */
  if (not is_ok) {
    return;
  }

  /* Update the value */
  if (under_endurance) {
    this->player.statistics.RemoveEndurance(value);
    this->SetEndurance();
  } else {
    this->player.statistics.RemoveHope(value);
    this->SetHope();
  }
}

void
PlayerSummaryUi::UpdateWound()
{
  const bool wounded = not this->player.statistics.wounded;
  this->player.statistics.wounded = wounded;

  /* Get some information about the injury */
  if (wounded) {
    bool is_ok = false;
    std::string injury = QInputDialog::getText(&this->parent,
                                               tr("Injury"),
                                               tr("Injury description"),
                                               QLineEdit::Normal,
                                               "",
                                               &is_ok)
                           .toStdString();

    /* Check if we received something */
    if (not is_ok) {
      return;
    }
    this->player.statistics.injury = injury;
  } else {
    this->player.statistics.injury = "";
  }

  /* Update Ui */
  this->SetStatus();
}

void
PlayerSummaryUi::mousePressEvent(QMouseEvent* event)
{
  QFrame::mousePressEvent(event);

  const bool under_endurance = this->endurance->underMouse();
  const bool under_hope = this->hope->underMouse();
  const bool under_status = this->status->underMouse();
  if (event->button() != Qt::LeftButton ||
      (not under_endurance && not under_hope && not under_status)) {
    return;
  }

  if (under_status) {
    this->UpdateWound();
  } else {
    this->ClickedOnBar(under_endurance);
  }
};

void
PlayerSummaryUi::SetHope()
{
  const int max_hope = this->player.statistics.attributes.max_hope;
  const int current_hope = this->player.statistics.attributes.current_hope;
  const int total_shadow = this->player.statistics.GetTotalShadow();

  this->hope->setMaximum(max_hope);
  this->hope->setValue(current_hope);
  QString format = "%v / " + QString::number(max_hope);
  format += tr(" (Shadow ") + QString::number(total_shadow) + ")";
  this->hope->setFormat(format);

  /* Avoid issue with 0/0 */
  if (max_hope == 0) {
    return;
  }

  /* Set style */
  if (this->player.IsMiserable()) {
    this->hope->setStyleSheet(kProgressBar + kProgressBarRed);
  } else {
    this->hope->setStyleSheet(kProgressBar + kProgressBarBlue);
  }

  /* Update the status */
  this->SetStatus();
};

void
PlayerSummaryUi::SetEndurance()
{
  const int max_endurance = this->player.statistics.attributes.max_endurance;
  const int current_endurance =
    this->player.statistics.attributes.current_endurance;
  const int total_load = this->player.GetCurrentLoadAndFatigue(
    /* wear_all */ false, /* use_temporary */ true);

  this->endurance->setMaximum(max_endurance);
  this->endurance->setValue(current_endurance);
  QString format = "%v / " + QString::number(max_endurance);
  format += tr(" (Load ") + QString::number(total_load) + ")";
  this->endurance->setFormat(format);

  /* Avoid issue with 0/0 */
  if (max_endurance == 0) {
    return;
  }

  /* Set style */
  if (this->player.IsWeary()) {
    this->endurance->setStyleSheet(kProgressBar + kProgressBarRed);
  } else {
    this->endurance->setStyleSheet(kProgressBar + kProgressBarGreen);
  }

  /* Update the status */
  this->SetStatus();
};
