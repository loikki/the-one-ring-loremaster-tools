/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "../utils.hpp"
#include "player_equipment.hpp"

QJsonObject
Shield::GetJson() const
{
  // NOLINTNEXTLINE
  QJsonObject object = Equipment::GetJson();
  object["type"] = kEquipmentTypeShield;
  object["parry_modifier"] = this->parry_modifier;
  return object;
}

void
Shield::RestoreFromJson(const QJsonObject& object)
{
  Armour::RestoreFromJson(object);
  this->parry_modifier = object["parry_modifier"].toInt();
}
