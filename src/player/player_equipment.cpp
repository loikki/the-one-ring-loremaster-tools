/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "player_equipment.hpp"
#include "../utils.hpp"

void
PlayerEquipment::AddToJson(QJsonObject& object) const
{
  object["treasure"] = this->treasure.current;
  object["treasure_total"] = this->treasure.total;

  object["temporary_load"] = this->temporary_load;

  /* Manage equipment */
  QJsonArray equip;
  for (const auto& element : this->equipment) {
    equip.append(element->GetJson());
  }
  object["equipment"] = equip;
}

void
PlayerEquipment::RestoreFromJson(const QJsonObject& object)
{
  this->treasure.current = object["treasure"].toInt();
  this->treasure.total = object["treasure_total"].toInt();

  this->temporary_load = object["temporary_load"].toInt();

  for (const auto& element : object["equipment"].toArray()) {
    const QJsonObject obj = element.toObject();
    const int type = obj["type"].toInt();
    switch (type) {
      case kEquipmentTypeEquipment:
        this->equipment.push_back(std::unique_ptr<Equipment>(new Equipment()));
        break;
      case kEquipmentTypeArmour:
        this->equipment.push_back(std::unique_ptr<Equipment>(new Armour()));
        break;
      case kEquipmentTypeWeapon:
        this->equipment.push_back(std::unique_ptr<Equipment>(new Weapon()));
        break;
      case kEquipmentTypeShield:
        this->equipment.push_back(std::unique_ptr<Equipment>(new Shield()));
        break;
      case kEquipmentTypeHelm:
        this->equipment.push_back(std::unique_ptr<Equipment>(new Helm()));
        break;
      default:
        throw std::logic_error("Equipment type unknown");
    }
    this->equipment.back()->RestoreFromJson(obj);
  }
}
