/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_ARMOUR_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_ARMOUR_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment_equipment.hpp"

class Armour : public Equipment
{
public:
  [[nodiscard]] QJsonObject GetJson() const override;
  void RestoreFromJson(const QJsonObject& object) override;
  [[nodiscard]] int GetProtection() const override
  {
    return wearing ? protection : 0;
  }
  [[nodiscard]] bool IsArmour() const override { return true; }
  void SetProtection(int value) override { protection = value; }

  int protection{ 0 };
  enum ArmourType m_type
  {
    kArmourTypeArmour
  };
  int parry_modifier{ 0 };
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_ARMOUR_HPP
