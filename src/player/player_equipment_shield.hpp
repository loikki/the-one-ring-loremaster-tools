/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_SHIELD_HPP
#define LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_SHIELD_HPP

/* Own includes */
#include "../enums.hpp"
#include "player_equipment_armour.hpp"

/* Other libraries */
#include <QJsonObject>

class Shield : public Armour
{
public:
  Shield() { m_type = kArmourTypeShield; };
  [[nodiscard]] QJsonObject GetJson() const override;
  void RestoreFromJson(const QJsonObject& object) override;
  [[nodiscard]] int GetParry() const override
  {
    return this->wearing ? parry_modifier : 0;
  }
  [[nodiscard]] bool IsWearingShield() const override { return wearing; }
  void WearShield(bool wear) override { wearing = wear; }
  [[nodiscard]] bool IsShield() const override { return true; }
  void SetParry(int value) override { parry_modifier = value; }
};

#endif // LOREMASTER_TOOLS_SRC_PLAYER_EQUIPMENT_SHIELD_HPP
