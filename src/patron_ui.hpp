/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_PATRON_UI_HPP
#define LOREMASTER_TOOLS_SRC_PATRON_UI_HPP

/* Own includes */
#include "gui/ui_patron.h"
#include "patron.hpp"

/* Other includes */
#include <QObject>

class PatronUi
  : public QObject
  , public Ui_Patron
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  PatronUi& operator=(PatronUi&&) = delete;
  PatronUi(PatronUi&&) = delete;
  PatronUi(const PatronUi&) = delete;
  PatronUi& operator=(const PatronUi&) = delete;
  PatronUi(QWidget& parent_ref, const std::string& dir);
  ~PatronUi() override = default;

  [[nodiscard]] int GetFellowshipPoints() const
  {
    return this->current_patron->fellowship_points;
  }
  void UpdateUi();
  void SetDirectory(const std::string& dir)
  {
    this->directory = dir + "/Patron/";
  };

protected:
  void LoadPatron(std::string filename = "");
  [[nodiscard]] std::string GetDirectory() const { return this->directory; };
  void ConnectSignalSlots();

  QWidget& parent;
  std::string directory;
  std::vector<Patron> patrons;
  Patron* current_patron;

signals:
  void UpdateStatusBar(QString text);
  void PatronChanged(Patron* new_patron);
};

#endif // LOREMASTER_TOOLS_SRC_PATRON_HPP
