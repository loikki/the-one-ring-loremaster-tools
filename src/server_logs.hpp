/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_SERVER_LOGS_HPP
#define LOREMASTER_TOOLS_SRC_SERVER_LOGS_HPP

/* Local includes */
#include "gui/ui_server_logs.h"

/* Standard library */
#include <cstdint>
#include <vector>

/* Other includes */
#include <QString>
#include <QTimer>

enum LogLevel
{
  kLogLevelWarning = 0,
  kLogLevelInfo,
  kLogLevelDebug,
  kLogLevelCount,
};

static const std::array<QString, kLogLevelCount> kLogLevelStrings = { "Warning",
                                                                      "Info",
                                                                      "Debug" };

using InternalLog = std::tuple<LogLevel, int /* time in s */, QString>;
using ExternalLog = std::tuple<LogLevel, QString>;

class ServerLogsUi : public Ui_ServerLog
{
public:
  explicit ServerLogsUi(QWidget& parent);
  static void AddLog(ExternalLog log);
  void UpdateLogs();

protected:
  void WriteLogs();
  void ConnectSignalSlots();

  /* Attributes */
  // NOLINTNEXTLINE
  static std::vector<InternalLog> logs;
  const static std::chrono::time_point<std::chrono::system_clock> start;

  std::uint64_t current_index{ 0 };
  QTimer timer;
};

/* Log functions */
inline void
LogInfo(const std::string& text)
{
  ServerLogsUi::AddLog(
    std::make_tuple(kLogLevelInfo, QString::fromStdString(text)));
};
inline void
LogDebug(const std::string& text)
{
  ServerLogsUi::AddLog(
    std::make_tuple(kLogLevelDebug, QString::fromStdString(text)));
}
inline void
LogWarning(const std::string& text)
{
  ServerLogsUi::AddLog(
    std::make_tuple(kLogLevelWarning, QString::fromStdString(text)));
}

#endif // LOREMASTER_TOOLS_SRC_SERVER_LOGS_HPP
