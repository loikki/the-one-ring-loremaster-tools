/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "server_logs.hpp"

/* Standard library */
#include <iostream>

/* Other includes */
#include <oatpp/core/base/Environment.hpp>

// NOLINTNEXTLINE
std::vector<InternalLog> ServerLogsUi::logs;
const std::chrono::time_point<std::chrono::system_clock> ServerLogsUi::start =
  std::chrono::system_clock::now();

ServerLogsUi::ServerLogsUi(QWidget& parent_ref)
  : Ui_ServerLog()
{
  this->setupUi(&parent_ref);

  for (int i = 0; i < kLogLevelCount; i++) {
    this->log_level->addItem(kLogLevelStrings[i]);
  }

  /* Add signals */
  this->ConnectSignalSlots();
}

void
ServerLogsUi::ConnectSignalSlots()
{
  /* Change log level */
  QObject::connect(this->log_level, &QComboBox::currentIndexChanged, [=]() {
    this->WriteLogs();
  });

  /* Timer */
  const int refresh_interval_ms = 1000;
  this->timer.setInterval(refresh_interval_ms);
  QObject::connect(
    &this->timer, &QTimer::timeout, [=]() { this->UpdateLogs(); });
  this->timer.start();
}

void
ServerLogsUi::UpdateLogs()
{
  /* Get the log level */
  auto current_level = static_cast<LogLevel>(this->log_level->currentIndex());

  /* Add the missing logs */
  QString output = "";
  std::uint64_t new_current_index = ServerLogsUi::logs.size();
  for (auto log = ServerLogsUi::logs.cbegin() +
                  static_cast<std::int64_t>(this->current_index);
       log != ServerLogsUi::logs.cend();
       log++) {

    /* Only show required logs */
    auto level = static_cast<LogLevel>(std::get<0>(*log));
    if (std::get<0>(*log) <= current_level) {

      output += " " + kLogLevelStrings[level] + ": [" +
                QString::number(std::get<1>(*log));
      output += "s] " + std::get<2>(*log) + "\n";
    }
  }

  /* Update the current index */
  this->current_index = new_current_index;

  /* Write the logs */
  if (!output.isEmpty()) {
    /* Remove last \n */
    output = output.remove(output.size() - 1, 1);
    this->logs_text->appendPlainText(output);
  }
  /* Restart the timer */
  this->timer.start();
}

void
ServerLogsUi::AddLog(ExternalLog log)
{
  /* Add the time */
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<float> diff = end - start;
  std::chrono::milliseconds milli_s =
    std::chrono::duration_cast<std::chrono::milliseconds>(diff);
  const int time =
    static_cast<int>(std::round(static_cast<double>(milli_s.count()) / 1000.));

  ServerLogsUi::logs.emplace_back(
    std::make_tuple(std::get<0>(log), time, std::get<1>(log)));
  switch (std::get<0>(log)) {
    case kLogLevelWarning:
      // NOLINTNEXTLINE
      OATPP_LOGW("TheOneRing", std::get<1>(log).toStdString().c_str());
      break;
    case kLogLevelInfo:
      // NOLINTNEXTLINE
      OATPP_LOGI("TheOneRing", std::get<1>(log).toStdString().c_str());
      break;
    case kLogLevelDebug:
      // NOLINTNEXTLINE
      OATPP_LOGD("TheOneRing", std::get<1>(log).toStdString().c_str());
      break;
    default:
      std::cout << "Not implemented" << std::endl;
  }
}

void
ServerLogsUi::WriteLogs()
{
  /* Reset everything */
  this->current_index = 0;
  this->logs_text->setPlainText("");

  /* Update the logs */
  this->UpdateLogs();
}
