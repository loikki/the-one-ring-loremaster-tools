/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_UTILS_HPP
#define LOREMASTER_TOOLS_SRC_UTILS_HPP

/* Other libraries */
#include <QBuffer>
#include <QFile>
#include <QImage>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

static const QString kProgressBar("QProgressBar {"
                                  "  border: 1px solid #76797C;"
                                  "  border-radius: 5px;"
                                  "  text-align: center;"
                                  "}");

static const QString kProgressBarRed("QProgressBar::chunk {"
                                     "  background-color: #ce2029;"
                                     "}");
static const QString kProgressBarGreen("QProgressBar::chunk {"
                                       "  background-color: #1acb39;"
                                       "}");
static const QString kProgressBarBlue("QProgressBar::chunk {"
                                      "  background-color: #4cbaf4;"
                                      "}");

template<typename Button, typename value_type>
void
SetValueWithBlock(Button* button, value_type& value)
{
  button->blockSignals(true);
  button->setValue(value);
  button->blockSignals(false);
}

template<typename Button, typename value_type>
void
SetCheckedWithBlock(Button* button, value_type& value)
{
  button->blockSignals(true);
  button->setChecked(value);
  button->blockSignals(false);
}

QJsonValue inline JsonValFromImage(const QImage& input)
{
  QBuffer buffer;
  buffer.open(QIODevice::WriteOnly);
  input.save(&buffer, "PNG");
  QByteArray const encoded = buffer.data().toBase64();
  return QLatin1String(encoded);
}

QImage inline ImageFromJsonVal(const QJsonValue& val)
{
  QByteArray const encoded = val.toString().toLatin1();
  QImage output;
  output.loadFromData(QByteArray::fromBase64(encoded), "PNG");
  return output;
}

template<typename T>
QJsonArray
ArrayToJson(const T& myVec)
{
  QJsonArray result;
  std::copy(myVec.begin(), myVec.end(), std::back_inserter(result));
  return result;
}

template<typename Array>
void
JsonToIntArray(Array& result, const QJsonValue& input_value)
{
  const QJsonArray input = input_value.toArray();
  const auto input_size = static_cast<std::uint64_t>(input.size());
  if (input_size != result.size()) {
    throw std::length_error("Inputs are invalid");
  }
  for (std::uint64_t i = 0; i < result.size(); i++) {
    const auto index = static_cast<std::int64_t>(i);
    result[i] = input[index].toInt();
  }
}

template<typename Array>
void
JsonToBoolArray(Array& result, const QJsonValue& input_value)
{
  const QJsonArray input = input_value.toArray();
  const auto input_size = static_cast<std::uint64_t>(input.size());
  if (input_size != result.size()) {
    throw std::length_error("Inputs are invalid");
  }
  for (std::uint64_t i = 0; i < result.size(); i++) {
    const auto index = static_cast<std::int64_t>(i);
    result[i] = input[index].toBool();
  }
}

template<typename Json>
void inline WriteJsonToFile(const Json& obj, const std::string& filename)
{
  QJsonDocument doc(obj);
  QFile file(QString::fromStdString(filename));
  file.open(QFile::WriteOnly);
  file.write(doc.toJson(QJsonDocument::Indented));
}

QJsonDocument inline ReadJsonDocument(const std::string& filename)
{
  QFile file(QString::fromStdString(filename));
  file.open(QFile::ReadOnly);
  QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
  return doc;
}

QJsonObject inline ReadJsonObject(const std::string& filename)
{
  QJsonDocument doc = ReadJsonDocument(filename);
  if (!doc.isObject()) {
    throw std::ios_base::failure("Document " + filename +
                                 " is not a json object");
  }
  return doc.object();
}

QJsonArray inline ReadJsonArray(const std::string& filename)
{
  QJsonDocument doc = ReadJsonDocument(filename);
  if (!doc.isArray()) {
    throw std::ios_base::failure("Document " + filename +
                                 " is not a json array");
  }
  return doc.array();
}

std::string inline ToTitleCase(const std::string& input)
{
  std::string output;

  std::uint64_t pre_pos = 0;
  std::uint64_t pos = input.find(' ', pre_pos);

  while (pos != std::string::npos) {
    std::string sub = input.substr(pre_pos, (pos - pre_pos));

    if (pre_pos != pos) {
      sub = input.substr(pre_pos, (pos - pre_pos));
    } else {
      sub = input.substr(pre_pos, 1);
    }
    sub[0] = static_cast<char>(toupper(sub[0]));
    output += sub + input[pos];

    if (pos < (input.length() - 1)) {
      pre_pos = (pos + 1);
    } else {
      pre_pos = pos;
      break;
    }

    pos = static_cast<int>(input.find(' ', pre_pos));
  }

  std::string sub = input.substr(pre_pos, std::string::npos);
  sub[0] = static_cast<char>(toupper(sub[0]));
  output += sub;
  return output;
}
#endif // LOREMASTER_TOOLS_SRC_UTILS_HPP
