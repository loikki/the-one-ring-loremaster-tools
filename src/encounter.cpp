/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "encounter.hpp"
#include "utils.hpp"

/* Other libraries */
#include <QDir>
#include <QJsonArray>

void
// NOLINTNEXTLINE
Encounter::LoadEncounter(const std::string& filename,
                         const std::string& adversary_directory)
{
  /* Cleanup the current adversaries */
  this->adversaries.clear();

  /* Load the encounter */
  QJsonArray encounter = ReadJsonArray(filename);
  for (auto current = encounter.constBegin(); current != encounter.constEnd();
       ++current) {
    this->NewAdversary();
    std::string adversary_filename = current->toString().toStdString() + ".adv";
    // NOLINTNEXTLINE
    this->adversaries.back().ReadFromFile(adversary_directory + "/" +
                                          adversary_filename);
  }
}

void
// NOLINTNEXTLINE
Encounter::SaveEncounter(const std::string& filename,
                         const std::string& adversary_directory) const
{
  QJsonArray output;
  for (const Adversary& current : this->adversaries) {
    current.SaveToFile(adversary_directory);
    output.push_back(QString::fromStdString(current.name));
  }
  WriteJsonToFile(output, filename);
}

void
Encounter::RemoveAdversary(const Adversary* adversary)
{
  for (auto it = this->adversaries.cbegin(); it != this->adversaries.cend();
       ++it) {
    if (&(*it) == adversary) {
      this->adversaries.erase(it);
      break;
    }
  }
  /* Ensure that we still have at least one player */
  if (this->adversaries.empty()) {
    this->adversaries.emplace_back(Adversary());
  }
}
