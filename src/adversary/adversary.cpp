/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "adversary.hpp"
#include "../utils.hpp"

void
Adversary::SaveToFile(const std::string& directory) const
{
  /* Ensure that the output directory exists */
  if (not std::filesystem::exists(directory)) {
    std::filesystem::create_directory(directory);
  }

  /* Avoid saving empty adversaries */
  if (this->IsEmpty()) {
    return;
  }
  std::string filename = this->GetFilename(directory);
  QJsonObject json = this->GetJson();
  WriteJsonToFile(json, filename);
}

std::string
Adversary::GetFilename(const std::string& directory) const
{
  std::string output(directory);
  output += "/";
  output += this->name;
  output += ".adv";
  return output;
}

void
Adversary::ReadFromFile(const std::string& filename)
{
  QJsonObject json = ReadJsonObject(filename);
  this->RestoreFromJson(json);
}

QJsonObject
Adversary::GetJson() const
{
  QJsonObject object;
  object["name"] = QString::fromStdString(this->name);
  object["charact"] = QString::fromStdString(this->characteristics);
  object["attribute"] = this->attribute;
  object["endurance"] = this->max_endurance;
  object["might"] = this->might;
  object["hate"] = this->max_hate;
  object["use_hate"] = this->use_hate;
  object["parry"] = this->parry;
  object["armor"] = this->armor;
  object["comb_prof"] = QString::fromStdString(this->combat_proficiencies);
  object["abilities"] = QString::fromStdString(this->abilities);

  return object;
}

void
Adversary::RestoreFromJson(const QJsonObject& object)
{
  this->name = object["name"].toString().toStdString();
  this->characteristics = object["charact"].toString().toStdString();
  this->attribute = object["attribute"].toInt();
  this->current_endurance = object["endurance"].toInt();
  this->max_endurance = this->current_endurance;
  this->might = object["might"].toInt();
  this->current_hate = object["hate"].toInt();
  this->max_hate = this->current_hate;
  this->use_hate = object["use_hate"].toBool();
  this->parry = object["parry"].toInt();
  this->armor = object["armor"].toInt();
  this->combat_proficiencies = object["comb_prof"].toString().toStdString();
  this->abilities = object["abilities"].toString().toStdString();
}

void
Adversary::RemoveEndurance(int lost)
{
  this->current_endurance = std::max(0, this->current_endurance - lost);
  this->current_endurance =
    std::min(this->max_endurance, this->current_endurance);
}

void
Adversary::RemoveHate(int lost)
{
  this->current_hate = std::max(0, this->current_hate - lost);
  this->current_hate = std::min(this->max_hate, this->current_hate);
}
