/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_ADVERSARY_UI_HPP
#define LOREMASTER_TOOLS_SRC_ADVERSARY_UI_HPP

/* Own includes */
#include "../encounter.hpp"
#include "../gui/ui_adversary.h"
#include "adversary.hpp"

class AdversaryUi
  : public QObject
  , public Ui_adversary
{
  // NOLINTNEXTLINE
  Q_OBJECT
public:
  AdversaryUi(QWidget& parent,
              std::string directory,
              std::shared_ptr<Encounter> encounter);
  void UpdateUi();
  void SetDirectory(std::string new_directory)
  {
    this->directory = std::move(new_directory);
  }
  [[nodiscard]] std::string GetDirectory() const;
  void UpdateCurrentAdversary(int index);
  void SaveToAdversary();

protected:
  void ConnectSignalSlots();
  void RemoveCurrentAdversary();
  void LoadAdversary(std::string filename = "");
  void NewAdversary();
  void SaveCurrentAdversary();

  QWidget& parent;
  Adversary* adversary{ nullptr };
  std::shared_ptr<Encounter> encounter;
  std::string directory;

signals:
  void UpdateStatusBar(QString text);
};

#endif // LOREMASTER_TOOLS_SRC_ADVERSARY_UI_HPP
