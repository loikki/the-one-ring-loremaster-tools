/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "adversary_summary_ui.hpp"
#include "../utils.hpp"
#include "adversary.hpp"

/* Other libraries */
#include <QInputDialog>

AdversarySummaryUi::AdversarySummaryUi(QWidget& parent_ref,
                                       Adversary& adversary_ref)
  : QFrame(&parent_ref)
  , Ui_AdversarySummary()
  , parent(parent_ref)
  , adversary(adversary_ref)
{
  this->setupUi(this);
  this->UpdateUi();
}

void
AdversarySummaryUi::UpdateUi()
{
  this->adv->setTitle(QString::fromStdString(this->adversary.name));
  this->parry->setText(QString::number(this->adversary.parry));
  this->comb_prof->setText(
    QString::fromStdString(this->adversary.combat_proficiencies));
  this->attribute->setText(QString::number(this->adversary.attribute));
  this->armor->setText(QString::number(this->adversary.armor));

  if (this->adversary.use_hate) {
    this->hate_label->setText(tr("Hate"));
    this->hate->setStyleSheet(kProgressBar + kProgressBarRed);
  } else {
    this->hate_label->setText(tr("Resolve"));
    this->hate->setStyleSheet(kProgressBar + kProgressBarBlue);
  }
  this->endurance->setStyleSheet(kProgressBar + kProgressBarGreen);

  this->SetHate();
  this->SetEndurance();
};

void
AdversarySummaryUi::mousePressEvent(QMouseEvent* event)
{
  QFrame::mousePressEvent(event);

  const bool under_endurance = this->endurance->underMouse();
  const bool under_hate = this->hate->underMouse();
  if (event->button() != Qt::LeftButton ||
      (not under_endurance && not under_hate)) {
    return;
  }

  /* Get some strings */
  QString title(tr("Hate"));
  QString description(tr("Hate lost"));
  if (under_endurance) {
    title = tr("Endurance");
    description = tr("Endurance lost");
  }

  /* Get the value */
  bool is_ok = false;
  int value = QInputDialog::getInt(&this->parent,
                                   title,
                                   description,
                                   /* value */ 0,
                                   /* min */ -kMaxSpinValue,
                                   /* max */ kMaxSpinValue,
                                   /* step */ 1,
                                   &is_ok);

  /* Check if we received something */
  if (not is_ok) {
    return;
  }

  /* Update the value */
  if (under_endurance) {
    this->adversary.RemoveEndurance(value);
    this->SetEndurance();
  } else {
    this->adversary.RemoveHate(value);
    this->SetHate();
  }
};

void
AdversarySummaryUi::SetHate()
{
  const int max_hate = this->adversary.max_hate;
  const int current_hate = this->adversary.current_hate;

  this->hate->setMaximum(max_hate);
  this->hate->setValue(current_hate);
  std::string format = "%v / " + std::to_string(max_hate);
  this->hate->setFormat(QString::fromStdString(format));
};

void
AdversarySummaryUi::SetEndurance()
{
  const int max_endurance = this->adversary.max_endurance;
  const int current_endurance = this->adversary.current_endurance;

  this->endurance->setMaximum(max_endurance);
  this->endurance->setValue(current_endurance);
  std::string format = "%v / " + std::to_string(max_endurance);
  this->endurance->setFormat(QString::fromStdString(format));
};
