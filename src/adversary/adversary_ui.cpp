/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Own includes */
#include "adversary_ui.hpp"
#include "../utils.hpp"
#include "adversary.hpp"

/* Other includes */
#include <QDir>
#include <QFileDialog>

AdversaryUi::AdversaryUi(QWidget& parent_ref,
                         std::string new_directory,
                         std::shared_ptr<Encounter> encounter_ptr)
  : Ui_adversary()
  , parent(parent_ref)
  , encounter(std::move(encounter_ptr))
  , directory(std::move(new_directory))
{
  this->setupUi(&parent_ref);
  this->UpdateCurrentAdversary(-1);
  this->ConnectSignalSlots();

  emit this->UpdateStatusBar(tr("Adversary ready"));
}

void
AdversaryUi::ConnectSignalSlots()
{
  /* Change name */
  QObject::connect(
    this->name, &QLineEdit::textChanged, [=](const QString& text) {
      int current = this->choice->currentIndex();
      this->adversary->name = text.toStdString();
      this->choice->setItemText(current,
                                QString::fromStdString(this->adversary->name));
    });

  /* New adversary */
  QObject::connect(
    this->new_button, &QPushButton::clicked, [=]() { this->NewAdversary(); });

  /* Save adversary */
  QObject::connect(this->save_button, &QPushButton::clicked, [=]() {
    this->SaveCurrentAdversary();
  });

  /* Load adversary */
  QObject::connect(
    this->load_button, &QPushButton::clicked, [=]() { this->LoadAdversary(); });

  /* Remove adversary */
  QObject::connect(this->remove_button, &QPushButton::clicked, [=]() {
    this->RemoveCurrentAdversary();
  });

  /* Change player */
  QObject::connect(this->choice, &QComboBox::currentIndexChanged, [=]() {
    this->SaveCurrentAdversary();
    this->UpdateCurrentAdversary(this->choice->currentIndex());
  });
}

std::string
AdversaryUi::GetDirectory() const
{
  return this->directory + "/Adversaries/";
}

void
AdversaryUi::UpdateUi()
{
  this->name->setText(QString::fromStdString(this->adversary->name));
  this->charact->setText(
    QString::fromStdString(this->adversary->characteristics));
  this->attribute->setValue(this->adversary->attribute);
  this->endurance->setValue(this->adversary->max_endurance);
  this->might->setValue(this->adversary->might);
  this->hate->setValue(this->adversary->max_hate);
  this->use_hate->setChecked(this->adversary->use_hate);
  this->parry->setValue(this->adversary->parry);
  this->armor->setValue(this->adversary->armor);
  this->comb_prof->setText(
    QString::fromStdString(this->adversary->combat_proficiencies));
  this->abilities->setText(QString::fromStdString(this->adversary->abilities));
}

void
AdversaryUi::RemoveCurrentAdversary()
{
  /* Remove current adversary */
  this->encounter->RemoveAdversary(this->adversary);

  /* Update the current adversary */
  this->UpdateCurrentAdversary(-1);

  emit this->UpdateStatusBar(tr("Adversary removed"));
}

void
AdversaryUi::LoadAdversary(std::string filename)
{
  if (filename.length() == 0) {
    /* Get the directory */
    const std::string default_directory = this->GetDirectory();
    const QString qdefault_directory =
      QString::fromStdString(default_directory);

    /* Ensure that the directory exists */
    if (!QDir().mkpath(qdefault_directory)) {
      throw std::ios_base::failure("Failed to created the directory '" +
                                   default_directory + "'");
    };
    filename = QFileDialog::getOpenFileName(&this->parent,
                                            tr("Open Adversary"),
                                            qdefault_directory,
                                            tr("Adversary file (*.adv)"))
                 .toStdString();

    /* Check if we received something */
    if (filename.length() == 0) {
      return;
    }
  }

  /* Read it from the file */
  this->adversary->ReadFromFile(filename);

  /* Update Ui */
  this->UpdateUi();

  emit this->UpdateStatusBar(tr("Adversary loaded"));
}

void
AdversaryUi::NewAdversary()
{
  this->encounter->NewAdversary();
  this->UpdateCurrentAdversary(-1);

  emit this->UpdateStatusBar(tr("New adversary created"));
}

void
AdversaryUi::UpdateCurrentAdversary(int index)
{
  if (index < 0) {
    index = static_cast<int>(this->encounter->size()) + index;
  }

  /* Block the signals */
  this->choice->blockSignals(true);

  /* Reset the combo box */
  this->choice->clear();
  for (const Adversary& current : *this->encounter) {
    this->choice->addItem(QString::fromStdString(current.name));
  }

  /* Set the current index */
  this->choice->setCurrentIndex(index);
  this->adversary = &(*this->encounter)[index];

  /* Unblock the signals */
  this->choice->blockSignals(false);

  /* Update UI */
  this->UpdateUi();
}

void
AdversaryUi::SaveCurrentAdversary()
{
  this->SaveToAdversary();
  this->adversary->SaveToFile(this->GetDirectory());
  emit this->UpdateStatusBar(tr("Adversary saved"));
}

void
AdversaryUi::SaveToAdversary()
{
  this->adversary->name = this->name->text().toStdString();
  this->adversary->characteristics = this->charact->text().toStdString();
  this->adversary->attribute = this->attribute->value();
  this->adversary->max_endurance = this->endurance->value();
  this->adversary->might = this->might->value();
  this->adversary->max_hate = this->hate->value();
  this->adversary->use_hate = this->use_hate->isChecked();
  this->adversary->parry = this->parry->value();
  this->adversary->armor = this->armor->value();
  this->adversary->combat_proficiencies =
    this->comb_prof->toPlainText().toStdString();
  this->adversary->abilities = this->abilities->toPlainText().toStdString();
}
