/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_ADVERSARY_HPP
#define LOREMASTER_TOOLS_SRC_ADVERSARY_HPP

/* Other libraries */
#include <QJsonObject>

class Adversary
{

public:
  [[nodiscard]] bool IsEmpty() const { return this->name.length() == 0; }

  void SaveToFile(const std::string& directory) const;

  [[nodiscard]] std::string GetFilename(const std::string& directory) const;

  void ReadFromFile(const std::string& filename);
  [[nodiscard]] QJsonObject GetJson() const;
  void RestoreFromJson(const QJsonObject& object);
  void RemoveEndurance(int lost);
  void RemoveHate(int lost);

  std::string name;
  std::string characteristics;
  int attribute{ 0 };
  int current_endurance{ 0 };
  int max_endurance{ 0 };
  int might{ 0 };
  int current_hate{ 0 };
  int max_hate{ 0 };
  bool use_hate{ true };
  int parry{ 0 };
  int armor{ 0 };
  std::string combat_proficiencies;
  std::string abilities;
};

#endif // LOREMASTER_TOOLS_SRC_ADVERSARY_HPP
