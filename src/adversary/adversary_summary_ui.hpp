/**
 * Loremaster-tools
 * Copyright (C) 2022 loikki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOREMASTER_TOOLS_SRC_SUMMARY_ADVERSARY_UI_HPP
#define LOREMASTER_TOOLS_SRC_SUMMARY_ADVERSARY_UI_HPP

/* Own includes */
#include "../gui/ui_adversary_summary.h"

/* Other includes */
#include <QFrame>
#include <QMouseEvent>

/* Forward declaration */
class Adversary;

const int kMaxSpinValue = 1000;

class AdversarySummaryUi
  : public QFrame
  , public Ui_AdversarySummary
{
  // NOLINTNEXTLINE
  Q_OBJECT;

public:
  AdversarySummaryUi(QWidget& parent, Adversary& adversary);
  void UpdateUi();

protected:
  void mousePressEvent(QMouseEvent* event) override;
  void SetHate();
  void SetEndurance();

  /* Attributes */
  QWidget& parent;
  Adversary& adversary;
};

#endif // LOREMASTER_TOOLS_SRC_SUMMARY_ADVERSARY_UI_HPP
