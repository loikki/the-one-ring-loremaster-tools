The One Ring - Loremaster tools
======================================
[![pipeline status](https://gitlab.com/loikki/the-one-ring-loremaster-tools/badges/main/pipeline.svg)](https://gitlab.com/loikki/the-one-ring-loremaster-tools/-/commits/main) 
[![coverage report](https://gitlab.com/loikki/the-one-ring-loremaster-tools/badges/main/coverage.svg)](https://gitlab.com/loikki/the-one-ring-loremaster-tools/-/commits/main)

This program provides a few tools to the loremaster (see screenshots below for the main tools).
It is designed and develop on Linux, but I am doing my best to ensure that everything is working for Windows.
This software is mainly developed in English, but translations can be easily integrated.
If you are interested in providing a translation, please contact me.
A discord channel is available here for any question, bug or new ideas: https://discord.gg/ZFstbnfppZ

Player's Frontend
---------------------

This software is build as a server and players can connect to it through the frontend ([code available here](https://gitlab.com/loikki/the-one-ring-loremaster-tools-frontend); [server here](https://theonering-loremaster-tools.herokuapp.com/)).
In order to access their characters, the players need to provide the server address and their own name (field "Player" in this software). The information is saved into the cookies. If for any reason, you need to change the values, there is a button that resets the cookies. 

For the server address, you will need to find your IP address. Most routers do not allow by default a remote connection to your machine for security reasons. So I would recommend that all the players and the server are on the same network (meaning using the same wifi) and then use the local ip (available through your OS).

Installing
------------
In the [release page](https://gitlab.com/loikki/the-one-ring-loremaster-tools/-/releases), some binaries are available for both Linux and Windows.
For Linux, rpm and deb package are provided. If you do not use such packages, you can download the AppImage that contains all the required libraries.

For some reasons, the deb packages is missing a dependency to `qt6-qpa-plugins`, so you might need to install it.
On Fedora, yaml-cpp 0.7.0 is not yet available. You can download it [here](https://gitlab.com/loikki/yaml-cpp-rpm/-/releases)

Compiling
-----------
If you wish to compile it yourself, you will need [Qt6](https://www.qt.io/product/qt6) and [cpp-yaml](https://yaml-cpp.docsforge.com/master/getting-started/):

```bash
sudo apt-get install libyaml-cpp-dev qt6-base-dev qt6-tools-dev qt6-tools-dev-tools qt6-l10n-tools libqt6charts6-dev libqt6svg6-dev
```

In `.gitlab-ci.yml`, you will find all the required libraries if I forgot one.

To compile, you will need to create a build directory and call CMake with a build type Release or Debug:
```bash
make cmake-debug build
```

Translation
--------------

Feel free to open a merge request with your translation. I will be happy to accept it.
If you need some help with the translation process, please contact me on discord.

### How to do a translation

Clean the repo and regenerate the translation files: `make update_translations`.
Use `linguist` from Qt to generate the translation (for french: `linguist translate/fr.ts`).
You can add some ts files for your language in the `CMakeLists.txt` (look for `qt6_add_translations`) and `cmake/Install.cmake`.
You will also need to add the language to `translate/languages.yaml`.
In order to produce the `qm` files, you will need to run `make release_translations`.

Merge Requests
----------------

Feel free to propose some merge requests or fill some issues, I am always happy to see some contributions.
If you are unsure on how to do it or wish to have some feedback, feel free to contact me on discord.


Small technical details
--------------------------

 * Loading a character will replace the current one. You need to create one first if you do not wish to replace it.
 * In the overviews, clicking a bar will open a dialog and allow you to update it.
 * To be safe, the program tends to often write the players into a file without almost no notice (see the status bar for more informations).
 * In the overview, it is possible to add a wound to a character by clicking on the status.
 * For the equipment qualities, you need to write one quality per line.


Contributors
---------------

 * Unexcpected Spanish Inquisition (arts)

Screenshots
------------

![alt text](screenshot/players.png)

![alt text](screenshot/adversaries.png)

![alt text](screenshot/combat_overview.png)

![alt text](screenshot/overview.png)

![alt text](screenshot/patron.png)

A few tools are also available:

![alt text](screenshot/name_generator.png)

![alt text](screenshot/random_features.png)

![alt text](screenshot/events.png)

![alt text](screenshot/rolls.png)

![alt text](screenshot/ruins.png)
