#!/bin/bash

# Install from apt-get
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y qt6-base-dev qt6-tools-dev qt6-tools-dev-tools qt6-l10n-tools libqt6charts6-dev build-essential cmake libgl1-mesa-dev gcovr clang-format libyaml-cpp-dev clang-tidy python3-pip doxygen file rpm wget libqt6svg6-dev

# Install conan
pip3 install conan

# Install other dependencies
conan profile detect
conan install cmake/conanfile_ubuntu.txt --build missing -s compiler.libcxx=libstdc++11 -s build_type=Release
conan install cmake/conanfile_ubuntu.txt --build missing -s compiler.libcxx=libstdc++11 -s build_type=Debug
