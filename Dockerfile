FROM ubuntu:22.04
ENV PIP_ROOT_USER_ACTION=ignore
COPY install_dependencies.sh /install_dependencies.sh
COPY cmake/conanfile_ubuntu.txt /cmake/conanfile_ubuntu.txt
RUN ./install_dependencies.sh