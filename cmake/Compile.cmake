function(set_compilation target)
  target_link_libraries(${target}
    OpenMP::OpenMP_CXX
    Qt6::Widgets
    Qt6::Charts
    Qt6::Core
    Qt6::OpenGL
    Qt6::OpenGLWidgets
    Qt6::Gui
    yaml-cpp
    oatpp::oatpp
    oatpp::oatpp-swagger
  )
  target_compile_definitions(${target} PUBLIC THE_ONE_RING_VERSION="${CMAKE_PROJECT_VERSION}" FRONTEND_URL="${FRONTEND_URL}")

  # Add the library if needed
  if(NOT target MATCHES lib-loremastertools)
    if ( WIN32 )
      target_include_directories(${target} PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/lib-loremastertools_autogen/include_Release)
    else()
      target_include_directories(${target} PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/lib-loremastertools_autogen/include)
    endif()
    target_link_libraries(${target}
      lib-loremastertools
    )
  endif()

  # Set some C++ parameters
  if ( WIN32 )
    # It seems that windows is behind in term of features
    target_compile_features(${target} PRIVATE cxx_std_20)
    target_link_libraries(${target} dwrite)
  else()
    target_compile_features(${target} PRIVATE cxx_std_17)

    # Debug vs Release
    if(CMAKE_BUILD_TYPE MATCHES Debug)
      target_compile_options(
        ${target} PRIVATE
        -gdwarf -fvar-tracking-assignments
      )
    else()
      target_compile_options(
        ${target} PRIVATE
        -O3 -fomit-frame-pointer
        -malign-double -fstrict-aliasing
        -ffast-math -funroll-loops -DPROD=1
      )
    endif()
    target_compile_options(${target} PRIVATE -Wall -Wextra -Wshadow -Werror -fmax-errors=5)
  endif()

  # Address sanitizer
  if(EnableSanitizer)
    target_compile_options(
      ${target} PRIVATE
      -fsanitize=address -fno-omit-frame-pointer
    )
    target_link_options(
      ${target} PRIVATE
      -fsanitize=address -fno-omit-frame-pointer
    )
  endif()

  # Undefined sanitizer
  if(EnableUndefinedSanitizer)
    target_compile_options(
      ${target} PRIVATE
      -fsanitize=undefined
    )
    target_link_options(
      ${target} PRIVATE
      -fsanitize=undefined
    )
  endif()

  # Coverage
  if(EnableCoverage)
    target_compile_options(
      ${target} PRIVATE
      --coverage
      -fno-inline -fno-inline-small-functions -fno-default-inline
      -Wno-unused-function
      # -fkeep-inline-functions
    )
    target_link_options(${target} PRIVATE -fprofile-arcs -lstdc++ --coverage)
  endif()
endfunction()
