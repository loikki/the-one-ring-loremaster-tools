[requires]
oatpp/1.3.0
oatpp-swagger/1.3.0
yaml-cpp/0.7.0
qt/6.5.2

[options]
qt/*:qtcharts=True
qt/*:qttranslations=True
qt/*:qttools=True
qt/*:shared=True

[generators]
cmake_find_package
