function(set_install)

  # which is useful in case of packing only selected components instead of the whole thing
  set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Loremaster Tools for The One Ring"
    CACHE STRING "Package description for the package metadata"
  )
  set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/loikki/the-one-ring-loremaster-tools")

  set(CPACK_VERBATIM_VARIABLES YES)

  set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_PACKAGE_NAME})
  SET(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_SOURCE_DIR}/build")

  set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
  set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
  set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

  set(CPACK_PACKAGE_CONTACT "loic_hausammann@hotmail.com")
  set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Loikki <${CPACK_PACKAGE_CONTACT}>")

  set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
  set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
  # if you want every group to have its own package,
  # although the same happens if this is not sent (so it defaults to ONE_PER_GROUP)
  # and CPACK_DEB_COMPONENT_INSTALL is set to YES
  set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)#ONE_PER_GROUP)
  set(CPACK_STRIP_FILES YES)
  set(
    CPACK_INSTALL_DEFAULT_DIRECTORY_PERMISSIONS
    OWNER_READ OWNER_WRITE OWNER_EXECUTE
    GROUP_READ GROUP_EXECUTE
    WORLD_READ WORLD_EXECUTE
  )

  if ( WIN32 )
    set(CPACK_PACKAGE_NAME "The One Ring - Loremaster Tools")
    # set the install/uninstall icon used for the installer itself
    set (CPACK_NSIS_MUI_ICON
      "${PROJECT_SOURCE_DIR}/favicon.ico")
    set (CPACK_NSIS_MUI_UNIICON
      "${PROJECT_SOURCE_DIR}/favicon.ico")
    # set the add/remove programs icon using an installed executable
    set (CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\loremastertools.exe")
    set (CPACK_CREATE_DESKTOP_LINKS loremastertools)
    # Start menu
    set (CPACK_PACKAGE_EXECUTABLES "loremastertools" "The One Ring - Loremaster tools" )

    # Add required libraries
    windeployqt(loremastertools)

    # Install
    install(
      TARGETS ${PROJECT_NAME}
      COMPONENT ${PROJECT_NAME}
      RUNTIME_DEPENDENCIES
      PRE_EXCLUDE_REGEXES "api-ms-" "ext-ms-" "yaml-"
      POST_EXCLUDE_REGEXES ".*system32/.*\\.dll"
    )
    # Yaml libraries
    file(GLOB YAML_LIB ${yaml-cpp_LIB_DIRS}/*lib)
    message(YAML: ${YAML_LIB})
    install(FILES ${YAML_LIB} DESTINATION bin)

    # Qt
    install(DIRECTORY build/Release/imageformats DESTINATION bin)
    install(DIRECTORY build/Release/platforms DESTINATION bin)
    install(DIRECTORY build/Release/styles DESTINATION bin)
    install(DIRECTORY build/Release/translations DESTINATION bin)

  else()
    set(CPACK_PACKAGE_NAME "the-one-ring-loremaster-tools")
    # package name for deb
    # if set, then instead of some-application-0.9.2-Linux.deb
    # you'll get some-application_0.9.2_amd64.deb (note the underscores too)
    set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
    # without this you won't be able to pack only specified component
    set(CPACK_DEB_COMPONENT_INSTALL YES)
    set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

    # https://unix.stackexchange.com/a/11552/254512
    set(CPACK_PACKAGING_INSTALL_PREFIX "/opt/the-one-ring-loremaster-tools")#/${CMAKE_PROJECT_VERSION}")

    install(
      TARGETS ${PROJECT_NAME}
      COMPONENT ${PROJECT_NAME}
    )

    # Application shortcut
    install(FILES the-one-ring.desktop DESTINATION /usr/share/applications)
  endif()

  # Icon
  install(FILES "favicon.png" DESTINATION .)
  install(FILES "favicon.ico" DESTINATION .)

  # Data files
  file(GLOB DATA_FILES
    ${PROJECT_SOURCE_DIR}/data/*json
  )
  foreach(file ${DATA_FILES})
    install(
      FILES ${file}
      DESTINATION data
    )
  endforeach()

  # Translation files
  set(TRANSLATION_FILES
    ${PROJECT_SOURCE_DIR}/translate/fr.qm
    ${PROJECT_SOURCE_DIR}/translate/en.qm
    ${PROJECT_SOURCE_DIR}/translate/pt.qm
    ${PROJECT_SOURCE_DIR}/translate/it.qm
    ${PROJECT_SOURCE_DIR}/translate/languages.yaml
  )
  foreach(file ${TRANSLATION_FILES})
    install(
      FILES ${file}
      DESTINATION translate
    )
  endforeach()

  # Add swagger
  install(DIRECTORY ${PROJECT_SOURCE_DIR}/build/swagger DESTINATION .)

  include(CPack)
endfunction()
