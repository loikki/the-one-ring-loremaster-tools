cmake_minimum_required(VERSION 3.16)
project(loremastertools VERSION 4.1.0 LANGUAGES CXX)
set(FRONTEND_URL "https://theonering-loremaster-tools.herokuapp.com/")
include(CTest)

# Setup Qt
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

# Setup Doxygen
set(DOXYGEN_GENERATE_HTML YES)

# Update some paths
if (NOT WIN32)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
endif()
set(CMAKE_MODULE_PATH ${CMAKE_BINARY_DIR} ${CMAKE_MODULE_PATH})

# set the project name
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Get all the files
file(GLOB UI_FILES ${PROJECT_SOURCE_DIR}/src/gui/*.ui)
file(GLOB SRC_FILES
  ${PROJECT_SOURCE_DIR}/src/*.cpp
  ${PROJECT_SOURCE_DIR}/src/*.hpp
  ${PROJECT_SOURCE_DIR}/src/*/*.cpp
  ${PROJECT_SOURCE_DIR}/src/*/*.hpp
)
file(GLOB TEST_SRC_FILES
  ${PROJECT_SOURCE_DIR}/tests/*cpp
)
file(GLOB TEST_HEADER_FILES
  ${PROJECT_SOURCE_DIR}/tests/*hpp
)
file(GLOB RESOURCE_FILES
  ${PROJECT_SOURCE_DIR}/resources/*qrc
)


# Remove the main from the sources
set(MAIN_FILE ${PROJECT_SOURCE_DIR}/src/main.cpp)
list(REMOVE_ITEM SRC_FILES ${MAIN_FILE})

# Find libraries
find_package(OpenMP)
find_package(Qt6 REQUIRED COMPONENTS Core Widgets LinguistTools Charts Svg)
find_package(Qt6LinguistTools REQUIRED)
find_package(yaml-cpp REQUIRED)
find_package(oatpp REQUIRED HINTS ${PROJECT_SOURCE_DIR}/conan)
find_package(oatpp-swagger REQUIRED HINTS ${PROJECT_SOURCE_DIR}/conan)
find_package(Doxygen)
include_directories(${YAML_CPP_INCLUDE_DIRS})

# Own personal files
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})
include(Compile)
include(Install)
if ( WIN32 )
  include(Windeployqt)
endif()

# Add ui (small hack in order to write files into src)
set(PREV_BIN_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(CMAKE_CURRENT_BINARY_DIR ${PROJECT_SOURCE_DIR}/src/gui)
qt6_wrap_ui(UI_HEADERS ${UI_FILES})
set(CMAKE_CURRENT_BINARY_DIR ${PREV_BIN_DIR})

# Set the options
option(EnableSanitizer "Enable sanitizer" OFF)
option(EnableUndefinedSanitizer "Enable the undefined sanitizer" OFF)
option(EnableCoverage "Enable the coverage" OFF)

# Create the lib
add_library(lib-loremastertools ${SRC_FILES} ${UI_HEADERS})
set_compilation(lib-loremastertools)

# Add resources
qt_add_resources(QT_RESOURCE_FILES ${RESOURCE_FILES})

# Create the binaries
add_executable(loremastertools WIN32 ${MAIN_FILE} ${PROJECT_SOURCE_DIR}/favicon.rc ${QT_RESOURCE_FILES})

# Add all the required options
set_compilation(loremastertools)
set_install()

# Set test
foreach(file ${TEST_SRC_FILES})
  get_filename_component(tmp ${file} NAME_WE)
  set(TEST_BIN ${tmp} ${TEST_BIN})
  add_executable(${tmp} EXCLUDE_FROM_ALL ${file} ${QT_RESOURCE_FILES})
  add_test(NAME ${tmp} COMMAND bin/${tmp} -platform offscreen)
  target_compile_definitions(${tmp} PUBLIC OWN_TEST="true")
endforeach()

# Copy swagger files
set(SWAGGER_RES ${oatpp-swagger_INCLUDE_DIRS}/../)
file(COPY ${SWAGGER_RES}/bin/oatpp-swagger/res DESTINATION swagger)

# Copy data
file(COPY ${PROJECT_SOURCE_DIR}/data DESTINATION .)
file(COPY ${PROJECT_SOURCE_DIR}/translate DESTINATION .)

foreach(file ${TEST_SRC_FILES})
  get_filename_component(tmp ${file} NAME_WE)
  set_compilation(${tmp})
endforeach()


# Add clang-format
add_custom_target(
  format
  COMMAND
  ${PROJECT_SOURCE_DIR}/format.sh format ${SRC_FILES} ${TEST_SRC_FILES} ${TEST_HEADER_FILES} ${MAIN_FILE}
)
add_custom_target(
  check-format
  COMMAND
  ${PROJECT_SOURCE_DIR}/format.sh check ${SRC_FILES} ${TEST_SRC_FILES} ${TEST_HEADER_FILES} ${MAIN_FILE}
)
add_custom_target(
  build-tests
  COMMAND
  make ${TEST_BIN}
)

# Translation
set(CMAKE_CURRENT_BINARY_DIR ${PROJECT_SOURCE_DIR}/translate/)
qt6_add_translations(loremastertools
  TS_FILES ${PROJECT_SOURCE_DIR}/translate/en.ts
  ${PROJECT_SOURCE_DIR}/translate/pt.ts
  ${PROJECT_SOURCE_DIR}/translate/it.ts
  ${PROJECT_SOURCE_DIR}/translate/fr.ts
  SOURCES ${SRC_FILES} ${UI_FILES} ${MAIN_FILE}
)
set(CMAKE_CURRENT_BINARY_DIR ${PREV_BIN_DIR})

# Create uninstall target
add_custom_target(uninstall
	COMMAND xargs rm < install_manifest.txt && rm install_manifest.txt
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
	COMMENT "Uninstalling executable..."
	VERBATIM
)

# Add clang-tidy
list(GET Qt6Core_INCLUDE_DIRS 0 QTCORE_INCLUDE)
string(REPLACE "/QtCore" "" QT_INCLUDE ${QTCORE_INCLUDE})
# Gui
set(QTGUI_INCLUDE ${QT_INCLUDE})
string(APPEND QTGUI_INCLUDE "/QtGui")
# Widgets
set(QTWIDGETS_INCLUDE ${QT_INCLUDE})
string(APPEND QTWIDGETS_INCLUDE "/QtWidgets")
# Charts
set(QTCHARTS_INCLUDE ${QT_INCLUDE})
string(APPEND QTCHARTS_INCLUDE "/QtCharts")

message(${QTCORE_INCLUDE})

# No need to use the real frontend url
string(REPLACE "." ";" VERSION_LIST ${CMAKE_CXX_COMPILER_VERSION})
list(GET VERSION_LIST 0 COMPILER_MAJOR)

add_custom_target(tidy
  COMMAND clang-tidy -extra-arg=-std=c++17 -extra-arg=-DTHE_ONE_RING_VERSION=\\"${CMAKE_PROJECT_VERSION}\\" -extra-arg=-DFRONTEND_URL=\\"abc\\" --config-file=.clang-tidy ${SRC_FILES} ${MAIN_FILE} -- -I${QTCORE_INCLUDE} -I${QT_INCLUDE} -I${QTGUI_INCLUDE} -I${QTWIDGETS_INCLUDE} -I${QTCHARTS_INCLUDE} -I${oatpp_INCLUDE_DIR} -I${oatpp-swagger_INCLUDE_DIR} -I./lib-loremastertools_autogen -I${PROJECT_SOURCE_DIR}/build/lib-loremastertools_autogen/include -I${PROJECT_SOURCE_DIR}/build/lib-loremastertools_autogen/include/gui -I${PROJECT_SOURCE_DIR}/build/lib-loremastertools_autogen/gui -I/usr/include/c++/${COMPILER_MAJOR}/ -I/usr/include/x86_64-linux-gnu/c++/${COMPILER_MAJOR}
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
)

# Add appimage
add_custom_target(appimage
  COMMAND VERSION=${CMAKE_PROJECT_VERSION} ${PROJECT_SOURCE_DIR}/make_appimage.sh
)

add_custom_target(cppclean
  COMMAND cppclean ${SRC_FILES} ${MAIN_FILE}
)


doxygen_add_docs(
  doxygen
  ${SRC_FILES}
  COMMENT "Generate html pages"
)
